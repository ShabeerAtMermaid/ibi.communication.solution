package org.ibi.communication.callhistoryimporter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseManager 
{
   // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";	   
   static final String DB_URL = "jdbc:mysql://localhost/communication";
   //  Database credentials
   static final String USER = "communication";
   static final String PASS = "ibi0786";
   
   private static Connection getDBConnection()
   {
	   Connection conn = null;
	   try
	   {
	      //STEP 1: Register JDBC driver
	      Class.forName(JDBC_DRIVER);
	      
	      //STEP 2: Open a connection
	      conn = DriverManager.getConnection(DB_URL,USER,PASS);
	      return conn;
	   }
	   catch(Exception e)
	   {
		   ;
	   }
	   return null;
   }

   public static void updateCallHistory() 
   {
		Connection conn = null;
		CallableStatement stmt = null;
		try
		{
			conn = getDBConnection();
			int i = 1;
			while(i < 1000000)
			{
				try
				{
					stmt = conn.prepareCall("{call insertcallhistorydummyrecords()}");
					stmt.executeQuery();
				}
				catch(Exception ex)
				{
					;
				}
				Thread.sleep(1 * 50);
				i = i + 1;
			}
	         if(stmt!=null)
		            stmt.close();
	         if(conn!=null)
		            conn.close();
		}
		catch(Exception ex)
		{
			;
		}
	}
}  

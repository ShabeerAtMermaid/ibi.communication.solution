package model;

import java.util.Date;

public class PTAJourney 
{
	private int ptaJourneyNumber;
	private int operatorDriverId;
	private String line;
	private String busNumber;
	private String journeyDate;
	private String plannedStart;
	private String plannedEnd;
	
	public PTAJourney(  int ptaJourneyNumber,
						int operatorDriverId,
						String line,
						String busNumber,
						String journeyDate,
						String plannedStart,
						String plannedEnd)
	{
		this.ptaJourneyNumber = ptaJourneyNumber;
		this.operatorDriverId = operatorDriverId;
		this.line = line;
		this.busNumber = busNumber;
		this.journeyDate = journeyDate;
		this.plannedStart = plannedStart;
		this.plannedEnd = plannedEnd;
	}
	
	public int getPTAJourneyNumber()
	{
		return this.ptaJourneyNumber;
	}
	public void setPTAJourneyNumber(int value)
	{
		this.ptaJourneyNumber = value;
	}
	
	public int getOperatorDriverId()
	{
		return this.operatorDriverId;
	}
	public void setOperatorDriverId(int value)
	{
		this.operatorDriverId = value;
	}
	
	public String getLine()
	{
		return this.line;
	}
	public void setLine(String value)
	{
		this.line = value;
	}
	
	public String getBusNumber()
	{
		return this.busNumber;
	}
	public void setBusNumber(String value)
	{
		this.busNumber = value;
	}
	
	public String getJourneyDate()
	{
		return this.journeyDate;
	}
	public void setJourneyDate(String value)
	{
		this.journeyDate = value;
	}
	
	public String getplannedStart()
	{
		return this.plannedStart;
	}
	public void setplannedStart(String value)
	{
		this.plannedStart = value;
	}
	
	public String getplannedEnd()
	{
		return this.plannedEnd;
	}
	public void setplannedEnd(String value)
	{
		this.plannedEnd = value;
	}

}

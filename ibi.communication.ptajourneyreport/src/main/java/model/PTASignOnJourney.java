package model;

import java.util.Date;

public class PTASignOnJourney 
{
	private String busNumber;
	private String line;
	private String signonTime;
	private int ptaJourneynumber;
	private String pta;
	
	public PTASignOnJourney(String busNumber,
							String line,
							String signonTime,
							int ptaJourneynumber,
							String pta)
	{
		this.busNumber = busNumber;
		this.line = line;
		this.signonTime = signonTime;
		this.ptaJourneynumber = ptaJourneynumber;
		this.pta = pta;
	}
	public String getBusNumber()
	{
		return this.busNumber;
	}
	public void setBusNumber(String value)
	{
		this.busNumber = value;
	}
	
	public String getLine()
	{
		return this.line;
	}
	public void setLine(String value)
	{
		this.line = value;
	}
	
	public String getSignonTime()
	{
		return this.signonTime;
	}
	public void setJourneyDate(String value)
	{
		this.signonTime = value;
	}
	
	public int getPtaJourneynumber()
	{
		return this.ptaJourneynumber;
	}
	public void setPtaJourneynumber(int value)
	{
		this.ptaJourneynumber = value;
	}
	
	public String getPta()
	{
		return this.pta;
	}
	public void setPta(String value)
	{
		this.pta = value;
	}
}

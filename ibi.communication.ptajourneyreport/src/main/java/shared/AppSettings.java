package shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class AppSettings 
{	
	public static String getDatabaseName()
	{
		String value = getConfigValue("dbname");
		return (value == null || value.equals("")) ? "jdbc:mysql://localhost/communication" : value;
	}
	
	public static String getDatabaseUser()
	{
		String value = getConfigValue("dbusername");
		return (value == null || value.equals("")) ? "communication" : value;
	}
	
	public static String getDatabaseUserPassword()
	{
		String value = getConfigValue("dbpassword");
		return (value == null || value.equals("")) ? "ibi0786" : value;
	}
	
	public static String getMSDatabaseServer()
	{
		String value = getConfigValue("msdbserver");
		return (value == null || value.equals("")) ? "Patty.mermaid.local" : value;
	}
	
	public static String getMSDatabaseName()
	{
		String value = getConfigValue("msdbname");
		return (value == null || value.equals("")) ? "IBI_Data" : value;
	}
	
	public static String getMSDatabaseUser()
	{
		String value = getConfigValue("msdbusername");
		return (value == null || value.equals("")) ? "global" : value;
	}
	
	public static String getMSDatabaseUserPassword()
	{
		String value = getConfigValue("msdbpassword");
		return (value == null || value.equals("")) ? "@sG/001002_" : value;
	}
	
	public static int getReportGenerationTime_HourKey()
	{
		String value = getConfigValue("reportgenerationtime_hourkey");
		return (value == null || value.equals("")) ? 6 : Integer.parseInt(value);
	}
	
	public static int getReportGenerationTime_MinKey()
	{
		String value = getConfigValue("reportgenerationtime_minkey");
		return (value == null || value.equals("")) ? 0 : Integer.parseInt(value);
	}
	
	public static String getFromAccount()
	{
		String value = getConfigValue("fromaccount");
		return (value == null || value.equals("")) ? "noreply@mermaid.dk" : value;
	}

	public static String getToAccount()
	{
		String value = getConfigValue("toaccount");
		return (value == null || value.equals("")) ? "support@mermaid.dk" : value;
	}
	
	public static String getSMTPHost()
	{
		String value = getConfigValue("smtphost");
		return (value == null || value.equals("")) ? "mail.mermaid.dk" : value;
	}
	
	public static int getSMTPPort()
	{
		String value = getConfigValue("smtpport");
		return (value == null || value.equals("")) ? 25 : Integer.parseInt(value);
	}
	
	public static Boolean getSMTPssl()
	{
		String value = getConfigValue("smtpssl");
		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);
	}
	
	public static String getEmailSubject()
	{
		String value = getConfigValue("subject");
		return (value == null || value.equals("")) ? "signon report" : value;
	}
	
	public static String getEmailBodyText()
	{
		String value = getConfigValue("bodytext");
		return (value == null || value.equals("")) ? "signon report" : value;
	}
	
	public static String getEmailAttachmentName()
	{
		String value = getConfigValue("attachmentname");
		return (value == null || value.equals("")) ? "Signonreport" : value;
	}

	public static String getAttachmentPTAJourneyFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), "ptajourneyreport.txt");
		return filePath.toString();
	}
	
	/* helper function*/
	private static String getConfigFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), "config.properties");
		return filePath.toString();
	}
	
	private static String getConfigValue(String settingName)
	{
		InputStream inputStream = null;
    	try 
    	{
    		Properties prop = new Properties();
    		//String propFileName = "config.properties";
    		//inputStream = Main.class.getClassLoader().getResourceAsStream(propFileName);
    		
    		String externalFileName = getConfigFilePath();
    		inputStream = new FileInputStream(new File(externalFileName));
    		prop.load(inputStream);
    		return prop.getProperty(settingName);
    	} 
    	catch (Exception e) 
    	{
    		return "";
    	} 
    	finally 
    	{
    		try {
    			if (inputStream != null)
    				inputStream.close();	
    		} catch (IOException e) {}
    	}
	}
	/* helper function*/
}

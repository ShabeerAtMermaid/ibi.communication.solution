package shared;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import model.PTASignOnJourney;

import java.sql.Date;

public class MSSQLDatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";	   
	   
	   static final String DB_Server = AppSettings.getMSDatabaseServer();
	   static final String DB_Name = AppSettings.getMSDatabaseName();
	   //  Database credentials
	   static final String USER = AppSettings.getMSDatabaseUser();
	   static final String PASS = AppSettings.getMSDatabaseUserPassword();
	   
	   //private static Connection conn = null;

	   public static LinkedHashMap<String,Integer> MSSQLbusesptajourneyssignoncount = new LinkedHashMap<String,Integer>();  
	   //public static LinkedHashMap<String, Object> MSSQLbusesptajourneyssignonlogs = new LinkedHashMap<String, Object>();
	   public static List<PTASignOnJourney> MSSQLbusesptajourneyssignonlogs = new ArrayList<PTASignOnJourney>();
	   
	   private static Connection getDBConnection() throws InterruptedException
	   {
		   Connection conn = null;
		   while(true)
		   {
			   try
			   {
				  //if (conn != null ) return conn;
				  conn = DriverManager.getConnection("jdbc:sqlserver://"+ DB_Server +":1433;databaseName="+ DB_Name +";user="+ USER +";password="+PASS);
			      return conn;
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_PTAJourneys_Report", "MSSQL getDBConnection Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
				   Logger.writeError("communication_PTAJourneys_Report",  se);
			   }
			   catch(Exception e)
			   {
				   Logger.write("communication_PTAJourneys_Report", "MSSQL getDBConnection Exception DB: " + e.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
				   Logger.writeError("communication_PTAJourneys_Report",  e);
			   }
			   Logger.write("communication_PTAJourneys_Report", "Retrying to connect after 5 seconds" + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
			   Thread.sleep(1000 * 5 );
		   }
	   }
	   
	   public static void getPTAJourneysSignonLogs() throws SQLException
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
			  conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call getptajourneyssignonlogs}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_PTAJourneys_Report", String.format("Qeury {call getptajourneyssignonlogs()} executed"));
		      //STEP 4: Extract data from result set
		      if (MSSQLbusesptajourneyssignonlogs != null && MSSQLbusesptajourneyssignonlogs.size() > 0) MSSQLbusesptajourneyssignonlogs.clear();
		      while(rs.next())
		      {
		    	  PTASignOnJourney ptaSigonJourney = new PTASignOnJourney(
		    			  							 rs.getString("busnumber"),
									    			 rs.getString("line"),
									    			 rs.getString("signonTime"),
									    			 Integer.parseInt(rs.getString("ptaJourneynumber")),
									    			 rs.getString("pta"));
		    	  MSSQLbusesptajourneyssignonlogs.add(ptaSigonJourney);
		      }
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_PTAJourneys_Report", "MSSQL getPTAJourneysSignonLogs >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_PTAJourneys_Report", "MSSQL getPTAJourneysSignonLogs >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
	   }

	   private static void cleanup(Connection conn, CallableStatement stmt, ResultSet rs)
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se)
	      {
	    	  Logger.write("communication_PTAJourneys_Report", "cleanup >>MSSQL Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_Server,USER,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se1)
	      {
	    	  Logger.write("communication_PTAJourneys_Report", "cleanup >>MSSQL Exception DB: " + se1.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_Server,USER,USER,PASS));
	      }// nothing we can do
	     
	      try
	      {
	         if(conn!=null)
	        	 conn.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.write("communication_PTAJourneys_Report", "cleanup >>MSSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_Server,USER,USER,PASS));
	      }//end finally try
	      
	   }
}  

package shared;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import model.PTAJourney;
import model.PTASignOnJourney;

public class ReportManager 
{
	public static void ExportReport() throws Exception
	{
		MSSQLDatabaseManager.getPTAJourneysSignonLogs();
		DatabaseManager.getPTAJourneysSignonLogs();
		DatabaseManager.getBusesPTAJourneysNotSignonCount();
		DatabaseManager.getDriversPTAJourneysNotSignonCount();

		//SendEmail.SendReport();
		StringBuilder sb = new StringBuilder();
		sb.append("Report generated - " +  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		sb.append("\n-------------------------------------------------------------------------------------------------------------------------------------------\n");
		sb.append("***Summary*********************************************************************************************************************************\n");
		sb.append("-------------------------------------------------------------------------------------------------------------------------------------------\n");
		sb.append("[busnumber]      [journeycount] not signon\n");
		List<Map.Entry<String, Integer>> busesCount = ApplicationUtility.sortMapValues_KeyAsString(DatabaseManager.busesPtaNotSignonCount);
		for (Map.Entry<String, Integer> entry : busesCount) {
			String key = (String)entry.getKey();
			String count = String.valueOf(entry.getValue());
			count       =  "             " + (count.length() == 0? "      " : "") + count;
			sb.append(String.format("%s%s\n", key,count));
		}
		sb.append("\n[driverid]       [journeycount] not signon\n");		
		List<Map.Entry<Integer, Integer>> drivercoutn = ApplicationUtility.sortMapValues_KeyAsInteger(DatabaseManager.driversPtaNotSignonCount);
		for (Map.Entry<Integer, Integer> entry : drivercoutn) {
			int key = entry.getKey();
			String count = String.valueOf(entry.getValue());
			count       =  (String.valueOf(key).length() <= 5 ? "            ": "           ") + count;
			sb.append(String.format("%s%s\n", key,count));
		}
		sb.append("\n-------------------------------------------------------------------------------------------------------------------------------------------\n");
		sb.append("***Detail************************************************************************************************************************************\n");
		sb.append("-------------------------------------------------------------------------------------------------------------------------------------------\n");
		sb.append("[line]   [journey]     [planned start]         [planned end]             [planned bus]    [planned driver]  [signon bus]    [signon time]\n");
		/*
		for (Map.Entry<String, Object> entry : DatabaseManager.MYSQLbusesptajourneyssignonlogs.entrySet()) {
			String journeyNumberInMYSQL = (String)entry.getKey();
			PTAJourney journeyDetailInMYSQL = (PTAJourney)entry.getValue();
			PTASignOnJourney journeyDetailInMSSQL = MSSQLDatabaseManager.MSSQLbusesptajourneyssignonlogs.containsKey(journeyNumberInMYSQL)? (PTASignOnJourney)MSSQLDatabaseManager.MSSQLbusesptajourneyssignonlogs.get(journeyNumberInMYSQL):null;			
			String lineWord = journeyDetailInMYSQL.getLine();
			String fileLine = lineWord + "       ";
			lineWord = String.valueOf(journeyDetailInMYSQL.getPTAJourneyNumber());
			fileLine       +=  lineWord + "           " + ( lineWord.length() == 1? "  " :( lineWord.length() == 2 ? " " : ""));
			fileLine       +=  journeyDetailInMYSQL.getplannedStart().substring(0, journeyDetailInMYSQL.getplannedStart().lastIndexOf(".")) + "     ";
			fileLine       +=  journeyDetailInMYSQL.getplannedEnd().substring(0, journeyDetailInMYSQL.getplannedEnd().lastIndexOf(".")) + "       ";
			lineWord       =   journeyDetailInMYSQL.getBusNumber();
			fileLine       +=  lineWord + "             " + ( lineWord.length() == 0? "    " : "");
			lineWord       =   String.valueOf(journeyDetailInMYSQL.getOperatorDriverId());
			fileLine       +=  lineWord + "            " + ( lineWord.length() == 4? "  " : ( lineWord.length() == 5? " " : ""));
			fileLine       +=  (journeyDetailInMSSQL != null? journeyDetailInMSSQL.getBusNumber():"") + "            ";
			fileLine       +=  (journeyDetailInMSSQL != null? journeyDetailInMSSQL.getSignonTime().substring(0, journeyDetailInMSSQL.getSignonTime().lastIndexOf(".")) :"") + "\n";
			sb.append(fileLine);
		}
		*/
		
		for (PTAJourney mysqlEntry : DatabaseManager.MYSQLbusesptajourneyssignonlogs) 
		{
			String signOnBus = "";
			String SignonTime = "";

			for (PTASignOnJourney mssqlEntry : MSSQLDatabaseManager.MSSQLbusesptajourneyssignonlogs)
			{
				int diffInMins = ApplicationUtility.getDifferenceInMins(mssqlEntry.getSignonTime(),mysqlEntry.getplannedStart());
				if (mysqlEntry.getLine().equalsIgnoreCase(mysqlEntry.getLine()) && mysqlEntry.getPTAJourneyNumber() == mssqlEntry.getPtaJourneynumber() && (diffInMins >= 0 && diffInMins <= 120))
				{
					signOnBus = mssqlEntry.getBusNumber();
					SignonTime = mssqlEntry.getSignonTime();
					break;
				}
			}
			
			String lineWord = mysqlEntry.getLine();
			String fileLine = lineWord + "       ";
			lineWord = String.valueOf(mysqlEntry.getPTAJourneyNumber());
			fileLine       +=  lineWord + "           " + ( lineWord.length() == 1? "  " :( lineWord.length() == 2 ? " " : ""));
			fileLine       +=  mysqlEntry.getplannedStart().substring(0, mysqlEntry.getplannedStart().lastIndexOf(".")) + "     ";
			fileLine       +=  mysqlEntry.getplannedEnd().substring(0, mysqlEntry.getplannedEnd().lastIndexOf(".")) + "       ";
			lineWord       =   mysqlEntry.getBusNumber();
			fileLine       +=  lineWord + "             " + ( lineWord.length() == 0? "    " : "");
			lineWord       =   String.valueOf(mysqlEntry.getOperatorDriverId());
			fileLine       +=  lineWord + "            " + ( lineWord.length() == 4? "  " : ( lineWord.length() == 5? " " : ""));
			fileLine       +=  signOnBus + "            ";
			fileLine       +=  SignonTime.substring(0, SignonTime.lastIndexOf(".")) + "\n";
			sb.append(fileLine);
		}
		
		sb.append("-----------------------------------------------------------------------------------------------------------------------------------------------\n");
		sb.append("***********************************************************************************************************************************************\n");
		try 
		{
			ApplicationUtility.writeFileContents(AppSettings.getAttachmentPTAJourneyFilePath(), sb.toString());
		} catch (Exception e) 
		{
			Logger.writeError("communication_PTAJourneys_ReportReport", e);
		}
		
		
		SendEmail.SendReport();
	}
}

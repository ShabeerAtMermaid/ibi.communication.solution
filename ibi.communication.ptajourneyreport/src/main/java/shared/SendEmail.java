package shared;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmail 
{
	public static void SendReport()  {
	      // Recipient's email ID needs to be mentioned.
	      String to = AppSettings.getToAccount();

	      // Sender's email ID needs to be mentioned
	      String from = AppSettings.getFromAccount();
	      
	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = AppSettings.getSMTPHost();
	      int port = AppSettings.getSMTPPort();

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "false");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", port);

	      // Get the Session object.
	      Session session = Session.getInstance(props);

	      try {
	         // Create a default MimeMessage object.
	         Message message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));
	         InternetAddress[] parse = InternetAddress.parse(to , true);
	         // Set To: header field of the header.
	         message.setRecipients(Message.RecipientType.TO,
	        		 parse);

	         // Set Subject: header field
	         message.setSubject(String.format("%s - %s", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()) ,AppSettings.getEmailSubject()));

	         // Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();

	         // Now set the actual message
	         messageBodyPart.setText(String.format("%s - %s", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()) ,AppSettings.getEmailBodyText()));

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment
	         messageBodyPart = new MimeBodyPart();
	         String filename = AppSettings.getAttachmentPTAJourneyFilePath();
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	        // messageBodyPart.setFileName(filename);
	         messageBodyPart.setFileName(String.format("%s - %s.txt", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()) ,AppSettings.getEmailAttachmentName()));
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart);

	         // Send message
	         Transport.send(message);
	         Logger.write("communication_PTAJourneys_Report", "Sent message successfully....");
	  
	      } catch (MessagingException e) {
	    	  Logger.write("communication_PTAJourneys_Report", "Exception in SendReport " + e.getMessage());
	      }
	   }
}

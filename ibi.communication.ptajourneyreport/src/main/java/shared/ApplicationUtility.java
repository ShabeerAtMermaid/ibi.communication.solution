package shared;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ApplicationUtility
{
	public static boolean isNullOrEmpty(String text)
	{
		if(text == null || text.equals("") || text.equals("null"))
			return true;
		else
			return false;
	}
	public static Date getDateFromString(String dateString) throws Exception
	{
		dateString = dateString.replace("T", " ");
		dateString = dateString.replace("/", "-");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date tempDate = (Date)format.parse(dateString);
		return tempDate;
	}
	
	public static Date getDateTimeFromString(String dateString) throws Exception
	{
		dateString = dateString.replace("T", " ");
		dateString = dateString.replace("/", "-");
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date tempDate = (Date)format.parse(dateString);
		return tempDate;
	}
	
	public static int getDifferenceInMins(String first, String second)
	{
		int result = 0;
		try
		{
			first = first.replace("T", " ");
			second = second.replace("T", " ");
			SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
    		@SuppressWarnings("deprecation")
    		Date firstDate = ft.parse(first);
    		Date secondDate = ft.parse(second);
			long diff = secondDate.getTime() - firstDate.getTime();
			//result = (int)(diff / 1000);
			result = (int)diff / (60 * 1000) % 60;
			if (result < 0)
				result = result * -1;
		}
		catch(Exception ex)
		{
			result = -1;
		}
		return result;
	}
	
	public static void writeFileContents(String filePath, String contents) throws Exception
	{
		try {
			FileOutputStream fos = new FileOutputStream(filePath);
			java.io.FileDescriptor fd = fos.getFD();
			
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos,"UTF-8"), 32768);
			out.write(contents);
			out.flush();
			fd.sync();
			out.close();
		} catch (IOException e) {
			Logger.writeError("communication_PTAJourneys_ReportReport", e);
		}
	}
	
	public static List<Map.Entry<Integer, Integer>> sortMapValues_KeyAsInteger(Map<Integer, Integer> map){
	    //Sort Map.Entry by value
	    List<Map.Entry<Integer, Integer>> result = new ArrayList(map.entrySet());
	    Collections.sort(result, new Comparator<Map.Entry<Integer, Integer>>(){
	        public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
	            return o2.getValue() - o1.getValue();
	    }});

	    return result;  
	}
	public static List<Map.Entry<String, Integer>> sortMapValues_KeyAsString(Map<String, Integer> map){
	    //Sort Map.Entry by value
	    List<Map.Entry<String, Integer>> result = new ArrayList(map.entrySet());
	    Collections.sort(result, new Comparator<Map.Entry<String, Integer>>(){
	        public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
	            return o2.getValue() - o1.getValue();
	    }});
	    return result;  
	}
}

package shared;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import model.PTAJourney;
import model.PTASignOnJourney;

import java.sql.Date;

public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";	   
	   static final String DB_URL = AppSettings.getDatabaseName();
	   //  Database credentials
	   static final String USER = AppSettings.getDatabaseUser();
	   static final String PASS = AppSettings.getDatabaseUserPassword();
	   
	   public static LinkedHashMap<String,Integer> busesPtaNotSignonCount = new LinkedHashMap<String,Integer>();  
	   public static LinkedHashMap<Integer,Integer> driversPtaNotSignonCount = new LinkedHashMap<Integer,Integer>();
	   //public static LinkedHashMap<String, Object> MYSQLbusesptajourneyssignonlogs = new LinkedHashMap<String, Object>();
	   public static List<PTAJourney> MYSQLbusesptajourneyssignonlogs = new ArrayList<PTAJourney>();

	   private static Connection getDBConnection() throws InterruptedException
	   {
		   Connection conn = null;
		   while(true)
		   {
			   try
			   {
				   //if (conn != null) return conn;
			      //STEP 1: Register JDBC driver
			      Class.forName(JDBC_DRIVER);
			      //Logger.write("communication_rest", "Step -01 Register JDBC driver and DB_URL: " + DB_URL + " USER: " + USER + " PASS: " + PASS);
			      
			      //STEP 2: Open a connection
			      conn = DriverManager.getConnection(DB_URL,USER,PASS);
			      //Logger.write("communication_rest", "Step -02 Open a connection");
			      return conn;
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_PTAJourneys_Report", "MYSQL Exception DB: getDBConnection " + se.getMessage());
				   Logger.writeError("communication_PTAJourneys_Report",  se);
			   }
			   catch(Exception e)
			   {
				   Logger.write("communication_PTAJourneys_Report", "MYSQL Exception DB: getDBConnection " + e.getMessage());
				   Logger.writeError("communication_PTAJourneys_Report",  e);
			   }
			   Logger.write("communication_PTAJourneys_Report", "getDBConnection : Retrying after 5 seconds");
			   Thread.sleep(1000 * 5);
		   }
		   
	   }
	   
	   public static void getBusesPTAJourneysNotSignonCount()
	   {
		   try
		   {
			   if (busesPtaNotSignonCount != null && busesPtaNotSignonCount.size() > 0) busesPtaNotSignonCount.clear();
				for (PTAJourney mysqlEntry : DatabaseManager.MYSQLbusesptajourneyssignonlogs) 
				{
					boolean vehiclefound = false;
					for (PTASignOnJourney mssqlEntry : MSSQLDatabaseManager.MSSQLbusesptajourneyssignonlogs)
					{
						int diffInMins = ApplicationUtility.getDifferenceInMins(mssqlEntry.getSignonTime(),mysqlEntry.getplannedStart());
						if (mysqlEntry.getLine().equalsIgnoreCase(mysqlEntry.getLine()) && mysqlEntry.getPTAJourneyNumber() == mssqlEntry.getPtaJourneynumber() && (diffInMins >= 0 && diffInMins <= 120))
						{
							vehiclefound = true;
							break;
						}
					}
					if(!vehiclefound)
					{
						if (busesPtaNotSignonCount.containsKey(mysqlEntry.getBusNumber()))
						{
							int count = busesPtaNotSignonCount.get(mysqlEntry.getBusNumber()) + 1;
							busesPtaNotSignonCount.put(mysqlEntry.getBusNumber(), count);
						}
						else
						{
							busesPtaNotSignonCount.put(mysqlEntry.getBusNumber(), 1);
						}
					}
				}
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_PTAJourneys_Report", "getBusesPTAJourneysNotSignonCount >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  e);
		   }
	   }

	   public static void getDriversPTAJourneysNotSignonCount()
	   {
		   try
		   {
			   if (driversPtaNotSignonCount != null && driversPtaNotSignonCount.size() > 0) driversPtaNotSignonCount.clear();
				for (PTAJourney mysqlEntry : DatabaseManager.MYSQLbusesptajourneyssignonlogs) 
				{
					boolean driverfound = false;
					for (PTASignOnJourney mssqlEntry : MSSQLDatabaseManager.MSSQLbusesptajourneyssignonlogs)
					{
						int diffInMins = ApplicationUtility.getDifferenceInMins(mssqlEntry.getSignonTime(),mysqlEntry.getplannedStart());
						if (mysqlEntry.getLine().equalsIgnoreCase(mysqlEntry.getLine()) && mysqlEntry.getPTAJourneyNumber() == mssqlEntry.getPtaJourneynumber() && (diffInMins >= 0 && diffInMins <= 60))
						{
							driverfound = true;
							break;
						}
					}
					if(!driverfound)
					{
						if (driversPtaNotSignonCount.containsKey(mysqlEntry.getOperatorDriverId()))
						{
							int count = driversPtaNotSignonCount.get(mysqlEntry.getOperatorDriverId()) + 1;
							driversPtaNotSignonCount.put(mysqlEntry.getOperatorDriverId(), count);
						}
						else
						{
							driversPtaNotSignonCount.put(mysqlEntry.getOperatorDriverId(), 1);
						}
					}
				}
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_PTAJourneys_Report", "getBusesPTAJourneysNotSignonCount >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  e);
		   }
	   }
	   
	   public static void getPTAJourneysSignonLogs()
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call getptajourneyssignonlogs}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_PTAJourneys_Report", String.format("MYSQL Qeury {call getptajourneyssignonlogs()} executed"));
		      //STEP 4: Extract data from result set
		      if (MYSQLbusesptajourneyssignonlogs != null && MYSQLbusesptajourneyssignonlogs.size() > 0) MYSQLbusesptajourneyssignonlogs.clear();
		      while(rs.next())
		      {
          		  PTAJourney ptaJourney = new PTAJourney(
          				  					  Integer.parseInt(rs.getString("ptajourneynumber")),
          				  					  Integer.parseInt(rs.getString("operatordriverid")),
          				  					  rs.getString("line"),
          				  					  rs.getString("busnumber"),
          				  					  rs.getString("journeydate"),
          				  					  rs.getString("plannedstart"),
          				  					  rs.getString("plannedend"));
		    	  MYSQLbusesptajourneyssignonlogs.add(ptaJourney);
		      }
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_PTAJourneys_Report", "getPTAJourneysSignonLogs >>MYSQL Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_PTAJourneys_Report", "getPTAJourneysSignonLogs >>MYSQL Exception DB: " + e.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
			   Logger.writeError("communication_PTAJourneys_Report",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
	   }
	   
	   private static void cleanup(Connection conn,CallableStatement stmt,ResultSet rs)
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneys_Report", "cleanup >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneys_Report", "cleanup >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	     
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
			   Logger.write("communication_PTAJourneys_Report", "cleanup >>MYSQL Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }//end finally try
	      
	   }
}  

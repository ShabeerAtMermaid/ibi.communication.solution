package multiq.ibi.communication.ptajourneyreport;

import java.util.Calendar;
import java.util.Date;

import shared.AppSettings;
import shared.Logger;
import shared.MSSQLDatabaseManager;
import shared.ReportManager;

public class ServiceManager 
{
	private static String version = "Production - IBI.Communication.PTAJourney_Report.Java_8.0.1"; // "Development
	private static String buildDate = "2017-08-28";
	
	private static Thread exporterThread;
	
	public static void StartServices() 
	{
		Logger.write("communication_PTAJourneys_Report", "============================================");
		Logger.write("communication_PTAJourneys_Report", "Current Version: " + version);
		Logger.write("communication_PTAJourneys_Report", "Build Date: " + buildDate);
		Logger.write("communication_PTAJourneys_Report", "============================================");

		Logger.write("communication_PTAJourneys_Report", "-----------------------------------------------");
		Logger.write("communication_PTAJourneys_Report", "Starting PTA Journey Report service");
		startExportPTAJourneyReportThread();
		/*try {
			ReportManager.ExportReport();
		} catch (Exception e) {
			Logger.write("communication_PTAJourneys_Report", "Exception startExportPTAJourneyReportThread >> " + e.getMessage());
		}*/
		Logger.write("communication_PTAJourneys_Report", "PTA Journey Report service started");
	}
	
	private static long getReportTime()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set( Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1 );
		calendar.set( Calendar.HOUR_OF_DAY, AppSettings.getReportGenerationTime_HourKey() );
		calendar.set( Calendar.MINUTE, AppSettings.getReportGenerationTime_MinKey() );
		calendar.set( Calendar.SECOND, 0 );
		return calendar.getTimeInMillis() - System.currentTimeMillis();
	}
	
	private static void startExportPTAJourneyReportThread() 
	{
	   exporterThread = new Thread()
 	   {
		public void run()
 	   	  {
			  Logger.write("communication_PTAJourneys_Report", "***********************************************");
   			  Logger.write("communication_PTAJourneys_Report", "Starting import journey report service data");
 	   		  while(true)
 	   		  {
 	   			try 
 		   		{
	 	   			//FTPManager.downlaodFTPFile();
	 	   			Logger.write("communication_PTAJourneys_Report", "***********************************************");
		 	   		try {
						ReportManager.ExportReport();
		 	   			long sleepTime = getReportTime();
	 		   			Thread.sleep(sleepTime < 0 ? 1000 * 60 * 60 * 24 : sleepTime);
	 		   			Logger.write("communication_PTAJourneys_Report", "***********************************************");

					} 
		 	   		catch (Exception e) {
		 	   			Thread.sleep(1 * 1000 * 60);
						Logger.write("communication_PTAJourneys_Report", "Exception startExportPTAJourneyReportThread >> " + e.getMessage());
					}
 			    }
 		   		catch (InterruptedException e) 
 		   		{
 		   			Logger.write("communication_PTAJourneys_Report", "Exception startExportPTAJourneyReportThread >> " + e.getMessage());
 		   		}
 	   		  }
 	   	  }
 	  };
 	 exporterThread.start();
	} 
}

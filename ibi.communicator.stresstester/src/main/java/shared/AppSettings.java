package shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class AppSettings 
{
	public static String resolveApplicationDirectory()
	{
		String applicationPath = System.getProperty("user.dir");
		String osName = System.getProperty("os.name");
		File applicationDirectory = new File(applicationPath);
		if(!applicationPath.endsWith("bin") && osName.toLowerCase().indexOf("windows") != -1 && applicationDirectory.exists())
			applicationPath = applicationPath + "/bin";

		return applicationPath + "/";
	}
	
	public static String getHostName()
	{
		String value = getConfigValue("botserver");
		return (value == null || value.equals("")) ? "teeny.mermaid.dk" : value;
	}
	
	public static int getPort()
	{
		String value = getConfigValue("botport");
		return (value == null || value.equals("")) ? 8024 : Integer.parseInt(value);
	}
	
	public static int getNumberofBuses()
	{
		String value = getConfigValue("numberofbuses");
		return (value == null || value.equals("")) ? 1 : Integer.parseInt(value);
	}
	
	public static int getPingpause_Sec()
	{
		String value = getConfigValue("pinginterval_sec");
		return (value == null || value.equals("")) ? 5 : Integer.parseInt(value);
	}
	
	public static int getStartBusNumber()
	{
		String value = getConfigValue("startbusnumber");
		return (value == null || value.equals("")) ? 9964 : Integer.parseInt(value);
	}
	
	private static String getConfigFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), "config.properties");
		return filePath.toString();
	}
	
	private static String getConfigValue(String settingName)
	{
		InputStream inputStream = null;
    	try 
    	{
    		Properties prop = new Properties();
    		//String propFileName = "config.properties";
    		//inputStream = Main.class.getClassLoader().getResourceAsStream(propFileName);
    		
    		String externalFileName = getConfigFilePath();
    		inputStream = new FileInputStream(new File(externalFileName));
    		prop.load(inputStream);
    		return prop.getProperty(settingName);
    	} 
    	catch (Exception e) 
    	{
    		return "";
    	} 
    	finally 
    	{
    		try {
    			if (inputStream != null)
    				inputStream.close();	
    		} catch (IOException e) {}
    	}
	}
}

package com.multiq.ibi.communicator.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.logging.FileHandler;

import shared.AppSettings;
import shared.ApplicationUtility;
import shared.Logger;

public class TCPClient {

   public   int DISCONNECTED = 0;
   public   int CONNECTED = 1;
  
   public  String hostIP = AppSettings.getHostName(); //"teeny.mermaid.dk"; //"localhost"; //"192.168.66.110";
   public  int port = AppSettings.getPort();
   public int pingInterval = AppSettings.getPingpause_Sec();
   
   public  int connectionStatus = DISCONNECTED;
   
    private  Thread communicationThread;
    
    private Thread outStreamThread;
    private Thread inStreamThread;
    private long heartbeatDelayMillis = 5 * 1000;
    
    // TCP Components
    public  Socket socket = null;    
    public  BufferedReader input = null;
    //public static  DataOutputStream output = null;
    public PrintWriter output = null;
    
    public TCPClient() {
    	communicationThread = new Thread() 
        {
            public void run() 
            {
            	while (true) {
					switch (connectionStatus) {
                    case 0:
                       try 
                       {
                    	   socket = new Socket();
                    	   socket.connect(new InetSocketAddress(hostIP, port), 10 * 1000);
                          
                    	   input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    	   //output = new DataOutputStream( socket.getOutputStream());
                    	   
                    	   output = new PrintWriter(
   										new BufferedWriter(
   												new OutputStreamWriter(
   														socket.getOutputStream(), "UTF-8")), true);

                    	   connectionStatus = CONNECTED;
                    	   socket.setSoTimeout(5 * 1000);
                       }
                       catch (SocketException e) {
                    	  Logger.write("communication_stresstester", String.format("SocketException >> %s - host %s - port %s", e.getMessage(),hostIP,port));
                    	  Logger.writeError("communication_stresstester", e);
                          cleanUp();
                          connectionStatus = DISCONNECTED;
                       }
                       catch (IOException e) {
                     	  Logger.write("communication_stresstester", String.format("SocketException >> %s - host %s - port %s", e.getMessage(),hostIP,port));
                     	  Logger.writeError("communication_stresstester", e);
                           cleanUp();
                           connectionStatus = DISCONNECTED;
                       }
                       catch (Exception e) {
                     	  	Logger.write("communication_stresstester", String.format("SocketException >> %s - host %s - port %s", e.getMessage(),hostIP,port));
                     	  	Logger.writeError("communication_stresstester", e);
                     	  	cleanUp();
                           connectionStatus = DISCONNECTED;
                       }
                       break;
                    default: break; // do nothing
                    }
					try { // Poll every ~1000 ms
	                       Thread.sleep(heartbeatDelayMillis);
	                }
 		   			catch (Exception e) 
 		   			{
 		   				  Logger.write("communication_stresstester", String.format("Ping command format Exception >> %s - host %s - port %s", e.getMessage(),hostIP,port));
 		   				  Logger.writeError("communication_stresstester", e);
 		   			}
                 }
            };
        };
        communicationThread.start();
    }
    public void startCommunication(int busNumber)
    {
    	startinStreamThread();
    	startouStreamThread(busNumber);
    }
    private void startouStreamThread(int busNumber) {
    	
    	class ThreadTask implements Runnable 
    	{
    		int bNumber;
    		float lat = 0;
    		float lng = 0;
    		ThreadTask(int busNumber) 
    		{
    			bNumber = busNumber;
			}
	   	   	public void run()
	   	   	{
	   	   		  lat = (float) 56.111292;
	   	   		  lng = (float) 10.1356312;
	   	   		  while(true)
	   	   		  {
	   		   		  if(connectionStatus == CONNECTED)
	   				  {
	   		   			  try
	   		   			  {
	   		   				  //lat += 0.002 + CommandsHandler.issueToken();
	   		   				  lng += CommandsHandler.issueToken();
	   		   				  
	   		   				  String ping = CommandsHandler.getPingCommand(bNumber,lat,lng);
	   		   				  sendCommand(ping);
	   		   			  }
	   		   			  catch (Exception e) 
	   		   			  {
	   		   				  Logger.write("communication_stresstester", String.format("Ping command format Exception >> %s - host %s - port %s", e.getMessage(),hostIP,port));
	   		   				  Logger.writeError("communication_stresstester", e);
	   		   			  }
	   		   			  //return;
	   				  }
	   		   		  
	   		   		 try 
	   		   		 {
	   		   			  
	   			          Thread.sleep(pingInterval * 1000);
	   			     }
	   		   		 catch (InterruptedException e) 
	   		   		 {
	   			    	  Logger.write("communication_stresstester", String.format("Ping command format Exception >> %s - host %s - port %s", e.getMessage(),hostIP,port));
	   			    	  Logger.writeError("communication_stresstester", e);
	   			      }
	   	   		  }
	   	   }
		}
		Thread t = new Thread(new ThreadTask(busNumber));
		t.start();
    	
    	
    	
       /*
       outStreamThread = new Thread()
 	   {
    	  int busNumber;
 	   	  public void run()
 	   	  {
 	   		  while(true)
 	   		  {
 		   		  if(connectionStatus == CONNECTED)
 				  {
 		   			  try
 		   			  {
 		   				  String ping = CommandsHandler.getPingCommand();
 		   				  sendCommand(ping);
 		   			  }
 		   			  catch (Exception e) 
 		   			  {
 		   				  Logger.write("communication_stresstester", String.format("Ping command format Exception >> %s - host %s - port %s", e.getMessage(),hostIP,port));
 		   				  Logger.writeError("communication_stresstester", e);
 		   			  }
 				  }
 		   		 try {
 			          Thread.sleep(5 * 1000);
 			      }catch (InterruptedException e) {}
 	   		  }
 	   	  }
 	   };
 	   outStreamThread.start();
 	   */
 	   
	}
	private void startinStreamThread() {
		inStreamThread = new Thread()
 	    {
     	  public void run()
     	  {
 		  	   while(true)
 		  	   {
 		  		 try 
 	     		  { 
	         		  if(connectionStatus == CONNECTED && input.ready())
	         		  {
	         			  String readData = input.readLine(); 
	         			  //System.out.println("Received: "+ readData);
	         			  if (readData.indexOf("heartbeat") == -1 && !ApplicationUtility.isNullOrEmpty(readData))
	         				  Logger.write("communication_stresstester",  "Received data >> " + readData);
	         		  }
 	     		  }
         		 catch (IOException e) 
        		 {
                	  Logger.write("communication_stresstester", String.format("exception >> >> %s - host %s - port %s", e.getMessage(),hostIP,port));
                	  Logger.writeError("communication_stresstester", e);
                      cleanUp();
                      connectionStatus = DISCONNECTED;
                 }
         		 try 
         		 { // Poll every ~1000 ms
                     Thread.sleep(100);
                 }
                 catch (InterruptedException e) 
         		 {
                	   Logger.write("communication_stresstester", String.format("exception >> >> %s - host %s - port %s", e.getMessage(),hostIP,port));
                 	   Logger.writeError("communication_stresstester", e);
                 }
			  } 
     	  }
 	   };
 	   inStreamThread.start();
	}
	private static boolean isSocketAliveUitlitybyCrunchify(String hostName, int port,String logFileName) {
		boolean isAlive = false;
 
		// Creates a socket address from a hostname and a port number
		SocketAddress socketAddress = new InetSocketAddress(hostName, port);
		Socket socket = new Socket();
 
		// Timeout required - it's in milliseconds
		int timeout = 2000;
		try {
			socket.connect(socketAddress, timeout);
			socket.close();
			isAlive = true;
 
		} catch (SocketTimeoutException exception) {
			Logger.write(logFileName,"SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
		} catch (IOException exception) {
			Logger.write(logFileName,
					"IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
		}
		return isAlive;
	}
	public boolean sendCommand(String command)
    {
		String logFileName = "communication_stresstester";
		if(command.indexOf(CommandsHandler.PING) != -1)
    		logFileName += "_ping";
    	try 
        {   
     	   //synchronized (output) 
     	   {
     		  //if(isSocketAliveUitlitybyCrunchify(hostIP, port,logFileName))
     		  //{
	         	   //output.writeBytes(command);
     			   output.println(command);
	     		   Logger.write(logFileName, "Send >> " + command);
	         	   return true;
         	 // }
     		 // else
     			//  throw new SocketException("Connection to server not available!");
     	   }
        }
    	/*
    	catch (SocketException e) 
    	{
      	   Logger.write(logFileName, String.format("exception >> >> %s - host %s - port %s", e.getMessage(),hostIP,port));
      	   Logger.writeError(logFileName, e);
            cleanUp();
            connectionStatus = DISCONNECTED;
         }
         catch (IOException e) 
    	{
       	   Logger.write(logFileName, String.format("exception >> >> %s - host %s - port %s", e.getMessage(),hostIP,port));
       	   Logger.writeError(logFileName, e);
             cleanUp();
             connectionStatus = DISCONNECTED;
        }*/
        catch (Exception e) 
        {
     	   Logger.write(logFileName, String.format("exception >> >> %s - host %s - port %s", e.getMessage(),hostIP,port));
     	   Logger.writeError(logFileName, e);
           cleanUp();
           connectionStatus = DISCONNECTED;
        }
    	return false;
    }

    // Cleanup for disconnect
    private  void cleanUp() {

       try {
          if (socket != null) {
             socket.close();
             socket = null;
          }
       }
       catch (IOException e) { socket = null; }

       try {
          if (input != null) {
        	  input.close();
        	  input = null;
          }
       }
       catch (IOException e) { input = null; }
       
       if (output != null) {
    	   output.close();
    	   output = null;
       }
       /*
       try {
           if (output != null) {
        	   output.close();
        	   output = null;
           }
        }
        catch (IOException e) { output = null; }
        */
    }
}
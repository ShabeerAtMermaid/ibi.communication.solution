package com.multiq.ibi.communicator.client;
import java.util.Random;

import shared.AppSettings;
import shared.Logger;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args ) throws InterruptedException
    {
        Logger.write("communication_stresstester", "-----------------------------------------------");

        for(int i=1; i <= AppSettings.getNumberofBuses() ;i++)
        {
    		TCPClient client = new TCPClient();
    		//client.startCommunication(BusNumber());
    		client.startCommunication(AppSettings.getStartBusNumber()+i-1);
    		Thread.sleep(500);
	        Logger.write("communication_stresstester", "communicator client "+ i +" service has been started!!!");
        }
        /*
        for(int i=1; i <= 5 ;i++)
        {
    		TCPClient client = new TCPClient();
    		//client.startCommunication(BusNumber());
    		client.startCommunication(AppSettings.getStartBusNumber()+i-1);
    		Thread.sleep(500);
	        Logger.write("communication_stresstester", "communicator client "+ i +" service has been started!!!");
        }
        */
        /*
         for(int i=1; i <= AppSettings.getNumberofBuses() ;i++)
        {
    		TCPClient client = new TCPClient();
    		//client.startCommunication(BusNumber());
    		client.startCommunication(AppSettings.getStartBusNumber()+i-1);
    		Thread.sleep(500);
	        Logger.write("communication_stresstester", "communicator client "+ i +" service has been started!!!");
        }
        */

        /*
        for(int i=1; i <= 5 ;i++)
        {
    		TCPClient client = new TCPClient();
    		//client.startCommunication(BusNumber());
    		client.startCommunication(AppSettings.getStartBusNumber()+i-1);
    		Thread.sleep(500);
	        Logger.write("communication_stresstester", "communicator client "+ i +" service has been started!!!");
        }
        */
    }
    public static int BusNumber() 
    {
    	Random rand = new Random();
    	//return rand.nextInt(8000 - 7000 + 1) + 7000;
    	return rand.nextInt(1000 - 1 + 1) + 1;
    }
}

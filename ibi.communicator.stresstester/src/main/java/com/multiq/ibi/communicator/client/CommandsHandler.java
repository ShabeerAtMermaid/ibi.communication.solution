package com.multiq.ibi.communicator.client;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

import shared.AppSettings;
import shared.Logger;


public class CommandsHandler 
{  
    public static final String PING = "ping";
    public static final String CALL = "call";
    public static final String HANGUP = "hangup";
    public static final String PANIC = "panic";
    public static final String CONFERENCE = "conference";
    
    public static final String INCALL = "1";
    public static final String NOTINCALL = "0";
    
    public static final String RINGING = "1";
    public static final String RINGINGEND = "0";
    
    public static final String PANICSTATE = "1";
    public static final String PANICLISTENSTATE = "2";
    public static final String PANICENDSTATE = "0";
    public static final String PANICCANCEL = "1";
    
    public static final String ACCEPT = "accept";
    public static final String REJECT = "reject";
    public static final String END = "end";
    public static final String DECLINE = "decline";
    public static final String ACK = "ack";
    public static final String RINGINGSTATUS = "";
    
    public static final String TYPE = "bus";
     
    /*public static String issueToken() 
    {
    	Random random = new SecureRandom();
    	String token = new BigInteger(130, random).toString(32);
    	return token;
    }*/
    
    public static float issueToken() 
    {
    	Random rand = new Random();
    	float fl = rand.nextFloat(); 
    	return fl/1000;
    }
    
    public static int BusNumber() 
    {
    	Random rand = new Random();
    	return rand.nextInt(8000 - 7000 + 1) + 7000;
    }
    
    public static int journeyId() 
    {
    	Random rand = new Random();
    	return rand.nextInt(1250 - 1000 + 1) + 1000;
    }
    
    /*
    public static String getPingCommand(int busNumber)
    {
    	String pingCommand = "{" + 
    			"\"command\":\"" + PING + "\"," + 
    			"\"type\":\"" + TYPE + "\"," +
    			"\"customer\":\"2164\","  + 
    			//"\"vehiclenumber\":\""+ BusNumber() + "\","  + 
    			"\"vehiclenumber\":\""+ busNumber + "\","  +
    			"\"line\":\"2A\","  + 
    			"\"destination\":\"XYZ\","  +    			
    			"\"ptajourneyid\":\""+ journeyId() +"\","  +
    			"\"journeytype\":\"J\","  +
    			"\"lat\":\"" + String.valueOf(55 + issueToken())  + "\","  + 
    			"\"long\":\"" + String.valueOf(12 + issueToken()) + "\""  + 
    			 "}" + System.lineSeparator();
    	return pingCommand;
    }
    */
    
    public static String getPingCommand(int busNumber,float lat, float lng)
    {
    	String pingCommand = "{" + 
    			"\"command\":\"" + PING + "\"," + 
    			"\"type\":\"" + TYPE + "\"," +
    			"\"customer\":\"2164\","  + 
    			//"\"vehiclenumber\":\""+ BusNumber() + "\","  + 
    			"\"vehiclenumber\":\""+ busNumber + "\","  +
    			"\"line\":\"2A\","  + 
    			"\"destination\":\"XYZ\","  +    			
    			"\"ptajourneyid\":\""+ journeyId() +"\","  +
    			"\"journeytype\":\"J\","  +
    			"\"lat\":\"" + String.valueOf(lat)  + "\","  + 
    			"\"long\":\"" + String.valueOf(lng) + "\""  + 
    			 "}" + System.lineSeparator();
    	return pingCommand;
    }
    
    public static String getPanicCommandForUI(String panicState)
    {
		String jsonString =  "{";
			   jsonString += "\"panicstate\":\""+ panicState +"\"";
			   jsonString += "}";
    	return jsonString;
    }
    
    public static String getPushtoTalkCommandForUI(String pushtotalk)
    {
		String jsonString =  "{";
			   jsonString += "\"pushtotalk\":\""+ pushtotalk +"\"";
			   jsonString += "}";
    	return jsonString;
    }
    /*
    public static void sendIncomingCallResponse(String incomingCallResp)
    {
    	incomingCallResp += System.lineSeparator();
    	Logger.write("communication_clientservice", "Client service to BOT >>Command ready to send " + incomingCallResp);
    	sendCommandToBot(incomingCallResp);
    }
   
    private static void sendCommandToBot(String command)
    {
    	class ThreadTask implements Runnable 
		{
			private String command;
			public ThreadTask(String c)
			{
				command = c;
			}
			public void run()
			{
				int attemps = 1;
				while(attemps <= 5)
				{
					try 
					{
						Logger.write("communication_clientservice", "Client service to BoT >>Attemp # "+ attemps +" Trying to send command " + this.command);
						boolean isSend = TCPClient.sendCommand(this.command);
						if (isSend)
						{
							Logger.write("communication_clientservice", "Client service to BoT >>Command has been send!!!");
							return;
						}
						Logger.write("communication_clientservice", "Failed to send command, will try after 1 sec ");
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						Logger.write("communication_clientservice", "Exception Failed to send command, will try after 1 sec ");
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {} 
					attemps += 1;
				}
			}
		}
		Thread t = new Thread(new ThreadTask(command));
		t.start();
    }
    */
    /*
    JSONObject obj = new JSONObject();
    obj.put("name", "mkyong.com");
    obj.put("age", new Integer(100));

    JSONArray list = new JSONArray();
    list.add("msg 1");
    list.add("msg 2");
    list.add("msg 3");
    obj.put("messages", list);
    
    try (FileWriter file = new FileWriter("f:\\test.json")) {
        file.write(obj.toJSONString());
        file.flush();
    } catch (IOException e) {
        e.printStackTrace();
    }
    System.out.print(obj);
    {
    	"age":100,
    	"name":"mkyong.com",
    	"messages":["msg 1","msg 2","msg 3"]
    }
    */
}


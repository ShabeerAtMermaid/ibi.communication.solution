package models;

public class MapVehicle 
{
	private String vehicleNumber;
	private int customerId;
	private String responseData = "";
	
	public MapVehicle(String vNumber, int cId)
	{
		this.vehicleNumber = vNumber;
		this.customerId = cId;
	}
	
	public void setVehicleNumber(String value)
	{
		this.vehicleNumber = value;
	}
	
	public String getVehiclenumber()
	{
		return this.vehicleNumber;
	}
	
	public void setCustomerId(int value)
	{
		this.customerId = value;
	}
	public int getCustomerId()
	{
		return this.customerId;
	}
	
	public void setResponseData(String value)
	{
		this.responseData = value;
	}
	public String getResponseData()
	{
		return this.responseData;
	}
}

package models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.websocket.Session;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class MapSubscriber 
{
	private Session session;
	private String operator;
	private List<MapVehicle> vehicleList;
	private JSONArray vehicleElements;
	private String responseData; 
	
	public MapSubscriber(Session s,String op,JSONArray vElements)
	{
		this.session = s;
		this.operator = op;
		this.vehicleElements = vElements;
		this.vehicleList = parseVehiclesObject(this.vehicleElements);
	}
	
	public Session getSession()
	{
		return session;
	}
	
	public void setSession(Session value)
	{
		session = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String value) {
		this.operator = value;
	}
	
	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String value) {
		this.responseData = value;
	}
	
	public List<MapVehicle> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(List<MapVehicle> value) {
		this.vehicleList = value;
	}
	
	public void setVehicleList(JSONArray vElements) {
		this.vehicleElements = vElements;
		this.vehicleList = parseVehiclesObject(this.vehicleElements);
	}
	
	private List<MapVehicle> parseVehiclesObject(JSONArray vehicleElements)
	{
		List<MapVehicle> vehicleList = new ArrayList<MapVehicle>();
		try
		{
			/*Iterator i = vehicleElements.i.iterator();
			while (i.hasNext()) 
			{
				JsonObject innerObj = (JsonObject) i.next();
				MapVehicle vehicle = new MapVehicle(innerObj.get("vehiclenumber").getAsString(),Integer.parseInt((innerObj.get("customer").getAsString())));
				vehicleList.add(vehicle);
			}*/
			for (int i = 0; i < vehicleElements.length(); i++) {
				JSONObject innerObj = vehicleElements.getJSONObject(i);
				MapVehicle vehicle = new MapVehicle(innerObj.getString("vehiclenumber"),Integer.parseInt((innerObj.getString("customerid"))));
				vehicleList.add(vehicle);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return vehicleList;
	}
}

package ibi.communicator.websocket;

public class CommandsHandler {
	public static final String PING = "ping";
    public static final String CALL = "call";
    public static final String HANGUP = "hangup";
    public static final String PANIC = "panic";
    public static final String CONFERENCE = "conference";
    public static final String CONNECTION = "connection";
    public static final String HEARTBEAT = "heartbeat";

    public static final String SUBSCRIPTION = "subscription";
    public static final String UNSUBSCRIPTION = "unsubscription";
    public static final String MAPS = "maps";

    
    public static final String INCALL = "1";
    public static final String NOTINCALL = "0";
    
    public static final String PANICSTATE = "1";
    public static final String PANICLISTENSTATE = "2";
    public static final String PANICENDSTATE = "0";
    public static final String PANICCANCEL = "1";
    
    public static final String ACCEPT = "accept";
    public static final String REJECT = "reject";
    public static final String END = "end";
    public static final String DECLINE = "decline";
    public static final String BUSY = "busy";
    
    public static final String TYPE = "bus";
    
    

}

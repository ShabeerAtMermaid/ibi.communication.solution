package ibi.communicator.websocket;

import javax.websocket.Session;

public class WebsocketSession {
	private String status;
	private Session session;
	private String customerId;
	
	public String getStatus()
	{
		return status;
	}
	
	public void setStatus(String value)
	{
		status = value;
	}
	
	public Session getSession()
	{
		return session;
	}
	
	public void setSession(Session value)
	{
		session = value;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}

package ibi.communicator.websocket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.websocket.Session;

import shared.DatabaseManager;
import shared.Logger;


public class WebSocketHandler 
{	
	public WebSocketHandler() {}
	
	public static void broadcastToBrowsers(String key, String message)
	{
		try
		{
			if (!DatabaseManager.IsOperatorCanReceiveCall(key))
			{
				if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", String.format("The operator %s is not enabled to receive calls", key));
				return;
			}
			if (message == null || message.isEmpty()) return;
			if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> message ... " + message + " for Browser " + key);
			/*for (Map.Entry<String, Object> entry2 : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
			{
				if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Session user: " + entry2.getKey());
			}*/
			if (WebSocketServerEndpoint.webUserSessionsMap.containsKey(key))
			{
				if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> Checking browser " + key + " websocket session existance in the list");
				//Session session = (Session)WebSocketServerEndpoint.webUserSessionsMap.get(key);
		    	WebsocketSession wsSession = (WebsocketSession)WebSocketServerEndpoint.webUserSessionsMap.get(key);
		    	Session session = wsSession.getSession();

				if (session != null && session.isOpen()) 
		        {
					if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> Browser " + key + " Session exist and opened as well");
					session.getBasicRemote().sendText(message);
					if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> message " + message +" has seen send to browser " + key);
				}
				else
				{
					if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> Browser " + key + " Session exist and not opened so cleanup performing against it");
					if (session != null)
						session.close();
					WebSocketServerEndpoint.webUserSessionsMap.remove(key);
					if (message.indexOf("heartbeat") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> Browser " + key + " Session closed successfully and removed from sessions list");
				}
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "Bot to Websocket Way >> Exceptoin Browser " + key + " Session " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
	}
	public static void broadcastToAllBrowsers(String customerId, String op, String message)
	{
		try
		{
			if (message == null || message.isEmpty()) return;
			if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "Bot to Websocket Way >> Message " + message + " for all Browsers");
			if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "Currently Open/Connected Sessions ");
			/*for (Map.Entry<String, Object> entry2 : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
			{
				Logger.write("communication_websocketserver", "Session user: " + entry2.getKey());
			}
			*/
			if (WebSocketServerEndpoint.webUserSessionsMap != null && WebSocketServerEndpoint.webUserSessionsMap.size() > 0)
			{
				List<String> userIdsToRemove = new ArrayList<String>();
				if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Iterating all sessions");
			    for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
				{
			    	if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", String.format("customerId %s ==> op %s ==> entry.getKey() %s", customerId,op,entry.getKey()));
			    	if (!DatabaseManager.IsOperatorCanReceiveCall(entry.getKey()))
			    	{
			    		if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", String.format("!DatabaseManager.IsOperatorCanReceiveCall(entry.getKey() ==>>>>   customerId %s ==> op %s ==> entry.getKey() %s", customerId,op,entry.getKey()));
			    		continue;
			    	}
			    	if (op.isEmpty() || !op.equalsIgnoreCase(entry.getKey()))
			    	{
			    		//Session session = (Session)entry.getValue();
				    	WebsocketSession wsSession = (WebsocketSession)entry.getValue();
				    	if (!customerId.equalsIgnoreCase(wsSession.getCustomerId()))
				    	{
				    		if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", String.format("Operator %s ignored because it is logged in with customer %s, current call customer: %s", entry.getKey(), wsSession.getCustomerId(), customerId));
				    		continue;
				    	}
				    	Session session = wsSession.getSession();
				    	
				    	if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " and key: "+ entry.getKey() + " >> found in the list");
						if (session != null && session.isOpen()) 
				        {
							if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  connection is opened");
							session.getBasicRemote().sendText(message);
							if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >> and message send " + message);
						}
						else
						{
							if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  connection is not opened so cleanup is performing against it");
							if (session != null)
								session.close();
							//WebSocketServerEndpoint.webUserSessionsMap.remove(entry.getKey());
							userIdsToRemove.add(entry.getKey());
							if (message.indexOf("vehiclelist") == -1) Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  has closed successfully and removed from sessions list as well");
						}
			    	}
					
				}
			    if (userIdsToRemove.size() > 0){
			    	for (String user: userIdsToRemove){
			    		WebSocketServerEndpoint.webUserSessionsMap.remove(user);
			    	}
			    }
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Exceptoin for all Browsers use exception text " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
	}
	public static boolean isAllOperatorsBusy(String customerId)
	{
		try
		{
			if (WebSocketServerEndpoint.webUserSessionsMap != null && WebSocketServerEndpoint.webUserSessionsMap.size() > 0)
			{
				Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Iterating all sessions");
				List<String> userIdsToRemove = new ArrayList<String>();
			    for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
				{
			    	if (!DatabaseManager.IsOperatorCanReceiveCall(entry.getKey())){
			    		continue;
			    	}
					//Session session = (Session)entry.getValue();
			    	WebsocketSession wsSession = (WebsocketSession)entry.getValue();
			    	if (!customerId.equalsIgnoreCase(wsSession.getCustomerId()))
			    	{
			    		continue;
			    	}
			    	Session session = wsSession.getSession();
					Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Session Id : " + session.getId() + " and key: "+ entry.getKey() + " >> found in the list");
					if (session != null && session.isOpen()) 
			        {
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Session Id : " + session.getId() + " >> " + wsSession.getStatus());
						if (!(wsSession.getStatus().equalsIgnoreCase(CommandsHandler.BUSY) || 
								wsSession.getStatus().equalsIgnoreCase(CommandsHandler.REJECT)))
							return false;
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Session Id : " + session.getId() + " >> is " + wsSession.getStatus());
					}
					else
					{
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Session Id : " + session.getId() + " >>  connection is not opened so cleanup is performing against it");
						if (session != null)
							session.close();
						//WebSocketServerEndpoint.webUserSessionsMap.remove(entry.getKey());
						userIdsToRemove.add(entry.getKey());
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Session Id : " + session.getId() + " >>  has closed successfully and removed from sessions list as well");
					}
				}
			    if (userIdsToRemove.size() > 0){
			    	for (String user: userIdsToRemove){
			    		WebSocketServerEndpoint.webUserSessionsMap.remove(user);
			    	}
			    }
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "WebSocket to Browser Way >> isAllOperatorsBusy >> Exceptoin for all Browsers use exception text " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
		return true;
	}
	public static void changeOperatorStatus(String operator,String status)
	{
		try
		{
			if (WebSocketServerEndpoint.webUserSessionsMap != null && WebSocketServerEndpoint.webUserSessionsMap.size() > 0)
			{
				//Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> finding operator : " + operator);
				if (WebSocketServerEndpoint.webUserSessionsMap.containsKey(operator))
				{
					//Session session = (Session)entry.getValue();
					WebsocketSession wsSession = (WebsocketSession)WebSocketServerEndpoint.webUserSessionsMap.get(operator);
			    	Session session = wsSession.getSession();
					Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Session Id : " + session.getId() + " and operator: "+ operator + " >> found in the list");
					if (session != null && session.isOpen()) 
			        {
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Session Id : " + session.getId() + " >>  connection is opened");
						wsSession.setStatus(status);
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Session Id : " + session.getId() + " >> and status " + status + " has been chanaged");
					}
					else
					{
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Session Id : " + session.getId() + " >>  connection is not opened so cleanup is performing against it");
						if (session != null)
							session.close();
						WebSocketServerEndpoint.webUserSessionsMap.remove(operator);
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Session Id : " + session.getId() + " >>  has closed successfully and removed from sessions list as well");
					}
				}
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Exceptoin for all Browsers use exception text " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
	}
	
	public static void changeAllOperatorStatus(String customerId, String status)
	{
		try
		{
			if (WebSocketServerEndpoint.webUserSessionsMap != null && WebSocketServerEndpoint.webUserSessionsMap.size() > 0)
			{
				for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
				{
					//Session session = (Session)entry.getValue();
			    	WebsocketSession wsSession = (WebsocketSession)entry.getValue();
			    	if (!customerId.equalsIgnoreCase(wsSession.getCustomerId()))
			    	{
			    		continue;
			    	}
			    	Session session = wsSession.getSession();
			    	if (session != null && session.isOpen()) 
			        {
						wsSession.setStatus(status);
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Changed status to " + status + " for " + entry.getKey());

					}
					else
					{
						if (session != null)
							session.close();
						WebSocketServerEndpoint.webUserSessionsMap.remove(entry.getKey());
					}
				}
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "WebSocket to Browser Way >> changeOperatorStatus >> Exceptoin for all Browsers use exception text " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
	}
	public static void broadcastToSpecificOperators(String message)
	{
		try
		{
			if (message == null || message.isEmpty()) return;
			Logger.write("communication_websocketserver", "Bot to Websocket Way >> Message " + message + " for all Browsers");
			Logger.write("communication_websocketserver", "Currently Open/Connected Sessions ");
			/*for (Map.Entry<String, Object> entry2 : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
			{
				Logger.write("communication_websocketserver", "Session user: " + entry2.getKey());
			}*/
			if (WebSocketServerEndpoint.webUserSessionsMap != null && WebSocketServerEndpoint.webUserSessionsMap.size() > 0)
			{
				List<String> userIdsToRemove = new ArrayList<String>();
				Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Iterating all sessions");
			    for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet())
				{
			    	Logger.write("communication_websocketserver", String.format("entry.getKey() %s ",entry.getKey()));
			    	if (DatabaseManager.IsOperatorCanReceiveCall(entry.getKey()))
			    	{
						//Session session = (Session)entry.getValue();
				    	WebsocketSession wsSession = (WebsocketSession)entry.getValue();
				    	Session session = wsSession.getSession();
	
						Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " and key: "+ entry.getKey() + " >> found in the list");
						if (session != null && session.isOpen()) 
				        {
							Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  connection is opened");
							session.getBasicRemote().sendText(message);
							Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >> and message send " + message);
						}
						else
						{
							Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  connection is not opened so cleanup is performing against it");
							if (session != null)
								session.close();
							//WebSocketServerEndpoint.webUserSessionsMap.remove(entry.getKey());
							userIdsToRemove.add(entry.getKey());
							Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Session Id : " + session.getId() + " >>  has closed successfully and removed from sessions list as well");
						}
			    	}
				}
			    if (userIdsToRemove.size() > 0){
			    	for (String user: userIdsToRemove){
			    		WebSocketServerEndpoint.webUserSessionsMap.remove(user);
			    	}
			    }
			}
		}
		catch(Exception ex)
		{
			Logger.write("communication_websocketserver", "WebSocket to Browser Way >> Exceptoin for all Browsers use exception text " + ex.getMessage());
			Logger.writeError("communication_websocketserver", ex);
		}
	}
}

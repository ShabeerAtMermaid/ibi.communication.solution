package ibi.communicator.websocket;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

public class ClientSessionHandler extends IoHandlerAdapter {
	@Override
	      public void sessionOpened(IoSession session) {

	      }
	  
	      @Override
	      public void messageReceived(IoSession session, Object message) {
	          
	      }
	  
	      @Override
	      public void exceptionCaught(IoSession session, Throwable cause) {
	          session.closeNow();
	      }
}

package ibi.communicator.websocket;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import shared.AppSettings;
import shared.ApplicationUtility;
import shared.DatabaseManager;
import shared.Logger;

@ServerEndpoint(value = "/server")
public class WebSocketServerEndpoint 
{

	public static HashMap<String, Object> webUserSessionsMap = new HashMap<String, Object>();
	String logFileName = "communication_websocketserver";
	
	@OnOpen
	public void onOpen(Session session) throws Exception {
		
		Logger.write(logFileName,
		"WebSocket >> Connected ... " + session.getId());
	}

	@OnError
    public void onError(Throwable error) {
		Logger.write(logFileName, "An error is thrown " + error.getMessage());
	}

	@OnMessage
	public void onMessage(String message, Session session) {
		logFileName = "communication_websocketserver";
		Logger.write(logFileName, "************* received");
		Logger.write(logFileName,String.format("************* %s", message));
		Logger.write(logFileName, "************* received");
		if (!ApplicationUtility.isNullOrEmpty(message)) {
			String key = "";
			String commandText = "";
			String statusText = "";
			String customerId = "";
			boolean onlyOneOperatorForThisCall = false;
			JSONObject jsonObj = null;
			
			try {
				jsonObj = new JSONObject(message);
				commandText = jsonObj.getString("command"); // call
				
				if (jsonObj.has("customer"))
				{
					customerId = jsonObj.getString("customer");
				}
				else if (jsonObj.has("acceptedfrom"))
				{
					JSONObject acceptdFrom = jsonObj.getJSONObject("acceptedfrom");
					customerId = acceptdFrom.getString("customer");
				}	
				
				if (commandText.equalsIgnoreCase(CommandsHandler.HEARTBEAT)) {
					logFileName += "_heartbeat";
				}
				Logger.write(logFileName, "Browser to Websocket Way >>>> message " + message + " received");
				if (commandText != null && !commandText.equalsIgnoreCase("connection")) {
					statusText = jsonObj.getString("status");
				}
				Logger.write(logFileName, "Browser to Websocket Way >>>> Check if it is from operator");
				if (jsonObj.has("webuser")) {
					Logger.write(logFileName, "Browser to Websocket Way >>>> This call originated from operator");
					key = jsonObj.getString("webuser");
					onlyOneOperatorForThisCall = true;
				} else {
					Logger.write(logFileName, "Browser to Websocket Way >>>> This call originated from vehicle or bot");
					if (ApplicationUtility.isNullOrEmpty(key)) {
						Logger.write(logFileName, "Browser to Websocket Way >>>> acceptedfrom : ");
						if (jsonObj.has("acceptedfrom")) {
							JSONObject acceptedFrom = jsonObj.getJSONObject("acceptedfrom");
							key = acceptedFrom.getString("webuser");
						}
					}
				}
				Logger.write(logFileName, "key " + key);

			} catch (JSONException e) {
				// Logger.write(logFileName, "Browser to
				// Websocket Way >>>> json object parsing");
			}
			WebSocketHandler.changeOperatorStatus(key, statusText);
			Logger.write(logFileName, "Browser to Websocket Way >>>> commandText : " + commandText + " statusText : "
					+ statusText + " key : " + key);
			if (commandText.equalsIgnoreCase(CommandsHandler.CONNECTION)) {
				Logger.write(logFileName,
						"Browser to Websocket Way >>>> Browser session  " + key + " not exist in list");
				// webUserSessionsMap.put(key, session);
				WebsocketSession wsSession = new WebsocketSession();
				wsSession.setSession(session);
				wsSession.setStatus(CommandsHandler.CONNECTION);
				wsSession.setCustomerId(customerId);
				// webUserSessionsMap.put(key, session);
				webUserSessionsMap.put(key, wsSession);
				MapsModuleHandler.Subscribe(key,session);
				Logger.write(logFileName,"Browser to Websocket Way >>>> Browser session  " + key + " has added in list");
				return;
			}
			else if (commandText.equalsIgnoreCase(CommandsHandler.SUBSCRIPTION)) 
			{
				Logger.write(logFileName, "Browser to Websocket Way >>>> Browser session  " + key + " for subcription command " + commandText);
				if (jsonObj.has("module"))
				{
					String moduleName = "";
					try 
					{
						moduleName = jsonObj.getString("module");
						String operatr = jsonObj.getString("webuser");
						switch(moduleName)
						{
							case CommandsHandler.MAPS:
								JSONArray jsonArray  = (JSONArray)jsonObj.get("buses");
								Logger.write(logFileName, String.format("Browser to Websocket Way >>>>operator %s has requested for subscriber %s " ,operatr,CommandsHandler.MAPS));
								if (jsonArray == null || jsonArray.length() == 0)
								{
									//MapsModuleHandler.Unsubscribe(operatr);
									message = message.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
									session.getBasicRemote().sendText(message);
									Logger.write(logFileName, "Browser to Websocket Way >>>> Subscriber "+ message +" for module " + CommandsHandler.MAPS + " has been send");
									MapsModuleHandler.Subscribe(operatr,session);
								}
								else
								{
									MapsModuleHandler.Subscribe(operatr,jsonArray);
									//MapsModuleHandler.Subscribe(session, operatr, jsonArray);
								}
								break;
						}
					} 
					catch (Exception e) 
					{
						Logger.write(logFileName,String.format("Browser to Websocket Way >>>> Exception %s in subscription %s", moduleName,e.getMessage()));
					}
				}
				return;
			}
			else if (commandText.equalsIgnoreCase(CommandsHandler.UNSUBSCRIPTION)) 
			{
				Logger.write(logFileName, "Browser to Websocket Way >>>> Browser session  " + key + " for subcription command " + commandText);
				if (jsonObj.has("module"))
				{
					String moduleName = "";
					try 
					{
						moduleName = jsonObj.getString("module");
						String operatr = jsonObj.getString("webuser");
						switch(moduleName)
						{
							case CommandsHandler.MAPS:
								Logger.write(logFileName, String.format("Browser to Websocket Way >>>>operator %s has requested for un-subscriber %s " ,operatr,CommandsHandler.MAPS));
								MapsModuleHandler.Unsubscribe(operatr);
								break;
						}
					} 
					catch (Exception e) 
					{
						Logger.write(logFileName,String.format("Browser to Websocket Way >>>> Exception %s in unsubscription %s", e.getMessage(),moduleName));
					}
				}
				return;
			}
			
			if ((commandText.equalsIgnoreCase(CommandsHandler.PANIC)
					|| commandText.equalsIgnoreCase(CommandsHandler.CALL))
					&& statusText.equalsIgnoreCase(CommandsHandler.ACCEPT)) 
			{
				Logger.write(logFileName, "Panic/Call accepted so broadcasting to browsers");
				WebSocketHandler.broadcastToAllBrowsers(customerId, key, message.replaceAll("\"ack\":\"false\",", ""));
				//WebSocketHandler.broadcastToAllBrowsers(customerId, "", message.replaceAll("\"ack\":\"false\",", ""));
			} 
			else if (commandText.equalsIgnoreCase(CommandsHandler.CALL)
					&& (statusText.equalsIgnoreCase(CommandsHandler.BUSY)
							|| statusText.equalsIgnoreCase(CommandsHandler.REJECT))) 
			{

				Logger.write(logFileName, "Command sending to BOT");
				//WebSocketHandler.changeOperatorStatus(key, statusText);
				if (!WebSocketHandler.isAllOperatorsBusy(customerId) && onlyOneOperatorForThisCall == false)
				{
					WebSocketHandler.broadcastToBrowsers(key, message.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\""));
					return;
				}
					
				WebSocketHandler.changeAllOperatorStatus(customerId, "");
			}
			sendCommandToRobotService(message, session, commandText);
			Logger.write(logFileName, "Command sent to BOT");
			
		}
	}

	@OnClose
	public void onClose(Session session, CloseReason closeReason) throws IOException {
		Logger.write(logFileName,
				"WebSocket >>" + String.format("Session %s closed because of %s", session.getId(), closeReason));
		session.close();
		String key = findOperator(session);
		if (WebSocketServerEndpoint.webUserSessionsMap != null
				&& WebSocketServerEndpoint.webUserSessionsMap.containsKey(key))
			WebSocketServerEndpoint.webUserSessionsMap.remove(key);

	}

	private String findOperator(Session s) {
		if (WebSocketServerEndpoint.webUserSessionsMap != null
				&& WebSocketServerEndpoint.webUserSessionsMap.size() > 0) {
			for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet()) {
				WebsocketSession webSession = (WebsocketSession) entry.getValue();
				if (webSession != null && webSession.getSession() == s) {
					return entry.getKey();
				}
			}
		}
		Logger.write(logFileName,
				"Web Socket to Update User Status >> could not find Operator User");
		return "";
	}

	private String findOperatorStatus(String key) {
		if (WebSocketServerEndpoint.webUserSessionsMap != null
				&& WebSocketServerEndpoint.webUserSessionsMap.size() > 0) {
			WebsocketSession webSession = (WebsocketSession) WebSocketServerEndpoint.webUserSessionsMap.get(key);
			if (webSession != null) {
				return webSession.getStatus();
			}
		}
		Logger.write(logFileName,
				"Web Socket to Update User Status >> coule not find operator status");
		return "";
	}

	private String findKey(Session s) {
		if (WebSocketServerEndpoint.webUserSessionsMap != null
				&& WebSocketServerEndpoint.webUserSessionsMap.size() > 0) {
			List<String> userIdsToRemove = new ArrayList<String>();
			for (Map.Entry<String, Object> entry : WebSocketServerEndpoint.webUserSessionsMap.entrySet()) {
				Session session = (Session) entry.getValue();
				if (session != null && session.isOpen()) {
					if (session.equals(s)) {
						String key = entry.getKey();
						Logger.write(logFileName,
								"Web Socket to Update User Status >> key found : " + key);
						return key;
					}
				} else {
					if (session != null)
						try {
							session.close();
						} catch (IOException e) {
							Logger.write(logFileName,
									"exception Web Socket to Update User Status>> " + e.getMessage());
						}
					// WebSocketServerEndpoint.webUserSessionsMap.remove(entry.getKey());
					userIdsToRemove.add(entry.getKey());
				}
			}
			if (userIdsToRemove.size() > 0) {
				for (String user : userIdsToRemove) {
					WebSocketServerEndpoint.webUserSessionsMap.remove(user);
				}
			}
		}
		Logger.write(logFileName, "Web Socket to Update User Status >> key NOT FOUND");
		return "";
	}
	
	
	

	private void sendCommandToRobotService(String command, Session session, String commandText) {
		class ThreadTask implements Runnable {
			private String command;
			private String commandText;
			private String hostIP = "localhost";
			private int port = AppSettings.isStaging() ? 8024 : 8026;
			private Socket socket;
			public PrintWriter output = null;
			private int attemptToSend = 1;
			private Session browserSession;

			public ThreadTask(String c, Session session, String ct) {
				command = c;
				browserSession = session;
				commandText = ct;
			}

			public void run() {
				String logFileNameheartbeat = "communication_websocketserver";
				if (commandText.equalsIgnoreCase(CommandsHandler.HEARTBEAT)) {
					logFileNameheartbeat += "_heartbeat";
				}
				Logger.write(logFileNameheartbeat, "Websocket to Bot Way >>>> command ... " + command);
				while (attemptToSend < 5) {
					try {
						socket = new Socket();
						socket.connect(new InetSocketAddress(hostIP, port), 10 * 1000);
						;
						Logger.write(logFileNameheartbeat, "Websocket to Bot Way >>>> connection establish");
						// output = new DataOutputStream(
						// socket.getOutputStream());
						output = new PrintWriter(
								new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8")), true);

						// output.writeBytes(command + System.lineSeparator());
						output.println(command + System.lineSeparator());
						Logger.write(logFileNameheartbeat, "Websocket to Bot Way >>>> command send " + command);
						return;

					} catch (Exception e) {
						Logger.write(logFileNameheartbeat, "Websocket to Bot Way >>>> Exception >> " + e.getMessage());
						Logger.writeError(logFileNameheartbeat, e);
					} finally {
						cleanUp();
					}

					try {
						Thread.sleep(1 * 1000);
					} catch (InterruptedException e) {
					}
					attemptToSend += 1;
				}
				try {
					Logger.write(logFileNameheartbeat, "Websocket to Browser Way >>>> Bot is offline/not available");
					command = "{\"command\":\"call\", \"status\":\"offline\", \"request\":" + command + "}";
					Logger.write(logFileNameheartbeat, "Websocket to Browser Way >>>> command " + command);
					browserSession.getBasicRemote().sendText(command);
				} catch (IOException e) {
					Logger.write(logFileNameheartbeat, "exception >> " + e.getMessage());
					Logger.writeError(logFileNameheartbeat, e);
				}
			}

			private void cleanUp() {
				try {
					if (socket != null) {
						socket.close();
						socket = null;
					}
				} catch (IOException e) {
					socket = null;
				}

				try {
					if (output != null) {
						output.close();
						output = null;
					}
				} catch (Exception e) {
					output = null;
				}
			}
		}
		Thread t = new Thread(new ThreadTask(command, session, commandText));
		t.start();
	}

	private String getWebUserFromCommand(String command) {
		if (!ApplicationUtility.isNullOrEmpty(command)) {
			JSONObject jsonObj;
			try {
				jsonObj = new JSONObject(command);
				return jsonObj.getString("webuser");
			} catch (JSONException e) {
			}
		}
		return "";
	}
}
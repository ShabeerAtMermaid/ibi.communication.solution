package ibi.communicator.websocket;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.websocket.Session;

import org.json.JSONArray;
import models.MapSubscriber;
import models.MapVehicle;
import shared.ApplicationUtility;
import shared.DatabaseManager;
import shared.Logger;

public class MapsModuleHandler {
	private static HashMap<String, Object> mapsSubscribers = new HashMap<String, Object>();
	private static String logFileName = "communication_websocketserver";

	public static void Subscribe(Session session, String operator, JSONArray vehicles) 
	{
		try 
		{
			synchronized (mapsSubscribers) 
			{
				/*
				 * if (mapsSubscribers.containsKey(operator)) {
				 * mapsSubscribers.remove(operator); }
				 */
				MapSubscriber mSubscriber = new MapSubscriber(session, operator, vehicles);
				mapsSubscribers.put(operator, mSubscriber);
				Logger.write(logFileName, "Websocket Level >>>> Browser session  " + operator + " has been subscribed");
			}
		}
		catch (Exception ex) 
		{
			Logger.write(logFileName,
					String.format("Websocket Level >>>> Subscribe Method >> Exception %s ", ex.getMessage()));
		}
	}
	
	public static void Subscribe(String operator, JSONArray vehicles) 
	{
		try
		{
			synchronized (mapsSubscribers) 
			{
				Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " is going to subscribe their vehicle");
				MapSubscriber mSubscriber = null;
				if (mapsSubscribers.containsKey(operator)) 
				{
					mSubscriber = (MapSubscriber)mapsSubscribers.get(operator);
					mSubscriber.setVehicleList(vehicles);
					Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " subscription already exist just updated new vehicles list");
				}
				else
				{
					mSubscriber = new MapSubscriber(null, operator, vehicles);
					Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " new subscription for vehicle created with socket session empty");
				}
				mapsSubscribers.put(operator, mSubscriber);
				Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " has been subscribed their vehicles");
			}
		}
		catch (Exception ex) 
		{
			Logger.write(logFileName,String.format("Websocket Level >>>> Vehicle subscription >> Exception %s ", ex.getMessage()));
		}
	}
	
	public static void Subscribe(String operator, Session session) {
		try 
		{
			Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " is going to subscribe");
			synchronized (mapsSubscribers) 
			{
				MapSubscriber mSubscriber = null;
				if (mapsSubscribers.containsKey(operator)) 
				{
					mSubscriber = (MapSubscriber)mapsSubscribers.get(operator);
					mSubscriber.setSession(session);
					Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " subscription already exist just updated socket session");
				}
				else
				{
					mSubscriber = new MapSubscriber(session, operator, null);
					Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + " new subscription created");
				}
				mapsSubscribers.put(operator, mSubscriber);
				Logger.write(logFileName, "Websocket Level >>>> Operator " + operator + "  has been subscribed successfully for map");
			}
		} 
		catch (Exception ex) 
		{
			Logger.write(logFileName,String.format("Websocket Level >>>> Socket session subscribe >> Exception %s ", ex.getMessage()));
		}
	}

	public static void Unsubscribe(String operator) {
		synchronized (mapsSubscribers) {
			try {
				if (mapsSubscribers.containsKey(operator)) {
					mapsSubscribers.remove(operator);
					Logger.write(logFileName,
							"Websocket Level >>>> Browser session  " + operator + " has been un-subscribed");
				}
			} catch (Exception ex) {
				Logger.write(logFileName,
						String.format("Websocket Level >>>> Unsubscribe Method >> Exception %s ", ex.getMessage()));
			}
		}
	}

	public static void sendMapDataThread() {
		Thread sendMapDataThread = new Thread() {
			public void run() {
				while (true) {
					try {
						List<String> userIdsToRemove = new ArrayList<String>();
						synchronized (mapsSubscribers) 
						{
							for (Map.Entry<String, Object> entry : mapsSubscribers.entrySet()) 
							{
								MapSubscriber mSubcriber = (MapSubscriber) entry.getValue();
								Session session = mSubcriber.getSession();
								if (session != null && session.isOpen()) 
								{
									String finalData = "";
									for (MapVehicle o : mSubcriber.getVehicleList()) 
									{
										int customer = o.getCustomerId();
										String vehicle = o.getVehiclenumber();
										String oldData = o.getResponseData();
										String newData = DatabaseManager.getVehicleMapData(customer, vehicle);
										if (!ApplicationUtility.isNullOrEmpty(newData)
												&& !oldData.equalsIgnoreCase(newData)) 
										{
											finalData += newData + ",";
											o.setResponseData(newData);
										}
									}
									if (!ApplicationUtility.isNullOrEmpty(finalData)) 
									{
										finalData = finalData.substring(0, finalData.lastIndexOf(","));
										finalData = "{\"command\":\"subscription\", \"ack\":\"true\", \"module\":\"maps\", \"webuser\":\""
												+ entry.getKey() + "\",\"buses\":[" + finalData + "]}";
										session.getBasicRemote().sendText(finalData);
										Logger.write(logFileName,
												String.format(
														"Websocket to Browser >>>> sendMapDataThread Method >> operator => %s AND data =>  %s ",
														entry.getKey(), finalData));
									}
								} else {
									if (session != null)
										session.close();
									userIdsToRemove.add(entry.getKey());
								}
							}
							if (userIdsToRemove.size() > 0) {
								for (String user : userIdsToRemove) {
									mapsSubscribers.remove(user);
								}
							}
						}
					} catch (IOException e) {
						Logger.write(logFileName, String.format(
								"Websocket Level >>>> sendMapDataThread Method >> Exception %s ", e.getMessage()));
					} finally {
						try {
							Thread.sleep(5 * 1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		};
		sendMapDataThread.start();
	}
}

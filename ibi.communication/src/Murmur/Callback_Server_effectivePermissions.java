// **********************************************************************
//
// Copyright (c) 2003-2016 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.3
//
// <auto-generated>
//
// Generated from file `Murmur.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package Murmur;

/**
 * Return users effective permissions
 **/

public abstract class Callback_Server_effectivePermissions
    extends IceInternal.TwowayCallback implements Ice.TwowayCallbackIntUE
{
    public final void __completed(Ice.AsyncResult __result)
    {
        ServerPrxHelper.__effectivePermissions_completed(this, __result);
    }
}

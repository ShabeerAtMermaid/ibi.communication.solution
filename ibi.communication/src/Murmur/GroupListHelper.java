// **********************************************************************
//
// Copyright (c) 2003-2016 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.6.3
//
// <auto-generated>
//
// Generated from file `Murmur.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package Murmur;

public final class GroupListHelper
{
    public static void
    write(IceInternal.BasicStream __os, Group[] __v)
    {
        if(__v == null)
        {
            __os.writeSize(0);
        }
        else
        {
            __os.writeSize(__v.length);
            for(int __i0 = 0; __i0 < __v.length; __i0++)
            {
                Group.__write(__os, __v[__i0]);
            }
        }
    }

    public static Group[]
    read(IceInternal.BasicStream __is)
    {
        Group[] __v;
        final int __len0 = __is.readAndCheckSeqSize(7);
        __v = new Group[__len0];
        for(int __i0 = 0; __i0 < __len0; __i0++)
        {
            __v[__i0] = Group.__read(__is, __v[__i0]);
        }
        return __v;
    }
}

package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class ApplicationUtility 
{
	public static boolean isNullOrEmpty(String text)
	{
		if(text == null || text.equals("") || text.equalsIgnoreCase("null"))
			return true;
		else
			return false;
	}
	public static String readCommWebsocket() throws Exception
	{
		try 
		{
			File file = new File(AppSettings.getCommWebsocketFilePath());
			if (file.exists())
			{
				String fileContent = readFileContents(file.getAbsolutePath()).trim();
				if (ApplicationUtility.isNullOrEmpty(fileContent))
				{
					return "";
				}	
				return fileContent;
			}
		} 
		catch (IOException e) 
		{
			Logger.writeError("dcu2_backendwebsocketservice", e);
		}
		return "";
	}
	
	public static String readFileContents(String filePath) throws Exception
	{
		File sourceFile = new File(filePath);
		FileInputStream xmlStream = new FileInputStream(sourceFile.getAbsolutePath());

		DataInputStream xmlIn = new DataInputStream(xmlStream);
		BufferedReader xmlBr = new BufferedReader(new InputStreamReader(xmlIn, Charset.forName("UTF-8")));
		
		String line;
		String xmlData = "";

		while((line = xmlBr.readLine()) != null)
		{
			xmlData += line;	
		}
		
		xmlIn.close();
		xmlStream.close();
		xmlData = xmlData.trim().replaceFirst("^([\\W]+)<","<");
		return xmlData;
	}
}

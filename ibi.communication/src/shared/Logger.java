package shared;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Logger {
	/*** Variables ***/
	private static SimpleDateFormat logDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static SimpleDateFormat logDateFormat = new SimpleDateFormat("yyyyMMdd");

	/*** Implementation ***/

	public static void writeError(String logName, Exception ex, Boolean stacktrace) {
		String message = "";

		Throwable exception = ex;

		while (exception != null) {
			message += ex.getMessage() + "\n";
			if (stacktrace) {
				message += Arrays.toString(ex.getStackTrace()) + "\n";
				message += getStackTrace(ex) + "\n";
				message += "--\n";
			}
			exception = exception.getCause();
		}

		write(logName, message);
	}

	public static void writeError(String logName, Exception ex) {
		String message = "";

		Throwable exception = ex;

		while (exception != null) {
			message += ex.getMessage() + "\n";
			// message += Arrays.toString(ex.getStackTrace()) + "\n";
			// message += getStackTrace(ex) + "\n";
			// message += "--\n";

			exception = exception.getCause();
		}

		write(logName, message);
	}

	public static void write(String logName, String text) {
		if (text != null && text.indexOf("heartbeat") == -1) {
			// writeDebug(text);
			writeDebug(logDateTimeFormat.format(new Date()) + "> " + text);
			if (AppSettings.isStaging()){
				logName += "_staging";
			}
			appendToLog(logName, logDateTimeFormat.format(new Date()) + "> " + text);
		}
	}

	public static void write(String logName, String text, Boolean trace) {
		try {
			if (trace) {
				writeDebug(text);

				appendToLog(logName, logDateTimeFormat.format(new Date()) + "> " + text);
			}
		} catch (Exception ex) {
			// ignore
		}
	}

	public static void writeDebug(String line) {
		String osName = System.getProperty("os.name");

		File debugFile = new File(AppSettings.resolveApplicationDirectory() + "debug");

		if (debugFile.exists() || osName.toLowerCase().indexOf("windows") != -1)
			System.out.println(line);
	}

	public static void clearOldLogs(String logName, int expireDays) {
		try {
			List<File> deleteList = new ArrayList<File>();

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, -1 * expireDays);
			Date expireDate = cal.getTime();

			File logDirectory = new File(resolveLogDirectoryPath());

			ensureDirectoryExist(logDirectory.getAbsolutePath());

			for (String logFilePath : logDirectory.list()) {
				boolean checkFile = false;

				if (!ApplicationUtility.isNullOrEmpty(logName) && logFilePath.startsWith(logName))
					checkFile = true;
				else if (ApplicationUtility.isNullOrEmpty(logName))
					checkFile = true;

				if (checkFile && logFilePath.endsWith(".log") && logFilePath.indexOf('_') != -1) {
					try {
						int logYearIndex = logFilePath.length() - 12;
						int logMonthIndex = logFilePath.length() - 8;
						int logDayIndex = logFilePath.length() - 6;

						int logYear = Integer.parseInt(logFilePath.substring(logYearIndex, logYearIndex + 4));
						int logMonth = Integer.parseInt(logFilePath.substring(logMonthIndex, logMonthIndex + 2));
						int logDay = Integer.parseInt(logFilePath.substring(logDayIndex, logDayIndex + 2));

						Date logDate = dateFormat.parse(logYear + "-" + logMonth + "-" + logDay);

						if (logDate.before(expireDate)) {
							File logFile = new File(logFilePath);

							deleteList.add(logFile);
						}
					} catch (Exception ex) {
						Logger.writeError("ibi", ex);
					}
				}
			}

			for (File logFile : deleteList) {
				String logFilePath = logFile.getAbsolutePath();

				if (logFile.delete()) {
					Logger.appendToLog("ibi", "Deleting log file: " + logFilePath);
					Logger.writeDebug("Deleting log file: " + logFilePath);
				}
			}
		} catch (Exception ex) {
			// SILENT
		}
	}

	public static void clearOldLogs(int expireDays) {
		clearOldLogs("", expireDays);
	}

	/*** Private helpers ***/
	private static void appendToLog(String logName, String line) {
		try {
			File logFile = new File(resolveLogFilePath(logName));
			if (logFile.length() < (20 * 1024 * 1024)) {
				ensureDirectoryExist(logFile.getParent());

				BufferedWriter bw = new BufferedWriter(new FileWriter(logFile.getAbsolutePath(), true));
				bw.write(line);
				bw.newLine();
				bw.flush();
				bw.close();

			}
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	private static String getStackTrace(Exception ex) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);

			return sw.toString();
		} catch (Exception ex2) {
			return "getStackTrace(..) failed!";
		}
	}

	/*** Resolves ***/
	private static String resolveRootPath() {
		String osName = System.getProperty("os.name");

		if (osName.toLowerCase().indexOf("windows") != -1)
			return "C:";
		else
			return "";
	}

	public static String resolveLogDirectoryPath() {
		return resolveRootPath() + "/storage/logs/";
	}

	private static String resolveLogFilePath(String logName) {
		return resolveLogDirectoryPath() + logName + "_" + logDateFormat.format(new Date()) + ".log";
	}

	private static void ensureDirectoryExist(String path) {
		ArrayList<String> missingPaths = new ArrayList<String>();

		File currentDirectory = new File(path);

		while (!currentDirectory.exists()) {
			missingPaths.add(0, currentDirectory.getAbsolutePath());

			currentDirectory = new File(currentDirectory.getParent());
		}

		for (int i = 0; i < missingPaths.size(); i++) {
			currentDirectory = new File(missingPaths.get(i));

			currentDirectory.mkdir();
		}
	}
}

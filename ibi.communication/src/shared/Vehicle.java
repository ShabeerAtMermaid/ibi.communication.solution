package shared;

public class Vehicle {
	private String customerid;
	private String vehiclenumber;
	private String status;

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getVehiclenumber() {
		return vehiclenumber;
	}

	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Vehicle() {

	}

	public Vehicle(String customerid, String vehicleno, String status) {
		this.customerid = customerid;
		this.vehiclenumber = vehicleno;
		this.status = status;
	}
}

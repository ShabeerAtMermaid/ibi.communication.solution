package shared;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Bot.BotServiceHandler;
import ibi.communicator.websocket.WebSocketHandler;
import ibi.communicator.websocket.WebSocketServerEndpoint;

public class DatabaseManager {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = AppSettings.isStaging()?"jdbc:mysql://localhost/communicationDEV":"jdbc:mysql://localhost/communication";

	// Database credentials
	static final String USER = AppSettings.isStaging()?"staging":"communication";
	static final String PASS = AppSettings.isStaging()?"staging123":"ibi0786";
	
	private static Map<String, String> busStatuses = new HashMap<String, String>();
	
	private static Map<Integer, Integer> callStatuses = new HashMap<Integer, Integer>();
	
	private static Map<String, String> customerMumbleHost = new HashMap<String, String>();
	private static Map<String, String> customerMumblePort = new HashMap<String, String>();
	private static Map<String, String> ibicustomerAgainstvTouchCustomer = new HashMap<String, String>();
	//private static Map<String, String> panicAllowedVehicles = new HashMap<String, String>();
	//private static Map<String, String> isOperatorsReceivedCall = new HashMap<String, String>();
	
	private static final String END = "end";
	private static final String REJECT = "reject";
	private static final String DECLINE = "decline";
	
	
	private static Connection conn = null;
	
	private static Connection getDBConnection() 
	{
		try {
			if (conn != null) return conn;
			// STEP 1: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			// STEP 2: Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Logger.write("communication_bot_DatabaseManage", String.format("DB_URL %s  -  USER %s  -  PASS %s", DB_URL,USER,PASS));
			return conn;
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception getDBConnection: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			Logger.write("communication_bot_DatabaseManage", "Exception getDBConnection: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		}
		return null;
	}
	
	/*
	public static void addEndCallLog(String from, String to, int customerId) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();
			stmt = conn.createStatement();
			String sql = "UPDATE calls set callstatus=2 where source='" + from + "' and dest='" + to
					+ "' and customerid='" + customerId + "'";
			stmt.executeUpdate(sql);

		} 
		catch (Exception ex) {
			ex.printStackTrace();
		} 
		finally {
			// finally block used to close resources
			cleanup(stmt,rs);
		}
	}
	
	public static void addStartCallLog(String from, String to, int customerId, String channel) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();
			stmt = conn.createStatement();
			String sql = "INSERT INTO calls (`customerid` ,`src` ,`dest` ,`type` ,`channel`) VALUES (" + customerId + ", '"
					+ from + "',  '" + to + "',  1,  '" + channel + "')";
			stmt.executeUpdate(sql);
		} catch (Exception ex) {
			Logger.write("communication_bot_DatabaseManage", "call to db " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		} finally {
			// finally block used to close resources
			cleanup(stmt,rs);
		}
	}
	*/
	public static boolean addPing(String customerid, String vehicleNumber, String line, String lat, String lng, String destination, String patjourneyid, String journeytype) {
		//if (true)
		//	return true;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		boolean gpsUpdated = true;
		try {
			if (lat == null){
				lat = "";
			}
			if (lng == null){
				lng = "";
			}
			if (destination == null){
				destination = "";
			}
			if (patjourneyid == null){
				patjourneyid = "";
			}
			if (journeytype == null){
				journeytype = "";
			}
			conn = getDBConnection();
			stmt = conn.createStatement();
			//Logger.write("communication_bot", "Check if customer already exists");
			String customerSQL = "select * from ibivtouchcustomers where customerid=" + customerid;
			rs = stmt.executeQuery(customerSQL);
			boolean bExist = rs.next();
			rs.close();
			if (!bExist) {
				Logger.write("communication_bot_DatabaseManage", "Customer does not exist so ignoring the ping");
				return false;
			}
			String sql = "SELECT * FROM vehicle WHERE vtouchcustomerid=" + customerid + " and vehiclenumber='" + vehicleNumber
					+ "'";
			rs = stmt.executeQuery(sql);
			//Logger.write("communication_bot", "Check if the vehicle exists");
			bExist = rs.next();
			if (bExist) {
				sql = "UPDATE vehicle set lastpingtime=NOW(),lat='" + lat + "',lng='" + lng + "',line='" + line
							+ "',destination='" + destination + "',PTAjourneyId='" + patjourneyid + "',journeytype='" + journeytype + "' where vtouchcustomerid=" + customerid + " and vehiclenumber='" + vehicleNumber + "'";

				String dbLat = rs.getString("lat");
				String dbLong = rs.getString("lng");
				if (dbLat!=null && dbLat.equalsIgnoreCase(lat) && dbLong != null && dbLong.equalsIgnoreCase(lng))
					gpsUpdated = false;
				stmt.executeUpdate(sql);
				
				
			} else {
				Logger.write("communication_bot_DatabaseManage", "This is new vehicle" + customerid + ", " + vehicleNumber);
				//if (!lat.isEmpty() && !lng.isEmpty()) {
					//Logger.write("communication_bot", "Adding vehicle info with no lat long");
					sql = "INSERT into vehicle (vtouchcustomerid, vehiclenumber, lastpingtime,lat,lng,line,destination,PTAjourneyId,journeytype) VALUES ("
							+ customerid + ", '" + vehicleNumber + "',NOW(),'" + lat + "','" + lng + "','" + line + "','"+destination+"','"+patjourneyid+"','"+journeytype+"')";
				stmt.executeUpdate(sql);
				
				sql = "INSERT into pinghistory (customerid, vehiclenumber, lastpingtime,lat,lng,line) VALUES ("
						+ customerid + ", '" + vehicleNumber + "',NOW(),'" + lat + "','" + lng + "','" + line+ "')";
				stmt.executeUpdate(sql);
				Logger.write("communication_bot_DatabaseManage", "Ping updated");
			}
		} catch (Exception ex) {
			Logger.write("communication_bot_DatabaseManage", "Exception in Ping " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		} finally {
			// finally block used to close resources
			cleanup(stmt,rs);
		}
		return gpsUpdated;
	}

	public static List<Vehicle> getVehicleList() {
		return null;
	}
	
	/*
	public static void addCallData(String type, String customerId, String from, String to, String channel,
			String status) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();
			stmt = conn.createStatement();

			String sql = "INSERT into calls (type, customerid, src, dest,channel, status) VALUES ('" + type + "', '"
					+ customerId + "','" + from + "','" + to + "','" + channel + "','" + status + "')";
			Logger.write("communication_bot_DatabaseManage", sql);
			stmt.executeUpdate(sql);
		} catch (Exception ex) {
			Logger.write("communication_bot_DatabaseManage", "addCallData " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		} finally {
			// finally block used to close resources
			cleanup(stmt,rs);
		}

	}
	*/
	
	public static void updateCallHistory(String tracknumber,
										 String ctype,
										 String csource,
										 String cevnt,
										 String fendpoint,
										 String tendpoint) 
	{
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		 try
		 {
			  conn = getDBConnection();
			  //STEP 3: Execute a query
			  //call communicationdev.savecallhistory_v1('call', 'bus', 'end', 	  '2254_9911','[{"customer":"2254","vehiclenumber":"1600"}]', 'sha');
			  stmt = conn.prepareCall("{call savecallhistory_v2(?,?,?,?,?,?)}");
			  stmt.setString(1, ctype);
			  stmt.setString(2, csource);
			  stmt.setString(3, cevnt);
			  stmt.setString(4, tracknumber);
			  stmt.setString(5, fendpoint);
			  stmt.setString(6, tendpoint.replace("vehicleno", "vehiclenumber"));
			  stmt.executeQuery();
			  Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call savecallhistory_v2(%s,%s,%s,%s,%s,%s)} executed",
					  ctype,
					  csource,
					  cevnt,
					  tracknumber,
					  fendpoint,
					  tendpoint));
			  //STEP 6: Clean-up environment
			  try
			  {
				  if (cevnt.equalsIgnoreCase(END)|| cevnt.equalsIgnoreCase(REJECT) || cevnt.equalsIgnoreCase(DECLINE))
				  {
					  Logger.write("communication_bot_DatabaseManage", String.format("Calling get callHistory for new event %s",cevnt));
					  sendCallHistory();
				  }
			  }
			  catch(Exception ex)
			  {
				  Logger.writeError("botservice", ex);
			  }
		 } 
		 catch (Exception ex) 
		 {
			Logger.write("communication_bot_DatabaseManage", "updateCallHistory " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		 } 
		 finally 
		 {
			// finally block used to close resources
			cleanup(stmt,rs);
		 }
	}
	
	/*
	public static String getCallHistory(String customerid) 
	{
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		 try
		 {
			  conn = getDBConnection();
			  //STEP 3: Execute a query
			  //call communicationdev.savecallhistory_v1('call', 'bus', 'end', 	  '2254_9911','[{"customer":"2254","vehiclenumber":"1600"}]', 'sha');
			  int icustomerid = Integer.parseInt(customerid);
			  int callId = (callStatuses.containsKey(icustomerid)? callStatuses.get(icustomerid) + 1: 1);
			  stmt = conn.prepareCall("{call getcallshistory_v4_bot(?,?)}");
			  stmt.setInt(1, icustomerid);
			  stmt.setInt(2, callId);
			  
			  rs = stmt.executeQuery();
			  Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call getcallshistory_v4_bot(%s,%s)} executed",
					  icustomerid,
					  callId));
		      //STEP 4: Extract data from result set
			  String jsonString = "";
		      while(rs.next())
		      {
		    	jsonString += rs.getString("jsonstring");
		    	if (rs.isFirst())	callStatuses.put(icustomerid, Integer.parseInt(rs.getString("id")));
		    	if (!rs.isLast())	jsonString += ",";
		      }
			  jsonString = ApplicationUtility.isNullOrEmpty(jsonString)? "" : "{\"command\":\"callhistory\", \"data\" :[" + jsonString + "]}";
			  Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call getCallHistory(%s,%s)} executed",
					  icustomerid,
					  callId));
			  //STEP 6: Clean-up environment
			  return jsonString;
		 } 
		 catch (Exception ex) 
		 {
			Logger.write("communication_bot_DatabaseManage", "getCallHistory " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		 } 
		 finally 
		 {
			// finally block used to close resources
			cleanup(stmt,rs);
		 }
		 return "[]";
	}
	*/
	
	
	private static void sendCallHistory() 
    {
    	class ThreadTask implements Runnable 
		{
			public ThreadTask()
			{}
			public void run()
	 	   	{
				//Map<Integer, Integer> tempbusStatuses = new HashMap<Integer, Integer>();
				try
				{
					//tempbusStatuses = callStatuses;
					for (Map.Entry<Integer, Integer> entry : callStatuses.entrySet())
					{
						Connection conn = null;
						CallableStatement stmt = null;
						ResultSet rs = null;
						try
						{
							  conn = getDBConnection();
							  //STEP 3: Execute a query
							  int icustomerid = entry.getKey();
							  int callId = entry.getValue() + 1;
							  stmt = conn.prepareCall("{call getcallshistory_v4_bot(?,?)}");
							  stmt.setInt(1, icustomerid);
							  stmt.setInt(2, callId);
							  
							  rs = stmt.executeQuery();
							  Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call getcallshistory_v4_bot(%s,%s)} executed",
									  icustomerid,
									  callId));
						      //STEP 4: Extract data from result set
							  String jsonString = "";
						      while(rs.next())
						      {
						    	jsonString += rs.getString("jsonstring");
						    	
						    	if (rs.isFirst())	
						    	{
						    		Logger.write("communication_bot_DatabaseManage", String.format("icustomerid - %s - callid - %s",icustomerid,rs.getString("id")));
						    		callStatuses.put(icustomerid, Integer.parseInt(rs.getString("id")));
						    		Logger.write("communication_bot_DatabaseManage", String.format("icustomerid - %s - callid - %s",icustomerid,callStatuses.get(icustomerid)));
						    	}
						    	if (!rs.isLast())	jsonString += ",";
						      }
							  jsonString = ApplicationUtility.isNullOrEmpty(jsonString)? "" : "{\"command\":\"callhistory\", \"data\" :[" + jsonString + "]}";
							  Logger.write("communication_bot_DatabaseManage", String.format("%s",jsonString));
							  //STEP 6: Clean-up environment
							  WebSocketHandler.broadcastToAllBrowsers(Integer.toString(icustomerid), "", jsonString);
						 } 
						 catch (Exception ex) 
						 {
							Logger.write("communication_bot_DatabaseManage", "getCallHistory " + ex.getMessage());
							Logger.writeError("communication_bot_DatabaseManage", ex);
						 } 
						 finally 
						 {
							// finally block used to close resources
							cleanup(stmt,rs);
						 }
					}
				}
				catch(Exception ex)
				{
					Logger.write("communication_bot_DatabaseManage", "getCallHistory " + ex.getMessage());
					Logger.writeError("communication_bot_DatabaseManage", ex);
				}
				finally
				{
					//tempbusStatuses.clear();
					//tempbusStatuses = null;
				}
	 	   }
		}
		Thread t = new Thread(new ThreadTask());
		t.start();
	}
	
	public static void getCallIdHistory(String customerid) 
	{
		
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		 try
		 {
			  if (callStatuses.containsKey(Integer.parseInt(customerid))) return;
			  conn = getDBConnection();
			  //STEP 3: Execute a query
			  //call communicationdev.savecallhistory_v1('call', 'bus', 'end', 	  '2254_9911','[{"customer":"2254","vehiclenumber":"1600"}]', 'sha');
			  int icustomerid = Integer.parseInt(customerid);
			  stmt = conn.prepareCall("{call getcallsId_history_v4_bot(?)}");
			  stmt.setInt(1, icustomerid);

			  rs = stmt.executeQuery();
			  Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call getcallsId_history_v4_bot(%s)} executed",
					  icustomerid));
		      //STEP 4: Extract data from result set
		      while(rs.next())
		      {
		    	  callStatuses.put(icustomerid, Integer.parseInt(rs.getString("id")));
		      }
		      if (!callStatuses.containsKey(Integer.parseInt(customerid)))
		    	  callStatuses.put(Integer.parseInt(customerid), 1);
		 } 
		 catch (Exception ex) 
		 {
			Logger.write("communication_bot_DatabaseManage", "getCallIdHistory " + ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		 } 
		 finally 
		 {
			// finally block used to close resources
			cleanup(stmt,rs);
		 }
	}
	

	public static boolean isPanicEnabled(String customerId, String busNumber)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
			String key = String.format("%s_%s", customerId,busNumber);
			//if (panicAllowedVehicles.containsKey(key))
			//	return Boolean.parseBoolean(panicAllowedVehicles.get(key));
			
			conn = getDBConnection();
			stmt = conn.createStatement();
			String qry = "SELECT panicallowed FROM vehicle where vtouchcustomerid='" + customerId + "' and vehiclenumber='" + busNumber + "'";
			Logger.write("communication_bot_DatabaseManage", qry);
			rs = stmt.executeQuery(qry);
			if (rs.next()) {
				int panicallowed = rs.getInt("panicallowed");
				if (panicallowed == 0){
					//panicAllowedVehicles.put(key,"false");
					return false;
				}
				//panicAllowedVehicles.put(key,"true");
			}
		}catch(Exception ex){
			Logger.write("communication_bot_DatabaseManage", ex.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", ex);
		}
		finally 
		{
			cleanup(stmt,rs);
		}
		return true;
	}
	public static Boolean IsOperatorCanReceiveCall(String userName)
    {
	   Boolean ret = false;
	   
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
	   
	   try
	   {
		   try
		   {
				//if (isOperatorsReceivedCall.containsKey(userName))
				//	return Boolean.parseBoolean(isOperatorsReceivedCall.get(userName));
			   
		      conn = getDBConnection();
		      //Logger.write("communication_bot_DatabaseManage", "Connection open");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call isoperatorcanreceivecall(?)}");
		      //Logger.write("communication_bot_DatabaseManage", String.format("{call isoperatorcanreceivecall(%s)}", userName));
		      stmt.setString(1, userName);
		      rs = stmt.executeQuery();
		      //Logger.write("communication_bot_DatabaseManage", "Qeury executed");
		      //STEP 4: Extract data from result set
		      while(rs.next())
		      {
		    	String auth = rs.getString("canrecieve");
		    	//Logger.write("communication_bot_DatabaseManage", String.format("rs.getString(canrecieve) %s ", auth));
		    	if (auth.equalsIgnoreCase("1"))
		    	{
		    		//isOperatorsReceivedCall.put(userName,"true");
		    		ret = true;
		    		
		    	}
		    	//isOperatorsReceivedCall.put(userName,"false");
		    		//return ApplicationUtility.issueToken();
		      }
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_bot_DatabaseManage", "Exception IsOperatorCanReceiveCall - 1 : " + se.getMessage());
			   Logger.writeError("communication_bot_DatabaseManage",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_bot_DatabaseManage", "Exception IsOperatorCanReceiveCall - 2 : " + e.getMessage());
			   Logger.writeError("communication_bot_DatabaseManage",  e);
		   }
		   finally
		   {
			   cleanup(stmt,rs);
		   }//end try
	   }
	   catch(Exception se)
	   {
    	  Logger.writeError("communication_bot_DatabaseManage",  se);
	   }//end finally try
	   //Logger.write("communication_bot_DatabaseManage", "return " + ret);
	   return ret;
   }
	
	public static String getIBICustomer(String vTouchCustomer){
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try{
			
			if (ibicustomerAgainstvTouchCustomer.containsKey(vTouchCustomer))
				return (String)ibicustomerAgainstvTouchCustomer.get(vTouchCustomer);
			
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getIBICustomer(?)}");
			stmt.setString(1, vTouchCustomer);
			rs = stmt.executeQuery();
			if (rs.next()){
				ibicustomerAgainstvTouchCustomer.put(vTouchCustomer,rs.getString("customerid"));
				return rs.getString("customerid");
			}
		}catch(Exception ex){
			Logger.writeError("communication_bot_DatabaseManage",  ex);
		}
		finally 
		{
			cleanup(stmt,rs);
		} 
		return "";
	}
	
	public static String getVehicleMapData(int customerId,String vehicleNumber)
	{
	   Connection conn = null;
	   ResultSet rs = null;
	   CallableStatement stmt = null;

	   try
	   {
	      conn = getDBConnection();
	      //STEP 3: Execute a query
	      
	      stmt = conn.prepareCall("{call getvehiclemapdata(?,?)}");
	      stmt.setInt(1, customerId);
	      stmt.setString(2, vehicleNumber);
	      
	      rs = stmt.executeQuery();
	      Logger.write("communication_bot_DatabaseManage", String.format("Qeury {call getvehiclemapdata(%s,%s)} executed",customerId,vehicleNumber));
	      //STEP 4: Extract data from result set
	      String buses = "";
	      while(rs.next())
	      {
	    	  buses =  rs.getString("jsonstring");
	      }
	      Logger.write("communication_bot_DatabaseManage", String.format("Return %s",buses));
	      return buses;
	      //STEP 6: Clean-up environment
	   }
	   catch(SQLException se)
	   {
		   Logger.write("communication_bot_DatabaseManage", "getvehiclemapdata >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
		   Logger.writeError("communication_bot_DatabaseManage",  se);
	   }
	   catch(Exception e)
	   {
		   //return "NOT AUTH";
		   Logger.write("communication_bot_DatabaseManage", "getvehiclemapdata >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
		   Logger.writeError("communication_bot_DatabaseManage",  e);
	   }
	   finally
	   {
		   cleanup(stmt,rs);
	   }//end try
	   return "";
	}
	
	public static String vehicleListWithStatus(String customerId, int groupId) {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getUpdatedVehicleStatus(?)}");
			stmt.setInt(1, 60);
			rs = stmt.executeQuery();
			//Logger.write("communication_bot_DatabaseManage", "Qeury executed");
			// STEP 4: Extract data from result set
			String finalJson = "";

			while (rs.next()) {
				if (!customerId.equalsIgnoreCase(rs.getString("ibicustomerid"))){
					continue;
				}
				String vehicleNumber = String.format("%s_%s", rs.getString("customerid"),
						rs.getString("vehiclenumber"));
				String jsonString = "{" + "\"vehiclenumber\":\"" + rs.getString("vehiclenumber") + "\","
						+ "\"customer\":\"" + rs.getString("customerid") + "\"," 
						+ "\"currentstatus\":\"" + rs.getString("status") + "\"," + "\"line\":\"" + rs.getString("line")+ 
						"\"," + "\"destination\":\"" + rs.getString("destination")+ 
						"\"," + "\"ptajourneyid\":\"" + rs.getString("ptajourneyid")+ "\"," + "\"journeytype\":\"" + rs.getString("journeytype")+ 
						"\"" + "," + "\"groups\":\"" + rs.getString("groups")+ 
						"\"" + "}";
				if (busStatuses.containsKey(vehicleNumber)) {
					String oldData = (String) busStatuses.get(vehicleNumber);
					if (oldData != null && oldData.equalsIgnoreCase(jsonString)) {
						continue;
					}
				}
				busStatuses.put(vehicleNumber, jsonString);
				if (!finalJson.isEmpty())
					finalJson += ",";
				finalJson += jsonString;
			}
			if (!ApplicationUtility.isNullOrEmpty(finalJson))
				finalJson = "{\"command\":\"vehiclelist\", \"vehicles\":" + "[" + finalJson + "]" + "}";
			//Logger.write("communication_bot_DatabaseManage", "Returned " + finalJson);
			return finalJson;
			// STEP 6: Clean-up environment
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception vehicleListWithStatus - 1: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			// return "NOT AUTH";
			Logger.write("communication_bot_DatabaseManage", "Exception vehicleListWithStatus - 2: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		} finally {
			cleanup(stmt,rs);
		} // end try
		return "{}";
	}
	
	public static List<String> getUniqueCustomers() {
		List<String> ibiCustomers = new ArrayList<String>();
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getUniqueCustomers()}");
			rs = stmt.executeQuery();
			//Logger.write("communication_mumble", "Qeury executed");
			while (rs.next()) {
				//Logger.write("communication_mumble", String.format("%s;%s", rs.getString("mumblehost"), rs.getString("mumbleport")));
				ibiCustomers.add(String.format("%s", rs.getString("ibicustomerid")));
			}
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception getUniqueCustomers - 1: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			Logger.write("communication_bot_DatabaseManage", "Exception getUniqueCustomers - 2: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		} finally {
			cleanup(stmt,rs);
		} // end try
		return ibiCustomers;
	}
	
	public static List<String> getUniqueMumbleServers() 
	{
		List<String> mumbleHosts = new ArrayList<String>();
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try 
		{
			conn = getDBConnection();

			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getDistinctMumbleServers()}");
			rs = stmt.executeQuery();
			//Logger.write("communication_mumble", "Qeury executed");
			while (rs.next()) {
				//Logger.write("communication_mumble", String.format("%s;%s", rs.getString("mumblehost"), rs.getString("mumbleport")));
				mumbleHosts.add(String.format("%s;%s", rs.getString("mumblehost"), rs.getString("mumbleport")));
			}
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception getUniqueMumbleServers - 1: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			Logger.write("communication_bot_DatabaseManage", "Exception getUniqueMumbleServers - 2: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		} finally {
			cleanup(stmt,rs);
		} // end try
		return mumbleHosts;
	}
	
	public static String getMumbleHost(int customerId) 
	{
		String customer = Integer.toString(customerId);
		if (customerMumbleHost.containsKey(customer)) 
		{
			return (String)customerMumbleHost.get(customer);
		}
		return "";
		//customerMumbleHost.put(customerId, value);
	}
	
	public static Map<String,String> getMumbleHostFromDB() {
		String customer = "";
		String mumbleHost = "";
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();
			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getHostAndPort()}");
			rs = stmt.executeQuery();
			Logger.write("communication_bot_DatabaseManage", "stored procedure getHostAndPort() called");
			while (rs.next()) {
				customer = rs.getString("customer");
				mumbleHost = rs.getString("mumblehost"); 
				customerMumbleHost.put(customer, mumbleHost);
			}
			return customerMumbleHost;
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception getMumbleHostFromDB: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			Logger.write("communication_bot_DatabaseManage", "Exception getMumbleHostFromDB: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		} finally {
			cleanup(stmt,rs);
		} // end try
		return null;
		//return "127.0.0.1";
	}
	
	
	public static Map<String,String> getMumblePortFromDB() {
		String customer = "";
		String mumbleport = "";
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = getDBConnection();
			// STEP 3: Execute a query
			stmt = conn.prepareCall("{call getHostAndPort()}");
			rs = stmt.executeQuery();
			Logger.write("communication_bot_DatabaseManage", "stored procedure getHostAndPort() called");
			while (rs.next()) {
				customer = rs.getString("customer");
				mumbleport = rs.getString("mumbleport"); 
				customerMumblePort.put(customer, mumbleport);
			}
			return customerMumblePort;
		} catch (SQLException se) {
			Logger.write("communication_bot_DatabaseManage", "Exception getMumblePortFromDB: " + se.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", se);
		} catch (Exception e) {
			Logger.write("communication_bot_DatabaseManage", "Exception getMumblePortFromDB: " + e.getMessage());
			Logger.writeError("communication_bot_DatabaseManage", e);
		} finally {
			cleanup(stmt,rs);
		} // end try
		return null;
		//return "127.0.0.1";
	}
	
	public static String getMumblePort(int customerId) {
		
		String customer = Integer.toString(customerId);
		if (customerMumblePort.containsKey(customer)) 
		{
			return (String)customerMumblePort.get(customer);
		}
		return "";
	}
	
	private static void cleanup(CallableStatement stmt,ResultSet rs)
	{
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	     
	      /*try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
	      }//end finally try
	      */
	}
	private static void cleanup(Statement stmt,ResultSet rs)
	{
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >>MYSQL Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_Server %s , USER %s, PASS %s",DB_URL,USER,USER,PASS));
	      }// nothing we can do
	     
	      /*try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
			   Logger.write("communication_PTAJourneyImporterReport", "getGroupData >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_Server %s , DB_Name %s ,USER %s, PASS %s",DB_Server,DB_Name,USER,PASS));
	      }//end finally try
	      */
	}
	/*
	private static void cleanup() {
		// finally block used to close resources
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException se2) {
			Logger.writeError("communication_bot_DatabaseManage", se2);
		} // nothing we can do
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException se2) {
			Logger.writeError("communication_bot_DatabaseManage", se2);
		} // nothing we can do
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException se) {
			Logger.writeError("communication_bot_DatabaseManage", se);
		} // end finally try
	}
	*/
}
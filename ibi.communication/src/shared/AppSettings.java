package shared;

import java.io.File;

public class AppSettings 
{
	public static boolean staging = false;
	public static String resolveApplicationDirectory()
	{
		String applicationPath = System.getProperty("user.dir");
		String osName = System.getProperty("os.name");
		File applicationDirectory = new File(applicationPath);
		if(!applicationPath.endsWith("bin") && osName.toLowerCase().indexOf("windows") != -1 && applicationDirectory.exists())
			applicationPath = applicationPath + "/bin";

		return applicationPath + "/";
	}
	public static String getCommWebsocketFilePath()
	{
		return "/storage/system/cache" + "/comm_websocket.txt";
	}
	
	public static boolean isStaging(){
		//if (true)
		//	return true;
		return staging;
	}
}

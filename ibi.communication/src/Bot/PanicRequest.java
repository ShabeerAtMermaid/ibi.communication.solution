package Bot;

public class PanicRequest {
	private String customerId;
	private String vehicleNumber;
	private String timestamp;
	private String lat;
	private String lng;
	
	public PanicRequest(){
		
	}
	
	public PanicRequest(String customerid, String vehiclenumber, String timestamp, String lat, String lng){
		this.setCustomerId(customerid);
		this.setVehicleNumber(vehiclenumber);
		this.setTimestamp(timestamp);
		this.setLat(lat);
		this.setLng(lng);
				
	}

	public String getMumbleUser(){
		return String.format("%s_%s", this.customerId, this.vehicleNumber);
	}
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
		if (this.lat == null || this.lat.isEmpty()){
			this.lat = "";
		}
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
		if (this.lng == null || this.lng.isEmpty()){
			this.lng = "";
		}
	}
}

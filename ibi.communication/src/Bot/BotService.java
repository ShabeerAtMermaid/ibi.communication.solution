package Bot;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.Session;

import org.glassfish.tyrus.server.Server;
import Murmur.Channel;
import Murmur.MetaPrx;
import Murmur.MetaPrxHelper;
import Murmur.ServerPrx;
import Murmur.User;
import ibi.communicator.websocket.MapsModuleHandler;
import ibi.communicator.websocket.WebSocketHandler;
import ibi.communicator.websocket.WebSocketServerEndpoint;
import shared.AppSettings;
import shared.ApplicationUtility;
import shared.DatabaseManager;
import shared.Logger;
import shared.Vehicle;

import java.nio.charset.Charset;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;


public class BotService {
	private MetaPrx meta = null;
	//private ServerPrx serverProxy = null; 
	private SecureRandom random = new SecureRandom();
	//private static MultiThreadedServer service;
	private static final int PORT = AppSettings.isStaging()?8024:8026;
	private static IoAcceptor acceptor = null;

	Map<String, ServerPrx> serverProxies = new HashMap<String, ServerPrx>();
	
	List<String> uniqueMumbleServers;
	
	public static boolean testmode = false;
	
	public BotService()
	{
		DatabaseManager.getMumbleHostFromDB();
		DatabaseManager.getMumblePortFromDB();
		uniqueMumbleServers = DatabaseManager.getUniqueMumbleServers();
	}
	private static int readPort(String[] args){
		int defaultPort = 8026;
		try{
			if (args.length > 0){
				defaultPort = Integer.parseInt(args[0]);
			}
		}catch(Exception e){
			
		}
		return defaultPort;
	}
	
	public static String processBrowserMessage(String message)
	{
		ClientRequest request = new ClientRequest(message);
		if (request.isParsed()){
			
		}
		return "";
	}
	
	private static void sendPingStatus() 
    {
    	class ThreadTask implements Runnable 
		{
    		List<Vehicle> oldList = DatabaseManager.getVehicleList();
			public ThreadTask()
			{

			}
			public void run()
	 	   	{
				
	    	  Logger.write("communication_bot", "Thread started to sync Ping");
	    	  List<String> customers = DatabaseManager.getUniqueCustomers();
 	   		  while(true)
 	   		  {
				try 
				{  
					//TODO: Fix customer wise
					if (customers != null && customers.size() > 0)
					{
						for(String customer: customers)
						{
							try
							{
								String vehicleList = DatabaseManager.vehicleListWithStatus(customer,0);//
								WebSocketHandler.broadcastToAllBrowsers(customer, "", vehicleList);	
							}
							catch(Exception ex)
							{
								Logger.writeError("botservice", ex);
							}
							try
							{
								DatabaseManager.getCallIdHistory(customer);
							}
							catch(Exception ex)
							{
								Logger.writeError("botservice", ex);
							}
						}
					}
				}
				catch (Exception e) 
				{
					Logger.write("communication_bot", "BOT to WebSocket - Ping Sync >>>> Exception >> " + e.getMessage());
					Logger.writeError("communication_bot", e);
				}
				   
				try {
					Thread.sleep(60 * 1000);
				}catch (InterruptedException e) {}
				}
	 	   }
		}
		Thread t = new Thread(new ThreadTask());
		t.start();
		Logger.write("communication_bot", "Ping Status Time started");
	}
	
	private static void runBotService(int port)
	{
		try{
			acceptor = new NioSocketAcceptor();
			Logger.write("communication_bot", "The server has initialised");
	        //acceptor.getFilterChain().addLast( "logger", new LoggingFilter() );
			TextLineCodecFactory codec = new TextLineCodecFactory( Charset.forName( "utf-8" ));
			codec.setDecoderMaxLineLength(1000*1024);
			codec.setEncoderMaxLineLength(1000*1024);
	        acceptor.getFilterChain().addLast( "codec", new ProtocolCodecFilter(codec));

	        acceptor.setHandler( new BotServiceHandler(acceptor) );
	        acceptor.getSessionConfig().setReadBufferSize( 100*1024 );
	        acceptor.getSessionConfig().setIdleTime( IdleStatus.BOTH_IDLE, 10 );
	        acceptor.bind( new InetSocketAddress(port) );
	        Logger.write("communication_bot", "BOT is listening at " + port);
	        
	        sendPingStatus();
	        
	        MapsModuleHandler.sendMapDataThread();
	        
		}catch(Exception ex)
		{
			Logger.writeError("botservice", ex);
		}

	}
	private static void runWebSocketServer(int port) 
    {
		Logger.write("communication_websocketserver", "Web Socket >> websockets server creating !!!");
		Server server = new Server("localhost", port, "/websockets", WebSocketServerEndpoint.class);
		Logger.write("communication_websocketserver", "Web Socket >> websockets server created !!!");
        try 
        {
        	Logger.write("communication_websocketserver", "Web Socket >> websockets server starting !!!");
            server.start();
            Logger.write("communication_websocketserver", "Web Socket >> started !!!");
            Thread.currentThread().join();
        } 
        catch (Exception e) 
        {
            Logger.writeError("communication_websocketserver", e);
        } 
        finally 
        {
            server.stop();
        }
    }
	
	public static void main(String [] args)
	{
		try
		{
			/*
			service = new MultiThreadedServer(readPort(args));
			new Thread(service).start();
			System.out.println("The BOT server has started");
			
			*/
			AppSettings.staging = false;
			int tcpPort = 0;
			int wsPort = 0;
			int staging = 0;
			if (args.length == 3){
				try{
					tcpPort = Integer.parseInt(args[0]);
					wsPort = Integer.parseInt(args[1]);
					staging = Integer.parseInt(args[2]);
					AppSettings.staging = staging == 1;
				}catch(Exception ex){
					
				}
			}
			if (tcpPort <= 0){
				tcpPort = AppSettings.isStaging()?8024:8026;
			}
			if (wsPort <=0){
				wsPort = AppSettings.isStaging()?8023:8025;
			}
	        System.out.println("The Server has started");
	        runBotService(tcpPort);
			runWebSocketServer(wsPort);
			
			
			System.out.println("The WebSocket server has started");
			
	        
	        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.println("The WebSocket server has started");
			System.out.println("Press enter to exit");
			br.readLine();
			
			
		}catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
		}

	}
	/*
	public boolean isServerRunning()
	{
		try
		{
			if (serverProxy != null)
				return serverProxy.isRunning();
			Ice.Communicator ic = null;

			// lets connect to the murmurs ice
			ic = Ice.Util.initialize();
			Ice.ObjectPrx base = ic.stringToProxy("Meta:tcp -h 127.0.0.1 -p 6502");
			meta = MetaPrxHelper.checkedCast(base);
			ServerPrx[] allServers = meta.getAllServers();
			System.out.println("Servers " + allServers.length);
			serverProxy = meta.getServer(1);
			return serverProxy.isRunning();

		}catch(Exception ex)
		{
			System.out.println("Error " + ex.getMessage());
		}
		return false;
	}
	*/
	public ServerPrx getProxy(String host, String port)
	{
		try
		{
			if(testmode)
				return null;
			//Logger.write("communication_mumble", String.format("Proxy requested for %s_%s", host, port) );
			String serverKey = String.format("%s_%s", host, port);
			if (serverProxies.get(serverKey) != null && serverProxies.get(serverKey).isRunning())
			{
				//Logger.write("communication_mumble", String.format("Already started and running %s_%s", host, port) );
				return serverProxies.get(serverKey);
			}
				

			Ice.Communicator ic = null;

			// lets connect to the murmurs ice
			ic = Ice.Util.initialize();
			Ice.ObjectPrx base = ic.stringToProxy(String.format("Meta:tcp -h %s -p %s", host, port));
			meta = MetaPrxHelper.checkedCast(base);
			serverProxies.put(serverKey, meta.getServer(1));
			//Logger.write("communication_mumble", String.format("Proxy created and saved in map %s_%s", host, port) );
			return serverProxies.get(serverKey);

		}catch(Exception ex)
		{
			Logger.write("communication_mumble", String.format("Error getting proxy %s_%s, %s", host, port, ex.getMessage()) );
			System.out.println("Error " + ex.getMessage());
		}
		return null;
	}
	
	public ServerPrx getProxy(String customerId)
	{
		try
		{
			if(testmode)
				return null;
			//Logger.write("communication_mumble", String.format("Proxy(customerId) requested for %s", customerId) );
			String host = DatabaseManager.getMumbleHost(Integer.parseInt(customerId));
			String port = DatabaseManager.getMumblePort(Integer.parseInt(customerId));
			String serverKey = String.format("%s_%s", host, port);
			Logger.write("communication_mumble", String.format("Proxy requested for %s_%s", host, port) );
			if (serverProxies.get(serverKey) != null && serverProxies.get(serverKey).isRunning()){
				//Logger.write("communication_mumble", String.format("(customerId) Already started and running %s_%s", host, port) );
				return serverProxies.get(serverKey);
			}

			Ice.Communicator ic = null;
			Logger.write("communication_mumble", String.format("Start step 1 %s_%s", host, port) );
			// lets connect to the murmurs ice
			ic = Ice.Util.initialize();
			Ice.ObjectPrx base = ic.stringToProxy(String.format("Meta:tcp -h %s -p %s", host, port));
			Logger.write("communication_mumble", String.format("Start step 2 %s_%s", host, port) );
			meta = MetaPrxHelper.checkedCast(base);
			serverProxies.put(serverKey, meta.getServer(1));
			Logger.write("communication_mumble", String.format("(customerId) Proxy created and saved in map %s_%s", host, port) );
			return serverProxies.get(serverKey);

		}catch(Exception ex)
		{
			Logger.write("communication_mumble", String.format("(customerId) Error getting proxy %s, %s", customerId, ex.getMessage()) );
			System.out.println("Error " + ex.getMessage());
		}
		return null;
	}
	
	public boolean isServerRunning(String customerId)
	{
		try
		{
			if(testmode)
				return true;
			//Logger.write("communication_mumble", String.format("isServerRunning: for %s", customerId) );
			String host = DatabaseManager.getMumbleHost(Integer.parseInt(customerId));
			String port = DatabaseManager.getMumblePort(Integer.parseInt(customerId));
			//TODO: Change it back
			//String host = "127.0.0.1";
			//String port = "6502";
			String serverKey = String.format("%s_%s", host, port);
			
			if (serverProxies.get(serverKey) != null){
				Logger.write("communication_mumble", String.format("isServerRunning: Already started and running %s_%s", host, port) );
				return serverProxies.get(serverKey).isRunning();
			}
			Ice.Communicator ic = null;

			// lets connect to the murmurs ice
			ic = Ice.Util.initialize();
			Ice.ObjectPrx base = ic.stringToProxy(String.format("Meta:tcp -h %s -p %s", host, port));
			meta = MetaPrxHelper.checkedCast(base);
			serverProxies.put(serverKey, meta.getServer(1));
			Logger.write("communication_mumble", String.format("isServerRunning: Proxy created and saved in map %s_%s", host, port) );
			//serverProxy = meta.getServer(1);
			return serverProxies.get(serverKey).isRunning();

		}catch(Exception ex)
		{
			Logger.write("communication_mumble", String.format("isServerRunning: Error Proxy created and saved in map %s_%s", customerId) );
			Logger.writeError("communication_mumble", ex);
		}
		return false;
	}
	
	public String nextChannelName() {
	    return new BigInteger(130, random).toString(32);
	  }
	
	public String startSingleCall(String user1, String user2, String customerId)
	{
		try
		{
			if (testmode)
				return "start";
			String channame = "";
			//if (isServerRunning(customerId)){
				channame = "ROOM_" + (System.currentTimeMillis() % 1000);
	            int channelId = getProxy(customerId).addChannel(channame, 0);

            	moveUser(user1, channelId,customerId);
            	moveUser(user2, channelId,customerId);
            	muteUser(user1, false,customerId);
            	muteUser(user2, false,customerId);
            	
            	return channame;
			//}
		}catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
		}
		return "";
	}
	
	public void sendVoiceMessage(String mumbleUser, String message, String customerId)
	{
		try
		{
			//if (isServerRunning(customerId)){
				int nUID = usernameToId(mumbleUser,customerId);
				if (nUID != -1){
					User userState1 = getProxy(customerId).getState(nUID);
					if (userState1 != null && userState1.channel == 0){
						getProxy(customerId).sendMessage(userState1.session, message);
					}
				}
				
			//}
		}catch(Exception ex)
		{
			Logger.write("communication_mumble", ex.getMessage());
		}
	}
	
	public boolean endSingleCall(String user1, String user2, String customerId)
	{
		try
		{
			if(testmode)
				return true;
			//if (isServerRunning(customerId)){
				User userState1 = getProxy(customerId).getState(usernameToId(user1,customerId));
				User userState2 = getProxy(customerId).getState(usernameToId(user2,customerId));
				try{
					if (userState1.channel != 0){
						removeChannel(userState1.channel,customerId);
					}
					if (userState2.channel != 0){
						removeChannel(userState2.channel,customerId);
					}
				}catch(Exception ex){
					
				}
				muteUser(userState1.name, true,customerId);
				moveUser(userState1.name, 0,customerId);
				muteUser(userState2.name, true,customerId);
				moveUser(userState2.name, 0,customerId);
			//}
		}catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
		}
		return false;
	}
	
	public boolean startCall(String [] users, String customerId)
	{
		try
		{
			if(testmode)
				return true;
			//if (isServerRunning(customerId)){
				String channame = "ROOM_" + (System.currentTimeMillis() % 1000);
	            int channelId = getProxy(customerId).addChannel(channame, 0);
	            for(String userName: users)
	            {
	            	moveUser(userName, channelId,customerId);
	            	muteUser(userName, false,customerId);
	            }
	            //DatabaseManager.addStartCallLog(from, to, customerId, channel);
			//}
			

		}catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
		}
		return false;
	}
	
	public boolean endCall(String [] users, String customerId)
	{
		try
		{
			if(testmode)
				return true;
			//if (isServerRunning(customerId)){
				for(String userName: users){
					User user = getProxy(customerId).getState(usernameToId(userName,customerId));
					try{
						if (user.channel != 0){
							removeChannel(user.channel,customerId);
						}
					}catch(Exception ex){
						
					}
					muteUser(user.name, true,customerId);
					moveUser(user.name, 0,customerId);
				}
			//}
			

		}catch(Exception ex)
		{
			System.out.println("Error " + ex.getMessage());
		}
		return false;
	}
	
	public void moveUser(String userName, int channeId, String customerId)
	{
		moveUser(userName, Integer.toString(channeId),customerId);
	}
	public void moveUser(String userName, String channelName, String customerId)
	{
			//if (isServerRunning(customerId)){
				try{
					int uid = usernameToId(userName,customerId);
					if (uid == -1){
						return;
					}
					int cid = channelNameToId(channelName,customerId);
					if (cid == -1){
						return;
					}
					
					User userState = getProxy(customerId).getState(uid);
					if (userState == null)
						return;
					
					userState.channel = cid;
					getProxy(customerId).setState(userState);
				}catch(Exception ex){
					Logger.write("communication_mumble", ex.getMessage());
				}
			//}
	}
	
	public String muteUser(String userName, boolean muted, String customerId)
	{
		if(testmode)
			return "start";
		String channelName = "";
			//if (isServerRunning(customerId)){
				int uid = usernameToId(userName,customerId);
				if (uid == -1){
					return channelName;
				}
				try{
					User userState = getProxy(customerId).getState(uid);
					if (userState != null){
						userState.suppress = muted;
						getProxy(customerId).setState(userState);
						channelName = getProxy(customerId).getChannelState(userState.channel).name;
						return channelName;
					}
				}catch(Exception ex){
					Logger.write("communication_mumble", ex.getMessage());
				}
				
			//}
			return channelName;
	}
	
	public String[] getRelevantUsers(String username, String customerId){
		System.out.println("getRelevantUsers called");
		List<String> listUsers = new ArrayList<String>();
		listUsers.add(username);
		try{
			//if (isServerRunning(customerId)){
				try {
					System.out.println("getting Users");
					Map<Integer, User> users = getProxy(customerId).getUsers();
					for(Map.Entry<Integer, User> entry : users.entrySet()) {
					    User value = entry.getValue();
						System.out.println("processing User " + value.name);
					    if (!username.equalsIgnoreCase(value.name)){
					    	if (value.channel == 0){
					    		listUsers.add(value.name);
					    	}
					    }
					}
				} catch (Exception e) {
					e.printStackTrace();
				} 
			//}
		}catch(Exception ex){
			Logger.write("communication_mumble", ex.getMessage());
		}
		return listUsers.toArray(new String[0]);
	}
	
	public int usernameInCall(String userName, String customerId)
	{
		if(testmode)
			return -1;
		int result = isInteger(userName);
		if (result != -1){
			return result;
		}
			//if (isServerRunning(customerId)){
				try {
					Map<Integer, User> users = getProxy(customerId).getUsers();
					for(Map.Entry<Integer, User> entry : users.entrySet()) {
					    User value = entry.getValue();
					    if (userName.equalsIgnoreCase(value.name)){
					    	return value.session;
					    }
					}
				} catch (Exception e) {
					Logger.write("communication_mumble", e.getMessage());
				} 
			//}
		return -1;
	}
	
	public int usernameToId(String userName, String customerId)
	{
		if (testmode)
			return 10;
		int result = isInteger(userName);
		if (result != -1){
			Logger.write("communication_mumble", "returning isInteger != -1");
			return result;
		}
		//Logger.write("communication_mumble", String.format("isServerRunning: Check user %s_%s", userName, customerId));
			//if (isServerRunning(customerId)){
				//Logger.write("communication_mumble", String.format("isServerRunning: Server running %s_%s", userName, customerId));
				try {
					Map<Integer, User> users = getProxy(customerId).getUsers();
					for(Map.Entry<Integer, User> entry : users.entrySet()) {
						//Logger.write("communication_mumble", String.format(" Each user %s for %s_%s", entry.getValue().name, userName, customerId));
						User value = entry.getValue();
						Logger.write("communication_mumble", String.format("userName %s , value.name %s", userName,value.name));
					    if (userName.equalsIgnoreCase(value.name)){
					    	Logger.write("communication_mumble", String.format("return value.session %s", value.session));
					    	return value.session;
					    }
					}
				} catch (Exception e) {
					e.printStackTrace();
					Logger.writeError("communication_mumble", e);
					Logger.write("communication_mumble", String.format("return value.session %s", e.getMessage()));
				} 
			//}
				Logger.write("communication_mumble", "usernameToId >> returning -1");
		return -1;
	}
	
	private int channelNameToId(String channelName, String customerId)
	{
		if(testmode)
			return -1;
		
		int result = isInteger(channelName);
		if (result != -1){
			return result;
		}
			//if (isServerRunning(customerId)){
				try {
					Map<Integer, Channel> users = getProxy(customerId).getChannels();
					for(Map.Entry<Integer, Channel> entry : users.entrySet()) {
						Channel value = entry.getValue();
					    if (channelName.equalsIgnoreCase(value.name)){
					    	return value.id;
					    }
					}
				} catch (Exception e) {
					Logger.write("communication_bot", e.getMessage());
				} 
			//}
		return -1;
	}
	
	public int addChannel(String channelName, int parent, String customerId){
		int channelId = -1;
		try{
				//if (isServerRunning(customerId)){
					channelId = getProxy(customerId).addChannel(channelName, parent);
				//}
		}catch(Exception ex){
			Logger.write("communication_bot", ex.getMessage());
		}
		return channelId;
	}
	
	public int addChannel(String channelName, String customerId){
		return addChannel(channelName, 0, customerId);
	}
	
	public void removeChannel(String channelName, String customerId){
		try{
				//if (isServerRunning(customerId)){
					getProxy(customerId).removeChannel(channelNameToId(channelName,customerId));
				//}
		}catch(Exception ex){
			Logger.write("communication_bot", ex.getMessage());
		}
	}
	
	public void removeChannel(int channelId, String customerId)
	{
		try
		{
				//if (isServerRunning(customerId)){
					getProxy(customerId).removeChannel(channelId);
				//}
		}
		catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
		}
	}
	
	private int isInteger(String str) {
		int result = -1;
		try {
			result = Integer.parseInt(str);

        } catch (NumberFormatException nfe) {}
		return result;
    }

	public String startConferenceCall(String mumbleUser, List<RequestRecipient> recipients, String customerId) {
		try
		{
			if(testmode)
				return "start";
			String channame = "";
			//if (isServerRunning(customerId)){
				channame = "ROOM_" + (System.currentTimeMillis() % 1000);
	            int channelId = getProxy(customerId).addChannel(channame, 0);
	            Logger.write("communication_bot", String.format("starting conference from %s", mumbleUser));
	            for(RequestRecipient user: recipients)
	            {
	            	Logger.write("communication_bot", String.format("checking recipient %s", user.getMumbleUser()));
	            	int uid = usernameToId(user.getMumbleUser(),customerId);
	            	if (uid != -1){
		            	User userState1 = getProxy(customerId).getState(uid);
			            if (userState1 != null && userState1.channel != 0){
			            	Logger.write("communication_bot", String.format("removing channel %d", userState1.channel));
			            	getProxy(customerId).removeChannel(userState1.channel);
			            } 
			            Logger.write("communication_bot", String.format("muting user %s", user.getMumbleUser()));
		            	moveUser(user.getMumbleUser(), channelId,customerId);
		            	muteUser(user.getMumbleUser(), true,customerId);
	            	}
	            }
	            User userState1 = getProxy(customerId).getState(usernameToId(mumbleUser,customerId));
	            if (userState1!=null && userState1.channel != 0){
	            	Logger.write("communication_bot", String.format("removing channel for operator %d", userState1.channel));
	            	getProxy(customerId).removeChannel(userState1.channel);
	            } 
	            Logger.write("communication_bot", String.format("unmuting operator %s", mumbleUser));
	            moveUser(mumbleUser, channelId,customerId);
	            muteUser(mumbleUser, false,customerId);
	            return channame;
	            //DatabaseManager.addStartCallLog(from, to, customerId, channel);
			//}
			

		}
		catch(Exception ex)
		{
			Logger.write("communication_bot", ex.getMessage());
			Logger.writeError("communication_bot", ex);
		}
		return "";
	}

	public void endConferenceCall(String mumbleUser, List<RequestRecipient> recipients, String customerId) {
		try
		{
			//if (isServerRunning(customerId)){
				User userState1 = getProxy(customerId).getState(usernameToId(mumbleUser,customerId));
				int channel = userState1.channel;
	            for(RequestRecipient user: recipients)
	            {
	            	moveUser(user.getMumbleUser(), 0,customerId);
	            	muteUser(user.getMumbleUser(), true,customerId);
	            }
	            moveUser(mumbleUser, 0,customerId);
	            muteUser(mumbleUser, true,customerId);
	            getProxy(customerId).removeChannel(channel);
			//}
			

		}
		catch(Exception ex)
		{
			Logger.write("communication_bot", "Exception >> endConferenceCall " + ex.getMessage());
		}
	}

	public String startPanicCall(String mumbleUser, String busUser, String customerId) {
		try
		{
			if(testmode)
				return "start";
			String channame = "";
			//if (isServerRunning(customerId)){
				channame = "ROOM_" + (System.currentTimeMillis() % 1000);
	            int channelId = getProxy(customerId).addChannel(channame, 0);

            	moveUser(mumbleUser, channelId,customerId);
            	moveUser(busUser, channelId,customerId);
            	muteUser(mumbleUser, true,customerId);
            	muteUser(busUser, false,customerId);
            	return channame;
			//}
		}
		catch(Exception ex)
		{
			Logger.write("communication_bot", "Exception >> startPanicCall " + ex.getMessage());
		}
		return "";
	}

	public void muteIfNotInCall(String mumbleUser, String customerId) 
	{
		String channelName = "";
		//if (isServerRunning(customerId)){
		int uid = usernameToId(mumbleUser,customerId);
		if (uid == -1)
		{
			return ;
		}
		else
		{
			try
			{
				User userState = getProxy(customerId).getState(uid);
				if (userState != null && userState.channel == 0){
					userState.suppress = true;
					getProxy(customerId).setState(userState);
				}
			}
			catch(Exception ex)
			{
				Logger.write("communication_bot", "Exception >> muteIfNotInCall " + ex.getMessage());
			}
		}
	}

	public void cleanupChannels(){
		try{
			List<String> servers = uniqueMumbleServers;
			for(String server: servers)
			{
				//Logger.write("communication_mumble", "server: " + server);
				String[] hostandport = server.split(";");
				ServerPrx tmpProxy = getProxy(hostandport[0], hostandport[1]);
				Map<Integer, Channel> channels = tmpProxy.getChannels();
				for(Map.Entry<Integer, Channel> entry : channels.entrySet()) {
				    if (entry.getKey() != 0){
				    	int count = 0;
				    	for(Map.Entry<Integer, User> u : tmpProxy.getUsers().entrySet()) {
				    		if (u.getValue().channel == entry.getKey()){
				    			count ++;
				    		}
				    	}
				    	if (count <=1){
				    		tmpProxy.removeChannel(entry.getKey());
				    	}
				    }
				}
			}
			
		}
		catch(Exception ex)
		{
			Logger.write("communication_bot", "Exception >> cleanupChannels " + ex.getMessage());
		}
	}
	
	public void muteAllNotInCall() {
		Thread muteAllNotInCallThread = new Thread() 
        {
            public void run() 
            {
    			List<String> servers = uniqueMumbleServers;
            	while (true){
            		try{
            			for(String server: servers)
            			{
            				String[] hostandport = server.split(";");
            				ServerPrx tmpProxy = getProxy(hostandport[0], hostandport[1]);
            				Map<Integer, User> users = tmpProxy.getUsers();
    						for(Map.Entry<Integer, User> entry : users.entrySet()) {
    						    User value = entry.getValue();
    							//Logger.write("communication_botcleanup", "Process " + value.name);
    							User userState = tmpProxy.getState(entry.getKey());
    							if (userState != null && userState.channel == 0){
    								//Logger.write("communication_botcleanup", "Muting " + value.name);
    								userState.suppress = true;
    								tmpProxy.setState(userState);
    							}
    						}
            			}
    				}
    				catch(Exception ex){
    					Logger.write("communication_bot", "Error in processing " + ex.getMessage());
    				}
    				   
    				try {
    					Thread.sleep(1 * 1000);
    				}catch (InterruptedException e) {}
            	}
            	
			}
        };
        muteAllNotInCallThread.start();
	}
	public boolean notInCall(int userid, String customerId) {
		try{
			if(testmode)
				return true;
			//if (isServerRunning(customerId)){
				User userState = getProxy(customerId).getState(userid);
				if (userState != null && userState.channel == 0){
					return true;
				}
			//}
		}
		catch(Exception ex){
			
		}
		return false;
	}
	public String IsInCall(String mumbleUser, String customerId) {
		try{
			if(testmode)
				return "start";
			//if (isServerRunning(customerId)){
				User us = getProxy(customerId).getState(usernameToId(mumbleUser,customerId));
				if (us != null && us.channel != 0){
					for(Map.Entry<Integer, User> entry : getProxy(customerId).getUsers().entrySet()) {
					    User value = entry.getValue();
						//Logger.write("communication_botcleanup", "Process " + value.name);
						User userState = getProxy(customerId).getState(entry.getKey());
						if (userState != null && userState.channel == us.channel && userState.name.indexOf('_') == -1){
							return userState.name;
						}
					}
				}
			//}
		}
		catch(Exception ex){
			
		}
		return null;
	}
}

package Bot;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import com.google.gson.Gson;

import ibi.communicator.websocket.WebSocketHandler;
import shared.DatabaseManager;
import shared.Logger;

public class BotServiceHandler extends IoHandlerAdapter {
	private static final String CONFERENCE = "conference";
	private static final String CALL = "call";
	private static final String END = "end";
	private static final String FAILED = "failed call";
	private static final String UNMUTE = "unmute";
	private static final String ACCEPT = "accept";
	private static final String OPERATOR = "operator";
	private static final String PANIC = "panic";
	private static final String COMMUNICATION_BOT = "communication_bot";
	private static final String LASTREQUEST = "lastrequest";
	private static final String LASTREQUESTMUMBLE = "lastrequestmumble";
	private static final String HEARTBEAT = "heartbeat";
	private static final String RINGING = "ringing";
	private static final String PING = "ping";
	private static final String RING = "ring";
	private static final String DRIFTSCENTER = "Driftscenter";
	private static final String REJECT = "reject";
	private static final String CALLING = "calling";
	
	private IoAcceptor parentAcceptor = null;
	protected BotService botService = new BotService();
	Gson gson = new Gson();
	// private ReadWriteLock rwlock = new ReentrantReadWriteLock();
	// private Lock rlock = rwlock.readLock();
	// private Lock wl = rwl.writeLock();

	public BotServiceHandler() {
		// TODO:Shabeer:GetAllCustomers
		if (!BotService.testmode)
		{
			botService.cleanupChannels();
			botService.muteAllNotInCall();
		}
	}

	public BotServiceHandler(IoAcceptor acceptor) {
		this.parentAcceptor = acceptor;
		// TODO:Shabeer:GetAllCustomers
		if (!BotService.testmode)
		{
			botService.cleanupChannels();
			botService.muteAllNotInCall();
		}
		
	}

	@Override
	public void sessionCreated(IoSession session) {
		//Logger.write("communication_bot_ping", "session is created");
	}

	@Override
	public void sessionOpened(IoSession session) {
		//Logger.write("communication_bot_ping", "session is opened");
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		//Logger.write("communication_bot_ping", "exception in connection " + cause.getMessage());
	}

	private IoSession getActiveSession(String clientId) {
		Map<Long, IoSession> tmp = null;
		try {

			if (this.parentAcceptor != null) {
				tmp = new HashMap<Long, IoSession>(this.parentAcceptor.getManagedSessions());
				// this.parentAcceptor.getManagedSessions().
				// tmp.keySet().removeAll(target.keySet());
				// target.putAll(tmp);
				// Logger.write(COMMUNICATION_BOT, "check acceptor list");
				// for (Map.Entry<Long, IoSession> entry :
				// this.parentAcceptor.getManagedSessions().entrySet()){
				for (Map.Entry<Long, IoSession> entry : tmp.entrySet()) {
					// {
					// Long key = entry.getKey();
					// Logger.write("communication_bot", "Checking connection");
					IoSession tmpSession = entry.getValue();
					String tmpId = (String) tmpSession.getAttribute(LASTREQUESTMUMBLE);
					if (tmpId != null && tmpId.equalsIgnoreCase(clientId)) {
						return tmpSession;
					}
					ClientRequest request = (ClientRequest) tmpSession.getAttribute(LASTREQUEST);
					if (request != null) {
						String cid = String.format("%s_%s", request.getCustomerId(), request.getVehicleNo());
						if (clientId.equalsIgnoreCase(cid) && tmpSession.isConnected()) {

							return tmpSession;
						}
					}
				}
			}
		} catch (Exception ex) {

		} finally {
			tmp = null;
		}

		return null;
	}

	private void processCallCommand(IoSession session, ClientRequest request, String customerId) {
		List<RequestRecipient> recipients = request.getRecipients();
		List<RequestOperator> operators = request.getOperators();
		Logger.write(COMMUNICATION_BOT, "Request status " + request.getStatus());
		if (request.getStatus().isEmpty()) {
			if ((recipients != null && recipients.size() > 0) || (operators != null && operators.size() > 0)) {
				Logger.write(COMMUNICATION_BOT, "Request has recipients");

				Logger.write(COMMUNICATION_BOT, "Request type " + request.getType());
				if (request.getType().equalsIgnoreCase(OPERATOR)) {
					Logger.write(COMMUNICATION_BOT, "Pick the first recipient");
					RequestRecipient recipient = recipients.get(0);
					Logger.write(COMMUNICATION_BOT, "Recipient found " + recipient.getMumbleUser());
					try {				
						String tNumber = request.getWebUser();
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(recipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);
					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}

					if (botService.usernameToId(recipient.getMumbleUser(),customerId) == -1) {
						Logger.write(COMMUNICATION_BOT,
								"Sending ack to browser (recipient mumble user offline)");
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\",\"info\":\"vehicle mumble offline\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
						try {
							String tNumber = request.getWebUser();
							String cType = request.getCommand();
							String cSource = request.getType();
							String cEvnt = FAILED;
							String fEndpoint = request.getWebUser();
							String tEndpoint = gson.toJson(recipients);
							DatabaseManager.updateCallHistory(tNumber, 
															  cType, 
															  cSource, 
															  cEvnt, 
															  fEndpoint,
															  tEndpoint);
							
						} catch (Exception ex) {
							Logger.writeError(COMMUNICATION_BOT, ex);
						}

						// session.write(commandToSend);
					} else if (botService.usernameToId(request.getMumbleUser(),customerId) == -1) {
						
						Logger.write(COMMUNICATION_BOT,
								"Sending ack to browser (operator mumble offline)");
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\",\"info\":\"operator mumble offline\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
						try {
							String tNumber = request.getWebUser();
							String cType = request.getCommand();
							String cSource = request.getType();
							String cEvnt = FAILED;
							String fEndpoint = request.getWebUser();
							String tEndpoint = gson.toJson(recipients);
							DatabaseManager.updateCallHistory(tNumber, 
															  cType, 
															  cSource, 
															  cEvnt, 
															  fEndpoint,
															  tEndpoint);
						} catch (Exception ex) {
							Logger.writeError(COMMUNICATION_BOT, ex);
						}

						// session.write(commandToSend);
					} else if (request.getMumbleUser() == null || request.getMumbleUser().isEmpty()) {
						Logger.write(COMMUNICATION_BOT,
								"Sending ack to browser (no mumble yser mentioned)");
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\",\"info\":\"no mumble user\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
						// session.write(commandToSend);
					} else {
						IoSession tmpSession = getActiveSession(recipient.getMumbleUser());
						if (tmpSession != null) {
							Logger.write(COMMUNICATION_BOT,
									"Sending ack to browser (all is well)");
							String browserRequest = request.getRawdata();
							browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
							WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
							
							Logger.write(COMMUNICATION_BOT,
									"Found session for vehicle - 1 " + recipient.getMumbleUser());
							tmpSession.write(request.getRawdata());
							Logger.write(COMMUNICATION_BOT, "Command written to " + recipient.getMumbleUser());
							try {
								/*DatabaseManager.addCallData("call", recipient.getCustomer(), request.getMumbleUser(),
										recipient.getMumbleUser(), "NA", "forwarded");
										*/
								/*
								String tNumber = request.getWebUser();
								String cType = request.getCommand();
								String cSource = request.getType();
								String cEvnt = "forwarded";
								String fEndpoint = request.getWebUser();
								String tEndpoint = gson.toJson(recipients);
								DatabaseManager.updateCallHistory(tNumber, 
																  cType, 
																  cSource, 
																  cEvnt, 
																  fEndpoint,
																  tEndpoint);
								*/
								
							} catch (Exception ex) {
								Logger.writeError(COMMUNICATION_BOT, ex);
							}

						}
					}

				} else if (request.getType().equalsIgnoreCase("bus")) {
					Logger.write(COMMUNICATION_BOT, "Call from bus");
					String vehicleMumbleId = String.format("%s_%s", request.getCustomerId(), request.getVehicleNo());
					try {
						/*DatabaseManager.addCallData("call", request.getCustomerId(), vehicleMumbleId, "ALL", "NA",
								"received");*/
						/*
						String tNumber = vehicleMumbleId;
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = "received";
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(recipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);
						*/
					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}

					if (botService.usernameToId(vehicleMumbleId,customerId) == -1) {
						String commandToSend = String.format(
								"{\"command\":\"call\",\"status\":\"reject\",\"extrainfo\":\"your mumble is offline\",\"request\":%s}",
								request.getRawdata().replace("\n", "").replace("\r", ""));
						session.write(commandToSend);
						Logger.write(COMMUNICATION_BOT, "bus Call denied " + commandToSend + " for customer " + customerId);
						try{
							String tNumber = vehicleMumbleId;
							String cType = request.getCommand();
							String cSource = request.getType();
							String cEvnt = FAILED;
							String fEndpoint = request.getWebUser();
							String tEndpoint = request.getRecipients() == null ? DRIFTSCENTER :gson.toJson(request.getRecipients());
							DatabaseManager.updateCallHistory(tNumber, 
															  cType, 
															  cSource, 
															  cEvnt, 
															  fEndpoint,
															  tEndpoint);
						}catch(Exception ex){
							Logger.writeError(COMMUNICATION_BOT, ex);
						}
						
					} else {
						if (operators.size() > 0) {
							WebSocketHandler.broadcastToAllBrowsers(customerId, "", request.getRawdata());
							//WebSocketHandler.broadcastToSpecificOperators(request.getRawdata());
							Logger.write(COMMUNICATION_BOT, "Bus call forwarded to all operators ");
						}
					}
				}

			}
		} else if (request.getStatus().equalsIgnoreCase(ACCEPT)) {
			if (request.getOrignalRequest() != null) {
				if (request.getOrignalRequest().getType().equalsIgnoreCase(OPERATOR)) {
					List<RequestRecipient> orignalRecipients = request.getOrignalRequest().getRecipients();
					if (orignalRecipients.size() > 0) {
						if (botService.usernameToId(request.getOrignalRequest().getMumbleUser(),customerId) != -1
								&& botService.usernameToId(orignalRecipients.get(0).getMumbleUser(),customerId) != -1) {
							String channelName = botService.startSingleCall(request.getOrignalRequest().getMumbleUser(),
									orignalRecipients.get(0).getMumbleUser(),customerId);
							if (channelName != null && !channelName.isEmpty()) {
								try{
									String tNumber = request.getOrignalRequest().getWebUser();
									String cType = request.getOrignalRequest().getCommand();
									String cSource = request.getOrignalRequest().getType();
									String cEvnt = request.getStatus();
									String fEndpoint = request.getOrignalRequest().getWebUser();
									String tEndpoint = gson.toJson(orignalRecipients);
									DatabaseManager.updateCallHistory(tNumber, 
																	  cType, 
																	  cSource, 
																	  cEvnt, 
																	  fEndpoint,
																	  tEndpoint);
								}catch(Exception ex){
									Logger.writeError(COMMUNICATION_BOT, ex);
								}
								
							}
						} else {
							Logger.write(COMMUNICATION_BOT,
									String.format("One of %s and %s is not online",
											request.getOrignalRequest().getMumbleUser(),
											orignalRecipients.get(0).getMumbleUser()));
							String commandToSend = String.format(
									"{\"command\":\"call\",\"status\":\"reject\",\"extrainfo\":\"The called vehicle is offline in mumble\",\"request\":%s}",
									request.getRawdata().replace("\n", "").replace("\r", ""));
							session.write(commandToSend);
							WebSocketHandler.broadcastToBrowsers(request.getOrignalRequest().getWebUser(),
									commandToSend);
							try{
								String tNumber = request.getOrignalRequest().getWebUser();
								String cType = request.getOrignalRequest().getCommand();
								String cSource = request.getOrignalRequest().getType();
								String cEvnt = FAILED;
								String fEndpoint = request.getOrignalRequest().getWebUser();
								String tEndpoint = gson.toJson(orignalRecipients);
								DatabaseManager.updateCallHistory(tNumber, 
																  cType, 
																  cSource, 
																  cEvnt, 
																  fEndpoint,
																  tEndpoint);
							}catch(Exception ex){
								Logger.writeError(COMMUNICATION_BOT, ex);
							}
							
						}
					}
					Logger.write(COMMUNICATION_BOT, "Sending to WebSocket Server");
					if (request.getRawdata() == null || request.getRawdata().isEmpty()) {
						Logger.write(COMMUNICATION_BOT, "accept call empty");
					} else {
						WebSocketHandler.broadcastToBrowsers(request.getOrignalRequest().getWebUser(),
								request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Sent to WebSocket Server");
						session.write(request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Confirmation sent to vehicle");
					}

				} else {
					String vehicleMumbleId = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),
							request.getOrignalRequest().getVehicleNo());
					Logger.write(COMMUNICATION_BOT, "Vehicle Id " + vehicleMumbleId);

					if (request.getMumbleUser() != null && !request.getMumbleUser().isEmpty()) {
						if (botService.usernameToId(request.getMumbleUser(),customerId) != -1
								&& botService.usernameToId(vehicleMumbleId,customerId) != -1) {
							if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
								String browserRequest = request.getRawdata();
								browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
								WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
							}

							String channelName = botService.startSingleCall(request.getMumbleUser(), vehicleMumbleId,customerId);
							if (channelName != null && !channelName.isEmpty()) {
								try{
									String tNumber = vehicleMumbleId;
									String cType = request.getOrignalRequest().getCommand();
									String cSource = request.getOrignalRequest().getType();
									String cEvnt = request.getStatus();
									String fEndpoint = request.getOrignalRequest().getFromEndpoint();
									String tEndpoint = request.getWebUser();
									DatabaseManager.updateCallHistory(tNumber, 
																	  cType, 
																	  cSource, 
																	  cEvnt, 
																	  fEndpoint,
																	  tEndpoint);
								}catch(Exception ex){
									Logger.writeError(COMMUNICATION_BOT, ex);
								}
								
							}
							IoSession tmpSession = getActiveSession(vehicleMumbleId);
							if (tmpSession != null) {
								Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 2 " + vehicleMumbleId);
								tmpSession.write(request.getRawdata());
								Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
							}
						} else {
							Logger.write(COMMUNICATION_BOT, String.format("One of %s and %s is not online",
									request.getMumbleUser(), vehicleMumbleId));
							String commandToSend = String.format(
									"{\"command\":\"call\",\"status\":\"reject\",\"extrainfo\":\"The called vehicle is offline in mumble\",\"request\":%s}",
									request.getRawdata().replace("\n", "").replace("\r", ""));
							session.write(commandToSend);
							if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
								String browserRequest = request.getRawdata();
								browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
								WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
							}
							
							try{
								String tNumber = vehicleMumbleId;
								String cType = request.getOrignalRequest().getCommand();
								String cSource = request.getOrignalRequest().getType();
								String cEvnt = FAILED;
								String fEndpoint = request.getOrignalRequest().getFromEndpoint();
								String tEndpoint = request.getWebUser();
								DatabaseManager.updateCallHistory(tNumber, 
																  cType, 
																  cSource, 
																  cEvnt, 
																  fEndpoint,
																  tEndpoint);

							}catch(Exception ex){
								Logger.writeError(COMMUNICATION_BOT, ex);
							}
							
						}

						Logger.write(COMMUNICATION_BOT,
								"Call started for " + vehicleMumbleId + ", and " + request.getMumbleUser() == null
										? request.getOrignalRequest().getMumbleUser() : request.getMumbleUser());
					}

					
				}
			}

		} else if (request.getStatus().equalsIgnoreCase("reject")) {
			if (request.getOrignalRequest() != null) {
				if (request.getOrignalRequest().getType().equalsIgnoreCase(OPERATOR)) {
					WebSocketHandler.broadcastToAllBrowsers(customerId, "",request.getRawdata());
					try {
						String tNumber = request.getOrignalRequest().getWebUser();
						String cType = request.getOrignalRequest().getCommand();
						String cSource = request.getOrignalRequest().getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getOrignalRequest().getWebUser();
						String tEndpoint = gson.toJson(request.getRecipients());
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}

				} else {
					if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					}
					String vehicleMumbleId = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),
							request.getOrignalRequest().getVehicleNo());
					IoSession tmpSession = getActiveSession(vehicleMumbleId);
					if (tmpSession != null) {
						Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 3 " + vehicleMumbleId);
						tmpSession.write(request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
					}
					try {
						String tNumber = vehicleMumbleId;
						String cType = request.getOrignalRequest().getCommand();
						String cSource = request.getOrignalRequest().getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getOrignalRequest().getFromEndpoint();
						String tEndpoint = request.getRecipients() == null ? DRIFTSCENTER :gson.toJson(request.getRecipients());;
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				}
			} else {
				if (request.getType().equalsIgnoreCase(OPERATOR)) {
					if (request.getRecipients() != null) {
						if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
							String browserRequest = request.getRawdata();
							browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
							WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
						}
						RequestRecipient recipient = request.getRecipients().get(0);
						String mumbleUser = recipient.getMumbleUser();
						IoSession tmpSession = getActiveSession(mumbleUser);
						Logger.write(COMMUNICATION_BOT, "Forwarding to vehicle " + mumbleUser);
						if (tmpSession != null) {
							tmpSession.write(request.getRawdata());
							Logger.write(COMMUNICATION_BOT, "Sent to vehicle " + mumbleUser);
						}
						try {
							String tNumber = request.getWebUser();
							String cType = request.getCommand();
							String cSource = request.getType();
							String cEvnt = request.getStatus();
							String fEndpoint = request.getWebUser();
							String tEndpoint = gson.toJson(request.getRecipients());
							DatabaseManager.updateCallHistory(tNumber, 
															  cType, 
															  cSource, 
															  cEvnt, 
															  fEndpoint,
															  tEndpoint);

						} catch (Exception ex) {
							Logger.writeError(COMMUNICATION_BOT, ex);
						}
					}

				}
			}
		} else if (request.getStatus().equalsIgnoreCase("busy")) {
			if (request.getOrignalRequest() != null) {
				if (request.getOrignalRequest().getType().equalsIgnoreCase(OPERATOR)) {
					WebSocketHandler.broadcastToAllBrowsers(customerId, "",request.getRawdata());
					session.write(request.getRawdata());
					String tNumber = request.getWebUser();
					String cType = request.getCommand();
					String cSource = request.getType();
					String cEvnt = request.getStatus();
					String fEndpoint = request.getWebUser();
					String tEndpoint = gson.toJson(request.getRecipients());
					DatabaseManager.updateCallHistory(tNumber, 
													  cType, 
													  cSource, 
													  cEvnt, 
													  fEndpoint,
													  tEndpoint);
				} else {
					if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					}
					String vehicleMumbleId = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),
							request.getOrignalRequest().getVehicleNo());
					IoSession tmpSession = getActiveSession(vehicleMumbleId);
					if (tmpSession != null) {
						Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 4 " + vehicleMumbleId);
						tmpSession.write(request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
					}
					try {
						String tNumber = request.getOrignalRequest().getWebUser();
						String cType = request.getOrignalRequest().getCommand();
						String cSource = request.getOrignalRequest().getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getOrignalRequest().getWebUser();
						String tEndpoint = gson.toJson(request.getRecipients());
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				}
			}
		} else if (request.getStatus().equalsIgnoreCase("decline")) {
			Logger.write(COMMUNICATION_BOT, "Process end command: " + request.getType());
			// if (request.getOrignalRequest() != null) {
			if (request.getOrignalRequest() != null
					&& request.getOrignalRequest().getType().equalsIgnoreCase(OPERATOR)) {
				Logger.write(COMMUNICATION_BOT, "Declined from bus");
				WebSocketHandler.broadcastToAllBrowsers(customerId, "",request.getRawdata());
				session.write(request.getRawdata());
				try {
					String tNumber = request.getOrignalRequest().getWebUser();
					String cType = request.getCommand();
					String cSource = request.getOrignalRequest().getType();
					String cEvnt = request.getStatus();
					String fEndpoint = request.getOrignalRequest().getWebUser();
					String tEndpoint = gson.toJson(request.getOrignalRequest().getRecipients());
					DatabaseManager.updateCallHistory(tNumber, 
													  cType, 
													  cSource, 
													  cEvnt, 
													  fEndpoint,
													  tEndpoint);

				} catch (Exception ex) {
					Logger.writeError(COMMUNICATION_BOT, ex);
				}
			} else if (request.getOrignalRequest() != null
					&& request.getOrignalRequest().getType().equalsIgnoreCase("bus")) {
				if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
					String browserRequest = request.getRawdata();
					browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
				}
				Logger.write(COMMUNICATION_BOT, "Declined from operator");
				WebSocketHandler.broadcastToAllBrowsers(customerId,"",request.getRawdata());
				session.write(request.getRawdata());
				
				String tNumber = String.format("%s_%s", request.getCustomerId(),request.getVehicleNo());
				String cType = request.getCommand();
				String cSource = request.getType();
				String cEvnt = request.getStatus();
				String fEndpoint = request.getFromEndpoint();
				String tEndpoint = request.getRecipients() == null ? DRIFTSCENTER :gson.toJson(request.getRecipients());
				DatabaseManager.updateCallHistory(tNumber, 
												  cType, 
												  cSource, 
												  cEvnt, 
												  fEndpoint,
												  tEndpoint);
				

			} else if (request.getType().equalsIgnoreCase("bus")) {
				WebSocketHandler.broadcastToAllBrowsers(customerId,"",request.getRawdata());
				
				String tNumber = String.format("%s_%s", request.getCustomerId(),request.getVehicleNo());
				String cType = request.getCommand();
				String cSource = request.getType();
				String cEvnt = request.getStatus();
				String fEndpoint = request.getFromEndpoint();
				String tEndpoint = request.getRecipients() == null ? DRIFTSCENTER :gson.toJson(request.getRecipients());
				DatabaseManager.updateCallHistory(tNumber, 
												  cType, 
												  cSource, 
												  cEvnt, 
												  fEndpoint,
												  tEndpoint);
			} else {
				if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
					String browserRequest = request.getRawdata();
					browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
				}
				Logger.write(COMMUNICATION_BOT, "Call declined from operator");
				String vehicleMumbleId = String.format("%s_%s", request.getCustomerId(), request.getVehicleNo());
				Logger.write(COMMUNICATION_BOT, String.format("Sending decline to %s", vehicleMumbleId));
				IoSession tmpSession = getActiveSession(vehicleMumbleId);
				if (tmpSession != null) {
					Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 5 " + vehicleMumbleId);
					tmpSession.write(request.getRawdata());
					Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
				}
			}
			// }
		} else if (request.getStatus().equalsIgnoreCase(END)) {
			Logger.write(COMMUNICATION_BOT, "Process end command: " + request.getType());
			if (request.getType() != null && request.getType().equalsIgnoreCase(OPERATOR)) {
				if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
					String browserRequest = request.getRawdata();
					browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
				}
				Logger.write(COMMUNICATION_BOT, "Call ended from operator");
				List<RequestRecipient> endRecipients = request.getRecipients();
				if (endRecipients.size() > 0) {
					Logger.write(COMMUNICATION_BOT, "Call ended from operator");
					botService.endSingleCall(request.getMumbleUser(), endRecipients.get(0).getMumbleUser(),customerId);
					Logger.write(COMMUNICATION_BOT, "Mumble cleaned up");
					try {
						String tNumber = request.getWebUser();
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(request.getRecipients());
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				}
				// WebSocketHandler.broadcastToBrowsers(request.getWebUser(),request.getRawdata());
				IoSession tmpSession = getActiveSession(endRecipients.get(0).getMumbleUser());
				if (tmpSession != null) {
					Logger.write(COMMUNICATION_BOT, "Client session found for " + endRecipients.get(0).getMumbleUser());
					tmpSession.write(request.getRawdata());
					Logger.write(COMMUNICATION_BOT, "end call request sent to " + endRecipients.get(0).getMumbleUser());
				}
			} else {
				Logger.write(COMMUNICATION_BOT, "Call ended from vehicle");
				if (request.getOrignalRequest() != null
						&& request.getOrignalRequest().getType().equalsIgnoreCase(OPERATOR)) {
					Logger.write(COMMUNICATION_BOT, "original call was from operator");
					List<RequestRecipient> orignalRecipients = request.getOrignalRequest().getRecipients();
					if (orignalRecipients.size() > 0) {
						Logger.write(COMMUNICATION_BOT, String.format("Cleaning mumble call for %s, %s",
								request.getOrignalRequest().getMumbleUser(), orignalRecipients.get(0).getMumbleUser()));
						botService.endSingleCall(request.getOrignalRequest().getMumbleUser(),
								orignalRecipients.get(0).getMumbleUser(),customerId);
					}
					WebSocketHandler.broadcastToAllBrowsers(customerId,"",request.getRawdata());
					Logger.write(COMMUNICATION_BOT, String.format("Operators informed of called ended"));
					session.write(request.getRawdata());
					try {
						String tNumber = request.getOrignalRequest().getWebUser();
						String cType = request.getOrignalRequest().getCommand();
						String cSource = request.getOrignalRequest().getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getOrignalRequest().getWebUser();
						String tEndpoint = gson.toJson(orignalRecipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				} else if (request.getOrignalRequest() != null
						&& request.getOrignalRequest().getType().equalsIgnoreCase("bus")) {
					if (request.getAck() != null && request.getAck().equalsIgnoreCase("false")){
						String browserRequest = request.getRawdata();
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
						WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					}
					Logger.write(COMMUNICATION_BOT, "original call was from operator");
					String vehicleMumbleId = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),
							request.getOrignalRequest().getVehicleNo());
					botService.endSingleCall(vehicleMumbleId, vehicleMumbleId,customerId);
					IoSession tmpSession = getActiveSession(vehicleMumbleId);
					if (tmpSession != null) {
						Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 6 " + vehicleMumbleId);
						tmpSession.write(request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
					}
					try {
						String tNumber = vehicleMumbleId;
						String cType = request.getOrignalRequest().getCommand();
						String cSource = request.getOrignalRequest().getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getOrignalRequest().getFromEndpoint();
						String tEndpoint = request.getWebUser();
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				} else {
					String vehicleMumbleId = String.format("%s_%s", request.getCustomerId(), request.getVehicleNo());
					Logger.write(COMMUNICATION_BOT, "Vehicle Id " + vehicleMumbleId);
					// List<RequestOperator> orignalOperators =
					// request.getOrignalRequest().getOperators();
					botService.endSingleCall(vehicleMumbleId, vehicleMumbleId,customerId);
					Logger.write(COMMUNICATION_BOT, "Call ended for " + vehicleMumbleId);
					try {
						String tNumber = vehicleMumbleId;
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = request.getStatus();
						String fEndpoint = request.getFromEndpoint();
						String tEndpoint = request.getRecipients() == null ? DRIFTSCENTER :gson.toJson(request.getRecipients());;
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);
						
					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
					IoSession tmpSession = getActiveSession(vehicleMumbleId);
					if (tmpSession != null) {
						Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 7 " + vehicleMumbleId);
						tmpSession.write(request.getRawdata());
						Logger.write(COMMUNICATION_BOT, "Command written to " + vehicleMumbleId);
					}
					Logger.write(COMMUNICATION_BOT,
							String.format("Command written to Websocket %s", request.getRawdata()));
					// WebSocketHandler.broadcastToBrowsers(orignalOperators.get(0).getId(),
					// request.getRawdata());
					WebSocketHandler.broadcastToAllBrowsers(customerId,"",request.getRawdata());
				}
			}
		}
	}

	public HashMap<Long, IoSession> copy(Map<Long, IoSession> map) {
		HashMap<Long, IoSession> copy = new HashMap<Long, IoSession>();
		for (Map.Entry<Long, IoSession> entry : map.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
	}

	@Override
	public void messageReceived(IoSession session, Object message) {
		try {
			
			 //Logger.write("communication_bot", "message is received : " + message.toString().trim());
			 
			String clientData = message.toString().trim();
			//if (clientData.indexOf(HEARTBEAT) == -1)
			//	 return;
			if (clientData == null || clientData.isEmpty() || clientData.equalsIgnoreCase("null")) {
				return;
			}
			if (clientData.indexOf(PING) == -1 && clientData.indexOf(HEARTBEAT) == -1) {
				Logger.write(COMMUNICATION_BOT, String.format("The message %s received from %s", clientData,
						session.getRemoteAddress().toString()));
			} else {
				//Logger.write(COMMUNICATION_BOT + "_PING", clientData);
				// if (clientData.indexOf("ping") != -1){
				// session.write("{\"command\":\"ping\", \"status\":\"ack\",
				// \"mumbleclientstatus\":\"\"}");
				// }
			}
			if (clientData.trim().equalsIgnoreCase("quit")) {
				session.close();
				return;
			}
			
			ClientRequest request = new ClientRequest(clientData);
			String customerId = request.getMumbleCustomerId();
			if (!DatabaseManager.getIBICustomer(customerId).isEmpty()){
				customerId = DatabaseManager.getIBICustomer(customerId);
			}
			//Logger.write("communication_mumble", String.format("Request belongs to customer %s", customerId));
			if (request.getCommand().equalsIgnoreCase(PING)) {
				try {
					System.out.println("Ping arrives " + clientData);
					Logger.write(COMMUNICATION_BOT + "_PINGS", clientData);
					Logger.write(COMMUNICATION_BOT + "_PINGS", String.format("request.getCustomerId() %s ,request.getVehicleNo() %s ,customerId %s", request.getCustomerId(),request.getVehicleNo(),customerId));
					int nUID = botService
							.usernameToId(String.format("%s_%s", request.getCustomerId(), request.getVehicleNo()),customerId);
					Logger.write(COMMUNICATION_BOT + "_PINGS", String.format("nUID %s ", nUID));
					if (nUID == -1) {
						session.write("{\"command\":\"ping\", \"status\":\"ack\", \"mumbleclientstatus\":\"0\"}");
					}
				} catch (Exception ex) {

				}
			}
			if (request.getCommand().equalsIgnoreCase(PANIC)) {
				Logger.write(COMMUNICATION_BOT, "panic >> 1");
			}

			boolean gpsUpdated = false;
			if (!request.getCommand().equalsIgnoreCase(PING)) {
				if (request.getCommand().equalsIgnoreCase(PANIC)) {
					Logger.write(COMMUNICATION_BOT, "panic >> 2");
				}
				// Logger.write("communication_bot", "command received is" +
				// clientData);
				ClientRequest lastRequest = (ClientRequest) session.getAttribute(LASTREQUEST);
				if (request.getCommand().equalsIgnoreCase(PANIC)) {
					Logger.write(COMMUNICATION_BOT, "panic >> 3");
				}
				if (lastRequest == null)
					gpsUpdated = true;
				else {
					if (request.getCommand().equalsIgnoreCase(PING)) {
						if (!request.getLat().equalsIgnoreCase(lastRequest.getLat())
								|| request.getLng().equalsIgnoreCase(lastRequest.getLng())) {
							gpsUpdated = true;
						}
					}
				}
				if (lastRequest != null && lastRequest.getCommand().equals(PING)) {

				} else {
					session.setAttribute(LASTREQUEST, request);
				}
			} else {
				session.setAttribute(LASTREQUEST, request);
				session.setAttribute(LASTREQUESTMUMBLE, request.getPingMumbleUser());
			}
			if (request.isParsed()) {
				
				if (request.getCommand().equalsIgnoreCase(RING)) {
					Logger.write(COMMUNICATION_BOT, "Ring command");
					if (request.getRecipients() != null && request.getRecipients().size() > 0) {
						Logger.write(COMMUNICATION_BOT, "Ring command has recipients");
						botService.sendVoiceMessage(request.getRecipients().get(0).getMumbleUser(), RINGING,customerId);
						Logger.write(COMMUNICATION_BOT, "Message sent to mumble client");
					}
				} else if (request.getCommand().equalsIgnoreCase(HEARTBEAT)) {
					String browserRequest = request.getRawdata();
					String mumbUser = request.getMumbleUser();
					//Logger.write("communication_mumble", String.format("Checking user %s for customer %s", mumbUser,customerId) );
					if (botService.usernameToId(mumbUser, customerId)== -1){
						//Logger.write("communication_mumble", "Could not be verified");
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\", \"voicestatus\":\"false\"");
					}else{
						//Logger.write("communication_mumble", "Verified");
						browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\", \"voicestatus\":\"true\"");	
					}
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					if (this.parentAcceptor != null) {
						try {
							for (Map.Entry<Long, IoSession> entry : this.copy(this.parentAcceptor.getManagedSessions())
									.entrySet()) {
								// Long key = entry.getKey();
								IoSession tmpSession = entry.getValue();
								if (tmpSession != null && tmpSession.isConnected() && tmpSession.isActive()) {
									tmpSession.write(clientData + System.lineSeparator());
								} else {

									Logger.write(COMMUNICATION_BOT, "An invalid session exists");
								}
							}
						} catch (Exception ex) {
							Logger.writeError(COMMUNICATION_BOT, ex);
						}

					}

				} else if (request.getCommand().equalsIgnoreCase(PANIC)) {
					Logger.write(COMMUNICATION_BOT, "panic >> panic data " + clientData);
					if (request.getType().equalsIgnoreCase("bus")) {
						if (botService.usernameToId(String.format("%s_%s", request.getPanicRequest().getCustomerId(),
								request.getPanicRequest().getVehicleNumber()),customerId) == -1) {
							String commandToSend = String.format(
									"{\"command\":\"panic\",\"type\":\"operator\",\"webuser\":\"server\",\"mumbleuser\":\"server\",\"status\":\"end\",\"extrainfo\":\"your mumble is not online\",\"request\":%s}",
									request.getRawdata().replace("\n", "").replace("\r", ""));
							session.write(commandToSend);
							Logger.write(COMMUNICATION_BOT,
									"panic >> panic call rejected - no mumble " + commandToSend);
						} else {
							if (!DatabaseManager.isPanicEnabled(request.getPanicRequest().getCustomerId(),
									request.getPanicRequest().getVehicleNumber())) {
								String commandToSend = String.format(
										"{\"command\":\"panic\",\"type\":\"operator\",\"webuser\":\"server\",\"mumbleuser\":\"server\",\"status\":\"end\",\"extrainfo\":\"Panic calls from this bus not allowed\",\"request\":%s}",
										request.getRawdata().replace("\n", "").replace("\r", ""));
								session.write(commandToSend);
								Logger.write(COMMUNICATION_BOT, "panic >> panic call rejected " + commandToSend);
							} else {
								Logger.write(COMMUNICATION_BOT, "panic >> Call from bus");
								WebSocketHandler.broadcastToAllBrowsers(customerId,"",clientData);
								Logger.write(COMMUNICATION_BOT, "panic >> Written to web socket");
								try{
									String tNumber = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),request.getOrignalRequest().getVehicleNo());
									String cType = request.getOrignalRequest().getCommand();
									String cSource = request.getOrignalRequest().getType();
									String cEvnt = END;
									String fEndpoint = "{\"customer\":\"" + request.getOrignalRequest().getCustomerId() + "\",\"vehiclenumber\":\""+ request.getOrignalRequest().getVehicleNo() +"\"}";
									String tEndpoint = request.getOrignalRequest().getWebUser();
									DatabaseManager.updateCallHistory(tNumber, 
																	  cType, 
																	  cSource, 
																	  cEvnt, 
																	  fEndpoint,
																	  tEndpoint);

								}catch(Exception ex){
									Logger.writeError(COMMUNICATION_BOT, ex);
								}
								
								Logger.write(COMMUNICATION_BOT, "panic >> Added to database");
							}
						}

					} else if (request.getType().equalsIgnoreCase(OPERATOR)) {
						Logger.write(COMMUNICATION_BOT, "panic >> Panic response from browser");
						if (request.getStatus().equalsIgnoreCase(ACCEPT)) {
							Logger.write(COMMUNICATION_BOT, "panic >> Call accepted");
							int user1 = botService.usernameToId(request.getMumbleUser(),customerId);
							int user2 = botService
									.usernameToId(request.getOrignalRequest().getPanicRequest().getMumbleUser(),customerId);
							if (user1 > -1 && user2 > -1) {
								String browserRequest = request.getRawdata();
								browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
								WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
								String channelName = botService.startPanicCall(request.getMumbleUser(),
										request.getOrignalRequest().getPanicRequest().getMumbleUser(),customerId);
								try{
									/*DatabaseManager.addCallData(PANIC,
										request.getOrignalRequest().getPanicRequest().getCustomerId(),
										request.getOrignalRequest().getPanicRequest().getMumbleUser(),
										request.getMumbleUser(), channelName, "accepted");*/
									/*
									String tNumber = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),request.getOrignalRequest().getVehicleNo());
									String cType = request.getOrignalRequest().getCommand();
									String cSource = request.getOrignalRequest().getType();
									String cEvnt = request.getCommand();
									String fEndpoint = "{\"customer\":\"" + request.getOrignalRequest().getCustomerId() + "\",\"vehiclenumber\":\""+ request.getOrignalRequest().getVehicleNo() +"\"}";
									String tEndpoint = request.getOrignalRequest().getWebUser();
									DatabaseManager.updateCallHistory(tNumber, 
																	  cType, 
																	  cSource, 
																	  cEvnt, 
																	  fEndpoint,
																	  tEndpoint);
									 */
								}catch(Exception ex){
									Logger.writeError(COMMUNICATION_BOT, ex);
								}
								
								IoSession tmpSession = getActiveSession(
										request.getOrignalRequest().getPanicRequest().getMumbleUser());
								if (tmpSession != null) {
									Logger.write(COMMUNICATION_BOT, "panic >> Found session for vehicle - 8 "
											+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
									tmpSession.write(clientData + System.lineSeparator());
									Logger.write(COMMUNICATION_BOT, "panic >> Command written to "
											+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
								} else {
									Logger.write(COMMUNICATION_BOT, "panic >> No session for vehicle "
											+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
								}
								Logger.write(COMMUNICATION_BOT, "panic >> Added to database");
							} else {
								String browserRequest = request.getRawdata();
								browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\", \"info\":\"mumble user offline\"");
								WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
								Logger.write(COMMUNICATION_BOT, "panic >>  Not all users are online for call >>>");
								IoSession tmpSession = getActiveSession(
										request.getOrignalRequest().getPanicRequest().getMumbleUser());
								if (user2 == -1) {
									if (tmpSession != null) {
										Logger.write(COMMUNICATION_BOT, "panic >> Found session for vehicle - 9 "
												+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
										tmpSession
												.write("{\"command\":\"panic\", \"type\":\"sevrer\", \"status\":\"checkmumble\"}"
														+ System.lineSeparator());
										Logger.write(COMMUNICATION_BOT, "Command written to "
												+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
									}
								}
								if (user1 == -1) {
									Logger.write(COMMUNICATION_BOT, "panic >> back to browser/vehicle");
									session.write(
											"{\"command\":\"panic\", \"type\":\"sevrer\", \"status\":\"checkmumble\"}"
													+ System.lineSeparator());
								}
							}

						} else if (request.getStatus().equalsIgnoreCase(UNMUTE)) {
							Logger.write(COMMUNICATION_BOT, "Unmute received");
							String browserRequest = request.getRawdata();
							browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
							WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
							String channelName = botService.muteUser(request.getMumbleUser(), false,customerId);
							try{
								/*DatabaseManager.addCallData(PANIC,
									request.getOrignalRequest().getPanicRequest().getCustomerId(),
									request.getOrignalRequest().getPanicRequest().getMumbleUser(),
									request.getMumbleUser(), channelName, "unmuted");
									*/
								/*String tNumber = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),request.getOrignalRequest().getVehicleNo());
								String cType = request.getOrignalRequest().getCommand();
								String cSource = request.getOrignalRequest().getType();
								String cEvnt = request.getCommand();
								String fEndpoint = "{\"customer\":\"" + request.getOrignalRequest().getCustomerId() + "\",\"vehiclenumber\":\""+ request.getOrignalRequest().getVehicleNo() +"\"}";
								String tEndpoint = request.getOrignalRequest().getWebUser();
								DatabaseManager.updateCallHistory(tNumber, 
																  cType, 
																  cSource, 
																  cEvnt, 
																  fEndpoint,
																  tEndpoint);
								 */
							}catch(Exception ex){
								Logger.writeError(COMMUNICATION_BOT, ex);
							}
							
							Logger.write(COMMUNICATION_BOT, "Added to database");
						} else if (request.getStatus().equalsIgnoreCase(END)) {
							String browserRequest = request.getRawdata();
							browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
							WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
							Logger.write(COMMUNICATION_BOT, "Panic call ended");
							botService.endSingleCall(request.getMumbleUser(),
									request.getOrignalRequest().getPanicRequest().getMumbleUser(),customerId);
							try{
								/*DatabaseManager.addCallData(PANIC,
									request.getOrignalRequest().getPanicRequest().getCustomerId(),
									request.getOrignalRequest().getPanicRequest().getMumbleUser(),
									request.getMumbleUser(), "", "ended");*/
								/*String tNumber = String.format("%s_%s", request.getOrignalRequest().getCustomerId(),request.getOrignalRequest().getVehicleNo());
								String cType = request.getOrignalRequest().getCommand();
								String cSource = request.getOrignalRequest().getType();
								String cEvnt = request.getCommand();
								String fEndpoint = "{\"customer\":\"" + request.getOrignalRequest().getCustomerId() + "\",\"vehiclenumber\":\""+ request.getOrignalRequest().getVehicleNo() +"\"}";
								String tEndpoint = request.getOrignalRequest().getWebUser();
								DatabaseManager.updateCallHistory(tNumber, 
																  cType, 
																  cSource, 
																  cEvnt, 
																  fEndpoint,
																  tEndpoint);
								 */
							}catch(Exception ex){
								Logger.writeError(COMMUNICATION_BOT, ex);
							}
							
							Logger.write(COMMUNICATION_BOT, "Added to database");
							IoSession tmpSession = getActiveSession(
									request.getOrignalRequest().getPanicRequest().getMumbleUser());
							if (tmpSession != null) {
								Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 10 "
										+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
								tmpSession.write(clientData + System.lineSeparator());
								Logger.write(COMMUNICATION_BOT, "Command written to "
										+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
							} else {
								Logger.write(COMMUNICATION_BOT, "No session for vehicle "
										+ request.getOrignalRequest().getPanicRequest().getMumbleUser());
							}
						}
					}
				} else if (request.getCommand().equalsIgnoreCase(CALL)) {
					Logger.write(COMMUNICATION_BOT, "call data " + clientData);
					processCallCommand(session, request,customerId);
				} else if (request.getCommand().equalsIgnoreCase(CONFERENCE)) {
					Logger.write(COMMUNICATION_BOT, "conferencecall data " + clientData);
					processConferenceCallCommand(session, request,customerId);
				} else if (request.getCommand().equalsIgnoreCase(PING)) {
					// Logger.write("communication_bot", "ping data " +
					// clientData);
					botService.muteIfNotInCall(String.format("%s_%s", request.getCustomerId(), request.getVehicleNo()),customerId);
					boolean latlgnChanged = DatabaseManager.addPing(request.getCustomerId(), request.getVehicleNo(),
							request.getLine(), request.getLat(), request.getLng(), request.getDestination(), request.getPtaJourneyid(), request.getJourneytype());
					// Shabeer
					if (latlgnChanged) {
						int nUserId = botService
								.usernameToId(String.format("%s_%s", request.getCustomerId(), request.getVehicleNo()),customerId);
						if (nUserId != -1) {
							boolean notInCall = botService.notInCall(nUserId,customerId);
							if (!notInCall) {
								WebSocketHandler.broadcastToAllBrowsers(customerId,"",clientData);
							}
						}
					}
					// End Shabeer
					// WebSocketHandler.broadcastToAllBrowsers(clientData);
				}
			} else {
				if (request.getCommand().equalsIgnoreCase(PANIC)) {
					Logger.write(COMMUNICATION_BOT, "panic >> 6");
				}
				session.write("Invalid Data - JsonFormat Error");

			}
		} catch (Exception ex) {
			Logger.write(COMMUNICATION_BOT,
					"Exception in messageReceived " + ex.getMessage() + ", for message " + message.toString().trim());
			Logger.writeError(COMMUNICATION_BOT, ex);
		}
		// session.write("Session id is " + session.getId());
		/*
		 * if (clientData != null){ if (clientData.startsWith("panic:")){
		 * System.out.println("Panic call"); response =
		 * processPanicCall(clientData.substring(clientData.indexOf("panic:") +
		 * 6)); System.out.println("response " + response); }else if
		 * (clientData.startsWith("hangup:")){ System.out.println("Hangup call"
		 * ); response =
		 * processHangupCall(clientData.substring(clientData.indexOf("hangup:")
		 * + 7)); System.out.println("response " + response); }else if
		 * (clientData.startsWith("ping:")) { System.out.println("Ping call");
		 * response = processPingCall(clientData); System.out.println(
		 * "response " + response); }else{ response +=
		 * "COMMAND_NOT_IMPLEMENTED"; System.out.println(
		 * "Command not implemented");
		 * 
		 * } this. session.write(response); System.out.println(
		 * "Request processed: "); } else { session.write("No data received"
		 * .getBytes()); }
		 */
		// System.out.println("Message written...");
	}

	/*
	 * private synchronized String processPingCall(String message) {
	 * WebSocketHandler.broadcastToBrowsers(message); return "acknowledged"; }
	 * 
	 * private synchronized String processHangupCall(String clientData) {
	 * 
	 * String result = ""; try{ System.out.println(
	 * "processHangupCall called with " + clientData); String userName =
	 * clientData.trim(); String[] userList =
	 * botService.getRelevantUsers(userName); System.out.println("Ending call");
	 * botService.endCall(userList); result = "Call ended"; System.out.println(
	 * "Call started"); WebSocketHandler.broadcastToBrowsers("","Call Ended");
	 * }catch(Exception ex){ System.out.println(ex.getMessage());
	 * ex.printStackTrace(); result = ex.getMessage(); } return result; }
	 * 
	 * private synchronized String processPanicCall(String clientData) { String
	 * result = ""; try{ System.out.println("processPanicCall called with " +
	 * clientData); String userName = clientData.trim(); String[] userList =
	 * botService.getRelevantUsers(userName); if (userList.length == 1){ result
	 * = "No one available for call"; } System.out.println("Starting call");
	 * botService.startCall(userList); result = "Call initiated";
	 * System.out.println("Call started"); WebSocketHandler.broadcastToBrowsers(
	 * "Call started"); }catch(Exception ex){
	 * System.out.println(ex.getMessage()); ex.printStackTrace(); result =
	 * ex.getMessage(); } return result; }
	 */

	private void processConferenceCallCommand(IoSession session, ClientRequest request, String customerId) {
		List<RequestRecipient> recipients = request.getRecipients();
		Logger.write(COMMUNICATION_BOT, "Request status " + request.getStatus());
		try {
			/*DatabaseManager.addCallData("conference", "",
					request.getMumbleUser(), request.getRawdata(), "from operator", "received");*/
			/*String tNumber = request.getCustomerId(),request.getVehicleNo());
			String cType = request.getOrignalRequest().getCommand();
			String cSource = request.getOrignalRequest().getType();
			String cEvnt = request.getCommand();
			String fEndpoint = "{\"customer\":\"" + request.getOrignalRequest().getCustomerId() + "\",\"vehiclenumber\":\""+ request.getOrignalRequest().getVehicleNo() +"\"}";
			String tEndpoint = gson.toJson(recipients);
			DatabaseManager.updateCallHistory(tNumber, 
											  cType, 
											  cSource, 
											  cEvnt, 
											  fEndpoint,
											  tEndpoint);
			 */
		} catch (Exception ex) {
			Logger.writeError(COMMUNICATION_BOT, ex);
		}
		if (request.getStatus().isEmpty()) {
			if (botService.usernameToId(request.getMumbleUser(),customerId) == -1) {
				String commandToSend = String.format(
						"{\"command\":\"call\",\"status\":\"reject\",\"extrainfo\":\"your mumble is offline\",\"request\":%s}",
						request.getRawdata().replace("\n", "").replace("\r", ""));
				Logger.write(COMMUNICATION_BOT,
						"Sending ack to browser ");
				String browserRequest = request.getRawdata();
				browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
				WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
				//session.write(browserRequest);
				try {
					String tNumber = request.getWebUser();
					String cType = request.getCommand();
					String cSource = request.getType();
					String cEvnt = FAILED;
					String fEndpoint = request.getWebUser();
					String tEndpoint = gson.toJson(recipients);
					DatabaseManager.updateCallHistory(tNumber, 
													  cType, 
													  cSource, 
													  cEvnt, 
													  fEndpoint,
													  tEndpoint);

				} catch (Exception ex) {
					Logger.writeError(COMMUNICATION_BOT, ex);
				}
				Logger.write(COMMUNICATION_BOT, "Call denied " + commandToSend);
			} else if (recipients.size() > 0) {
				int nOnlineVehicles = 0;
				for (int index = 0; index < recipients.size(); index++) {
					RequestRecipient recipient = recipients.get(index);
					String mumbleUser = recipient.getMumbleUser();
					if (botService.usernameToId(recipient.getMumbleUser(),customerId) == -1) {
						continue;
					}
					nOnlineVehicles++;
					// Logger.write("communication_bot", String.format("Check if
					// %s is already in call", mumbleUser));
					String userInCall = botService.IsInCall(mumbleUser,customerId);
					if (userInCall != null && !userInCall.isEmpty()) {
						Logger.write(COMMUNICATION_BOT,
								String.format("The user %s is already in call with %s", mumbleUser, userInCall));
						IoSession tmpSession = getActiveSession(mumbleUser);
						if (tmpSession != null) {
							Logger.write(COMMUNICATION_BOT, "Found session for vehicle - 11 " + mumbleUser);
							tmpSession.write(request.getEndCall());
							Logger.write(COMMUNICATION_BOT,
									String.format("The command %s is sent to %s", request.getEndCall(), mumbleUser));
						}
						WebSocketHandler.broadcastToBrowsers(userInCall, request.getEndCall());
						Logger.write(COMMUNICATION_BOT, String.format("The command %s is sent to browser %s",
								request.getEndCall(), userInCall));
						botService.endSingleCall(userInCall, mumbleUser,customerId);
					}
				}
				if (nOnlineVehicles > 0) {
					
					try {
						String tNumber = request.getWebUser();
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = CALLING;
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(recipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);


					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
					
					String browserRequest = request.getRawdata();
					browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"").replaceAll("\"ackstatus\":\"false\"", "\"ackstatus\":\"true\"");
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					Logger.write(COMMUNICATION_BOT, String.format("starting a conference call"));
					String channelName = botService.startConferenceCall(request.getMumbleUser(), recipients,customerId);
					Logger.write(COMMUNICATION_BOT, String.format("Conference call started"));
					try {
						String tNumber = request.getWebUser();
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = ACCEPT;
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(recipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);


					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				} else {
					String commandToSend = String.format(
							"{\"command\":\"conference\",\"status\":\"reject\",\"extrainfo\":\"no recipient is online\",\"request\":%s}",
							request.getRawdata().replace("\n", "").replace("\r", ""));
					//WebSocketHandler.broadcastToAllBrowsers(commandToSend);
					Logger.write(COMMUNICATION_BOT,
							"Sending ack to browser ");
					String browserRequest = request.getRawdata();
					browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
					WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
					//session.write(browserRequest);
					Logger.write(COMMUNICATION_BOT, String.format("Written to WebSocket %s ", commandToSend));
					// session.write(commandToSend);
					try {
						String tNumber = request.getWebUser();
						String cType = request.getCommand();
						String cSource = request.getType();
						String cEvnt = REJECT;
						String fEndpoint = request.getWebUser();
						String tEndpoint = gson.toJson(recipients);
						DatabaseManager.updateCallHistory(tNumber, 
														  cType, 
														  cSource, 
														  cEvnt, 
														  fEndpoint,
														  tEndpoint);

					} catch (Exception ex) {
						Logger.writeError(COMMUNICATION_BOT, ex);
					}
				}

			} else {
				String commandToSend = String.format(
						"{\"command\":\"conference\",\"status\":\"reject\",\"extrainfo\":\"There are no recipients\",\"request\":%s}",
						request.getRawdata().replace("\n", "").replace("\r", ""));
				//WebSocketHandler.broadcastToAllBrowsers(commandToSend);
				Logger.write(COMMUNICATION_BOT,
						"Sending ack to browser ");
				String browserRequest = request.getRawdata();
				browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
				WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
				Logger.write(COMMUNICATION_BOT, String.format("Written to WebSocket %s ", commandToSend));
				try {
					String tNumber = request.getWebUser();
					String cType = request.getCommand();
					String cSource = request.getType();
					String cEvnt = REJECT;
					String fEndpoint = request.getWebUser();
					String tEndpoint = gson.toJson(recipients);
					DatabaseManager.updateCallHistory(tNumber, 
													  cType, 
													  cSource, 
													  cEvnt, 
													  fEndpoint,
													  tEndpoint);

				} catch (Exception ex) {
					Logger.writeError(COMMUNICATION_BOT, ex);
				}
				// session.write(commandToSend);
			}
		} else if (request.getStatus().equalsIgnoreCase(END)) {
			String browserRequest = request.getRawdata();
			browserRequest = browserRequest.replaceAll("\"ack\":\"false\"", "\"ack\":\"true\"");
			WebSocketHandler.broadcastToBrowsers(request.getWebUser(), browserRequest);
			if (recipients.size() > 0) {
				botService.endConferenceCall(request.getMumbleUser(), recipients,customerId);
				try {
					String tNumber = request.getWebUser();
					String cType = request.getCommand();
					String cSource = request.getType();
					String cEvnt = request.getStatus();
					String fEndpoint = request.getWebUser();
					String tEndpoint = gson.toJson(recipients);
					DatabaseManager.updateCallHistory(tNumber, 
													  cType, 
													  cSource, 
													  cEvnt, 
													  fEndpoint,
													  tEndpoint);

				} catch (Exception ex) {
					Logger.writeError(COMMUNICATION_BOT, ex);
				}
				// Optional
				/*
				 * for (int index = 0; index < recipients.size(); index++) {
				 * RequestRecipient recipient = recipients.get(0); String
				 * mumbleUser = recipient.getMumbleUser(); IoSession tmpSession
				 * = getActiveSession(mumbleUser); if (tmpSession != null) {
				 * Logger.write(COMMUNICATION_BOT,
				 * "Found session for vehicle - 12 " + mumbleUser);
				 * tmpSession.write(request.getEndCall());
				 * Logger.write(COMMUNICATION_BOT, "Command written to " +
				 * mumbleUser); } }
				 */
			}
		}

	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		// System.out.println( "IDLE " + session.getIdleCount( status ));
	}

	public IoAcceptor getParentAcceptor() {
		return parentAcceptor;
	}

	public void setParentAcceptor(IoAcceptor parentAcceptor) {
		this.parentAcceptor = parentAcceptor;
	}
}
package Bot;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.util.Log4jWebConfigurer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ibi.communicator.websocket.WebSocketHandler;
import shared.Logger;

public class ClientRequest {
	private String command;
	private String type;
	private String webuser;
	private String mumbleuser;
	private String pingmumbleuser;
	private String status;
	// Ping Attributes
	private String customerid;
	private String vehicleno;
	private String line;
	private String destination;
	private String lat;
	private String lng;
	private String ptaJourneyid;
	private String journeytype;
	private String vehicletype;
	private String ack;
	private String ackstatus;
	private String rawdata;
	private String fromendpoint; 
	private String toendpoint;
	private ClientRequest orignalRequest;
	private final String endCall = "{\"command\":\"call\", \"type\":\"server\", \"status\":\"end\"}" + System.lineSeparator();
	
	private boolean parsed;
	private List<RequestRecipient> recipients = null;
	private List<RequestOperator> operators = null;
	private PanicRequest panicRequest = null;
	private final String EMPTY = "";
	
	public String getFromEndpoint()
	{
		return !this.fromendpoint.startsWith("[") ? String.format("[%s]", this.fromendpoint): this.fromendpoint;
	}
	
	public void setFromEndpoint(String value)
	{
		this.fromendpoint = value;
	}
	
	public String getToEndpoint()
	{
		return this.toendpoint;
	}
	
	public void setToEndpoint(String value)
	{
		this.toendpoint = value;
	}
	
	public String getAck()
	{
		return this.ack;
	}
	
	public void setAck(String value)
	{
		this.ack = value;
	}
	
	public String getAckStatus()
	{
		return this.ackstatus;
	}
	
	public void setAckStatus(String value)
	{
		this.ackstatus = value;
	}
	
	public List<RequestOperator> getOperators(){
		return this.operators;
	}
	public PanicRequest getPanicRequest(){
		return this.panicRequest;
	}
	
	public String getPtaJourneyid() {
		return ptaJourneyid;
	}
	public void setPtaJourneyid(String ptaJourneyid) {
		this.ptaJourneyid = ptaJourneyid;
	}
	public String getJourneytype() {
		return journeytype;
	}
	public void setJourneytype(String journeytype) {
		this.journeytype = journeytype;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getWebUser() {
		return webuser;
	}
	public void setWebUser(String webUser) {
		this.webuser = webUser;
	}
	
	public String getPingMumbleUser() {
		return pingmumbleuser;
	}
	
	public String getMumbleUser() {
		return mumbleuser;
	}
	public void setMumbleUser(String mumbleUser) {
		this.mumbleuser = mumbleUser;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<RequestRecipient> getRecipients() {
		return recipients;
	}
	public void setRecipients(List<RequestRecipient> recipients) {
		this.recipients = recipients;
	}
	
	public boolean isParsed() {
		return parsed;
	}
	public void setParsed(boolean parsed) {
		this.parsed = parsed;
	}
	
	public ClientRequest()
	{
		this.recipients = new ArrayList<RequestRecipient>();
	}
	
	private String getProperty(JsonObject json, String propertyName)
	{
		try
		{
			if (json.get(propertyName) == null)
				return EMPTY;
			
			if (json.get(propertyName).isJsonObject()){
				return json.get(propertyName).getAsJsonObject().toString(); 
			}
			return json.get(propertyName).getAsString();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return EMPTY;
	}
	
	private JsonObject getPropertyAsObject(JsonObject json, String propertyName)
	{
		try
		{
			if (json.get(propertyName) == null)
				return null;
			
			return json.get(propertyName).getAsJsonObject();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	private void parseRecipients(JsonObject json, String propertyName)
	{
		try
		{
			if (this.recipients == null){
				this.recipients = new ArrayList<RequestRecipient>();
			}
			
			JsonArray recptElements = (JsonArray)json.get(propertyName);
			Iterator i = recptElements.iterator();
			while (i.hasNext()) {
				JsonObject innerObj = (JsonObject) i.next();
				RequestRecipient recpt = new RequestRecipient(innerObj.get("customer").getAsString(), innerObj.get("vehiclenumber").getAsString());
				this.recipients.add(recpt);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public String getMumbleCustomerId()
	{
		if (this.customerid != "" && this.customerid != null)
		{
			return this.customerid;
		}
		if (orignalRequest != null)
		{
			if (orignalRequest.customerid != "" && orignalRequest.customerid != null)
			{
				return orignalRequest.customerid;
			}
		}
		if (this.panicRequest != null){
			return this.panicRequest.getCustomerId();
		}
		return "2254";
	}
	
	public ClientRequest(String json)
	{
		try
		{
			JsonParser jsonParser = new JsonParser();
			JsonObject jsonRequest = (JsonObject)jsonParser.parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)))));
			this.command = getProperty(jsonRequest,"command");
			this.ack = getProperty(jsonRequest,"ack");
			this.ackstatus = getProperty(jsonRequest,"ackstatus");
			this.webuser = getProperty(jsonRequest,"webuser");
			this.customerid = getProperty(jsonRequest,"customer");
			//Logger.write("communication_bot", "command is " + this.command);
			if (this.command.equalsIgnoreCase("call") || this.command.equalsIgnoreCase("conference")){
				this.status = getProperty(jsonRequest,"status");
				this.type = getProperty(jsonRequest,"type");
				JsonObject acceptedFrom = (JsonObject) jsonRequest.get("acceptedfrom");
				if (acceptedFrom != null){
					this.webuser = acceptedFrom.get("webuser").getAsString();
					this.mumbleuser = acceptedFrom.get("mumbleuser").getAsString();
				}
				if (this.type != null && this.type.equalsIgnoreCase("bus")){
					Logger.write("communication_bot", "call  is from bus");
					
					JsonObject fromElement = (JsonObject) jsonRequest.get("from");
					
					if (fromElement != null){
						this.setFromEndpoint(getProperty(jsonRequest,"from"));
						this.customerid = fromElement.get("customer").getAsString();
						this.vehicleno = fromElement.get("vehiclenumber").getAsString();
					}
					
					Logger.write("communication_bot", "parse operators");
					parseOperators(jsonRequest, "to");	
					Logger.write("communication_bot", "operators parsed");
				}else{
					if (this.status.isEmpty() || (this.type != null && this.type.equalsIgnoreCase("operator") && (this.status.equalsIgnoreCase("end") || this.status.equalsIgnoreCase("reject")))){
						
						this.webuser = getProperty(jsonRequest,"webuser");
						this.mumbleuser = getProperty(jsonRequest,"mumbleuser");
						
						parseRecipients(jsonRequest, "to");	
						Logger.write("communication_bot", "vehicles parsed");
					}
					else{
						this.orignalRequest = new ClientRequest(getProperty(jsonRequest,"request"));
					}
				}
			}else if (this.command.equalsIgnoreCase("ring")){
				JsonObject request = (JsonObject) jsonRequest.get("request");
				this.status = getProperty(request,"status");
				this.type = getProperty(request,"type");
				if (this.type != null && this.type.equalsIgnoreCase("operator")){
					this.webuser = getProperty(request,"webuser");
					this.mumbleuser = getProperty(request,"mumbleuser");
					
					parseRecipients(request, "to");	
				}
			}
			else if (this.command.equalsIgnoreCase("ping")){
				this.customerid = getProperty(jsonRequest,"customer");
				this.vehicleno = getProperty(jsonRequest,"vehiclenumber");
				this.pingmumbleuser = String.format("%s_%s", this.customerid, this.vehicleno);
				this.line = getProperty(jsonRequest,"line");
				this.lat = getProperty(jsonRequest,"lat");
				this.lng = getProperty(jsonRequest,"long");
				this.vehicletype = getProperty(jsonRequest,"type");
				this.destination = getProperty(jsonRequest,"destination");
				this.ptaJourneyid = getProperty(jsonRequest,"ptajourneyid");
				this.journeytype = getProperty(jsonRequest,"journeytype");
			}
			else if (this.command.equalsIgnoreCase("hangup")){
				this.type = getProperty(jsonRequest,"type");
				this.webuser = getProperty(jsonRequest,"webuser");
				this.mumbleuser = getProperty(jsonRequest,"mumbleuser");
				
				parseRecipients(jsonRequest, "to");	
			}
			else if (this.command.equalsIgnoreCase("heartbeat")){
				this.webuser = getProperty(jsonRequest,"webuser");
				this.mumbleuser = getProperty(jsonRequest,"mumbleuser");
			}
			else if (this.command.equalsIgnoreCase("panic")){
					this.type = getProperty(jsonRequest,"type");
					Logger.write("communication_bot", "type " + this.type);
					this.status = getProperty(jsonRequest,"status");
					Logger.write("communication_bot", "status " + this.status);
					if (this.type != null){
						if(this.type.equalsIgnoreCase("bus")){
							Logger.write("communication_bot", "panic call from bus ");
							parsePanicCall(jsonRequest, "from");
							// forward that to WebSocket
						}else if(this.type.equalsIgnoreCase("operator")){
							this.webuser = getProperty(jsonRequest,"webuser");
							this.mumbleuser = getProperty(jsonRequest,"mumbleuser");
							this.orignalRequest = new ClientRequest(getProperty(jsonRequest,"request"));
						}
					}
						
				}
			this.rawdata = json;	
			this.parsed = true;
		}
		catch(Exception ex)
		{
			Logger.writeError("communication_bot", ex);
			this.parsed = false;
		}
		
	}
	private void parseOperators(JsonObject jsonRequest, String propertyName) {
		try
		{
			if (this.operators == null){
				this.operators = new ArrayList<RequestOperator>();
			}
			
			JsonArray recptElements = (JsonArray)jsonRequest.get(propertyName);
			Iterator i = recptElements.iterator();
			while (i.hasNext()) {
				JsonObject innerObj = (JsonObject) i.next();
				RequestOperator operator = new RequestOperator();
				operator.setId(innerObj.get("id").getAsString());
				operator.setName(innerObj.get("name").getAsString());
				if (innerObj.get("mumbleusername") != null){
					operator.setMumbleusername(innerObj.get("mumbleusername").getAsString());
				}
				this.operators.add(operator);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}

	private void parsePanicCall(JsonObject jsonRequest, String propertyName) {
		try
		{
			if (this.panicRequest == null){
				this.panicRequest = new PanicRequest();
			}
			
			JsonObject panicElement = (JsonObject) jsonRequest.get(propertyName);
			this.panicRequest.setCustomerId(panicElement.get("customer").getAsString());
			this.panicRequest.setVehicleNumber(panicElement.get("vehiclenumber").getAsString());
			this.panicRequest.setTimestamp(panicElement.get("time").getAsString());
			this.panicRequest.setLat(panicElement.get("lat").getAsString());
			this.panicRequest.setLng(panicElement.get("long").getAsString());
			Logger.write("communication_bot", "Panic call processed");
		}
		catch(Exception ex)
		{
			Logger.writeError("communication_bot", ex);
			ex.printStackTrace();
		}
		
	}
	public String getCustomerId() {
		return customerid;
	}
	public void setCustomerId(String customerid) {
		this.customerid = customerid;
	}
	public String getVehicleNo() {
		return vehicleno;
	}
	public void setVehicleNo(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getVehicleType() {
		return vehicletype;
	}
	public void setVehicleType(String vehicletype) {
		this.vehicletype = vehicletype;
	}
	public String getRawdata() {
		return rawdata + System.lineSeparator();
	}
	public void setRawdata(String rawdata) {
		this.rawdata = rawdata;
	}
	public ClientRequest getOrignalRequest() {
		return orignalRequest;
	}
	public void setOrignalRequest(ClientRequest orignalRequest) {
		this.orignalRequest = orignalRequest;
	}
	public String getEndCall() {
		return endCall;
	}

	
}

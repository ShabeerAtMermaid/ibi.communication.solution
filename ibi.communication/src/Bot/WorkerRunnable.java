package Bot;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;

import ibi.communicator.websocket.WebSocketHandler;
public class WorkerRunnable{
	
}
/*
public class WorkerRunnable implements Runnable{

    protected Socket clientSocket = null;
    protected String serverText   = null;
    protected BotService botService = new BotService();

    public WorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
    }

    public void run() {
    	OutputStream output = null;
    	BufferedReader input = null;
        try {
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = clientSocket.getOutputStream();
            while(true){
            	System.out.println("Reading from client");
                String clientData = input.readLine();
                String response = "";
                if (clientData != null){
	                System.out.println("The data received " + clientData);
	                

	                if (clientData.startsWith("panic:")){
	                	response = processPanicCall(clientData.substring(clientData.indexOf("panic:") + 6));
	                }else
	                if (clientData.startsWith("hangup:")){
	                	response = processHangupCall(clientData.substring(clientData.indexOf("hangup:") + 7));
	                }else
	                //if (clientData.startsWith("ping:"))
	                	{
	                	response = processPingCall(clientData);
	                }
	                output.write(("OK" + response + "\n").getBytes());
	                output.flush();
	                System.out.println("Request processed: ");
                }else{
                	if (input != null){
                		input.close();
                	}
                	if (output !=null){
                		output.close();
                	}
                	break;
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
            
            try {
				output = null;
	            input = null;
	            clientSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

        }
    }

	private synchronized String processPingCall(String message) 
	{
		WebSocketHandler.broadcastToBrowsers("",message);
		return "acknowledged";
	}

	private synchronized String processHangupCall(String clientData) {
		
		String result = "";
		try{
			System.out.println("processHangupCall called with " + clientData);
			String userName = clientData.trim();
			String[] userList = botService.getRelevantUsers(userName);
			System.out.println("Ending call");
			botService.endCall(userList);
			result = "Call ended";
			System.out.println("Call started");
			WebSocketHandler.broadcastToBrowsers("","Call Ended");
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			result = ex.getMessage();
		}
		return result;
	}

	private synchronized String processPanicCall(String clientData) {
		String result = "";
		try{
			System.out.println("processPanicCall called with " + clientData);
			String userName = clientData.trim();
			String[] userList = botService.getRelevantUsers(userName);
			if (userList.length == 1){
				result = "No one available for call";
			}
			System.out.println("Starting call");
			botService.startCall(userList);
			result = "Call initiated";
			System.out.println("Call started");
			WebSocketHandler.broadcastToBrowsers("","Call started");
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			result = ex.getMessage();
		}
		return result;
	}
}
*/
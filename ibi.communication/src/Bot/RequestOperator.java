package Bot;

public class RequestOperator {
	private String id;
	private String name;
	private String mumbleusername;
	
	public RequestOperator(){
		
	}
	
	public RequestOperator(String id, String name, String mumbleusername){
		this.id = id;
		this.name = name;
		this.mumbleusername = mumbleusername;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMumbleusername() {
		return mumbleusername;
	}
	public void setMumbleusername(String mumbleusername) {
		this.mumbleusername = mumbleusername;
	}
}

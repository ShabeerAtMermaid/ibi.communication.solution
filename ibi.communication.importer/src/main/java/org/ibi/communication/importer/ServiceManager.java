package org.ibi.communication.importer;

import shared.AppSettings;
import shared.FTPManager;
import shared.Logger;

public class ServiceManager 
{
	private static String version = "Production - IBI.Communication.importer.Java_8.0.1"; // "Development
	private static String buildDate = "2017-08-28";
	
	private static Thread importerThread;
	
	public static void StartServices() 
	{
		Logger.write("communication_PTAJourneyImporter", "============================================");
		Logger.write("communication_PTAJourneyImporter", "Current Version: " + version);
		Logger.write("communication_PTAJourneyImporter", "Build Date: " + buildDate);
		Logger.write("communication_PTAJourneyImporter", "============================================");

		Logger.write("communication_PTAJourneyImporter", "-----------------------------------------------");
		Logger.write("communication_PTAJourneyImporter", "Starting importer service");
		startImporterThread();
		Logger.write("communication_PTAJourneyImporter", "Importer service started");
	}
	private static void startImporterThread() 
	{
	   importerThread = new Thread()
 	   {
 	   	  public void run()
 	   	  {
 	   		  while(true)
 	   		  {
 	   			Logger.write("communication_PTAJourneyImporter", "***********************************************");
 	   			Logger.write("communication_PTAJourneyImporter", "Starting import and update operator plan data");
 	   			FTPManager.downlaodFTPFile();
 	   			Logger.write("communication_PTAJourneyImporter", "***********************************************");
 		   		 try 
 		   		 {
 			         Thread.sleep(1 * 1000 * 60 * AppSettings.getFetchinterval_minutes());
 			     }catch (InterruptedException e) {}
 	   		  }
 	   	  }
 	  };
 	  importerThread.start();
	} 
	
	
}

package model;

import java.util.Date;

public class PTAJourney 
{
	private int ptaJourneyNumber;
	private int operatorDriverId;
	private String line;
	private String busNumber;
	private Date journeyDate;
	private Date plannedStart;
	private Date plannedEnd;
	
	public PTAJourney(  int ptaJourneyNumber,
						int operatorDriverId,
						String line,
						String busNumber,
						Date journeyDate,
						Date plannedStart,
						Date plannedEnd)
	{
		this.ptaJourneyNumber = ptaJourneyNumber;
		this.operatorDriverId = operatorDriverId;
		this.line = line;
		this.busNumber = busNumber;
		this.journeyDate = journeyDate;
		this.plannedStart = plannedStart;
		this.plannedEnd = plannedEnd;
	}
	
	public int getPTAJourneyNumber()
	{
		return this.ptaJourneyNumber;
	}
	public void setPTAJourneyNumber(int value)
	{
		this.ptaJourneyNumber = value;
	}
	
	public int getOperatorDriverId()
	{
		return this.operatorDriverId;
	}
	public void setOperatorDriverId(int value)
	{
		this.operatorDriverId = value;
	}
	
	public String getLine()
	{
		return this.line;
	}
	public void setLine(String value)
	{
		this.line = value;
	}
	
	public String getBusNumber()
	{
		return this.busNumber;
	}
	public void setBusNumber(String value)
	{
		this.busNumber = value;
	}
	
	public Date getJourneyDate()
	{
		return this.journeyDate;
	}
	public void setJourneyDate(Date value)
	{
		this.journeyDate = value;
	}
	
	public Date getplannedStart()
	{
		return this.plannedStart;
	}
	public void setplannedStart(Date value)
	{
		this.plannedStart = value;
	}
	
	public Date getplannedEnd()
	{
		return this.plannedEnd;
	}
	public void setplannedEnd(Date value)
	{
		this.plannedEnd = value;
	}

}

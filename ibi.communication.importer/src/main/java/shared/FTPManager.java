package shared;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Date;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import model.PTAJourney;

public class FTPManager 
{
	private static FTPClient ftpClient = new FTPClient();
	private static long  ftpFileSize = 0;
	private static Date ftpFileTimeStamp;
	
	public static void downlaodFTPFile()
	{
        try
        { 
        	Logger.write("communication_PTAJourneyImporter", String.format("connecting to FTP Server %s - %s", AppSettings.getFTPHost(),AppSettings.getFTPPort()) );
        	ftpClient.connect(AppSettings.getFTPHost(), AppSettings.getFTPPort());
        	Logger.write("communication_PTAJourneyImporter", "connection established" );
        	int reply = ftpClient.getReplyCode();
    	    if (!FTPReply.isPositiveCompletion(reply)) 
    	    {
    	    	Logger.write("communication_PTAJourneyImporter", "FTP server refused connection.");
    	    } 
        	Logger.write("communication_PTAJourneyImporter", String.format("Trying to login %s - %s", AppSettings.getFTPUsername(),AppSettings.getFTPUsername()));
        	ftpClient.login(AppSettings.getFTPUsername(), AppSettings.getFTPPassword());
        	ftpClient.enterLocalActiveMode();
        	Logger.write("communication_PTAJourneyImporter", "has been logged-in" );        	
        	
        	 String remoteFilePath =AppSettings.getAROSFilePath();
        	 Logger.write("communication_PTAJourneyImporter", "Fetching file details" );
        	 FTPFile[] files = ftpClient.listFiles();
        	 Logger.write("communication_PTAJourneyImporter", "File details fetched" );
             for (FTPFile file : files) 
             {
	             if (file.isFile()) 
	             {
	            	 Date currentFileTimeStamp  = file.getTimestamp().getTime();
	            	 long currentFileSize = file.getSize();
	                  
	            	 if (remoteFilePath.equalsIgnoreCase(file.getName()) && (ftpFileSize != currentFileSize || ftpFileTimeStamp == null || currentFileTimeStamp.after(ftpFileTimeStamp)))
	            	 {
	            		 Logger.write("communication_PTAJourneyImporter", "File has changed!!!!");
	            		 ftpFileSize = currentFileSize;
	            		 ftpFileTimeStamp = currentFileTimeStamp;
	            		 
	            		 /*
		                  System.out.print("Remote file  "+ remoteFilePath +" - current File name :" + file.getName());
		                  System.out.print("previous size : " + ftpFileSize + " Current size : " + currentFileSize);
		                  System.out.print("Previous Datetime : " + ftpFileTimeStamp.toString() + " Current Datetime : " + currentFileTimeStamp.toString());
		                 */ 
	            		  Logger.write("communication_PTAJourneyImporter", "Downloading file" );
			              InputStream in = ftpClient.retrieveFileStream(remoteFilePath);
			              Logger.write("communication_PTAJourneyImporter", "File has been download" );
			              BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			          	  
			              String line;			          		
			          	  int ptajourneyNumber;
			          	  int operatorDriverId;
			          	  String lineNumber;
			          	  String busNumber;
			          	  Date journeyDate;
			          	  Date journeyPlannedStart;
			          	  Date journeyPlannedEnd;
			          	  
			          	  while((line = br.readLine()) != null)
			          	  {			
			          		  //Logger.write("communication_PTAJourneyImporter", line);
			          		  String dataParts[] = line.split(";");
	          			
			          		  operatorDriverId = Integer.parseInt(dataParts[0]);
			          		  ptajourneyNumber = Integer.parseInt(dataParts[1]);
			          		  lineNumber = dataParts[2].replaceFirst("^0+(?!$)", "");
			          		  busNumber = dataParts[3];
			          		  journeyDate = ApplicationUtility.getDateFromString(dataParts[4]);
			          		  journeyPlannedStart =  ApplicationUtility.getDateTimeFromString(dataParts[5]);
			          		  journeyPlannedEnd =  ApplicationUtility.getDateTimeFromString(dataParts[6]);
		          			
			          		  /*
			          		  System.out.println(String.format("Operator driver id = %s", operatorDriverId));
			          		  System.out.println(String.format("PTA journey number = %s", ptajourneyNumber));
			          		  System.out.println(String.format("Line               = %s", lineNumber));
			          		  System.out.println(String.format("Bus number         = %s", busNumber));
			          		  System.out.println(String.format("Journey start date = %s", journeyStartDate));
			          		  */
			          		  
			          		  PTAJourney ptaJourney = new PTAJourney(ptajourneyNumber,
		          												   operatorDriverId,
		          												   lineNumber,
		          												   busNumber,
		          												   journeyDate,
		          												   journeyPlannedStart,
		          												   journeyPlannedEnd);
			          		  DatabaseManager.updatePTAJourney(ptaJourney);
			          		  
			          		  if (ptaJourney != null) ptaJourney = null;
			          	  }
	            	 }
	            	 else
	            	 {
	            		 Logger.write("communication_PTAJourneyImporter", "File has not changed!!!!");
	            	 }
	             }
             }
         } catch (SocketException e) {
	    	System.out.println(e.getMessage());
	    } catch (IOException e) {
	    	System.out.println(e.getMessage());
	    } catch (NumberFormatException e) {
	    	System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} 
        finally 
        {
			try 
			{
                if (ftpClient.isConnected()) 
                {
                	ftpClient.logout();
                	ftpClient.disconnect();
                	Logger.write("communication_PTAJourneyImporter", "Successfully disconnected from the host");
                }
            } 
			catch (IOException ex) 
			{
				Logger.write("communication_PTAJourneyImporter", "Cannot disconnect from the host");
            }		      
        }
	}
	
	public static void download()
	{
	        String server = AppSettings.getFTPHost();
	        int port = AppSettings.getFTPPort();
	        String user = AppSettings.getFTPUsername();
	        String pass =  AppSettings.getFTPPassword();
	 
	        try {
	 
	            ftpClient.connect(server, port);
	            ftpClient.login(user, pass);
	            ftpClient.enterLocalActiveMode();
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	 
	            // APPROACH #1: using retrieveFile(String, OutputStream)
	            String remoteFile1 = AppSettings.getAROSFilePath();
	            File downloadFile1 = new File(AppSettings.getLocalAROSFilePath());
	            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
	            boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
	            outputStream1.close();
	 
	            if (success) {
	                System.out.println("File #1 has been downloaded successfully.");
	            }
	 
	        } catch (IOException ex) {
	            System.out.println("Error: " + ex.getMessage());
	            ex.printStackTrace();
	        } finally {
	            try {
	                if (ftpClient.isConnected()) {
	                    ftpClient.logout();
	                    ftpClient.disconnect();
	                }
	            } catch (IOException ex) {
	                ex.printStackTrace();
	            }
	        }
	    
	}
}

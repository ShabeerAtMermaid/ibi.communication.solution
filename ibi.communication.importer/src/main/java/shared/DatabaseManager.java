package shared;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Date;

import model.PTAJourney;


public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";	   
	   
	   static final String DB_URL = AppSettings.getDatabaseName();
	   //  Database credentials
	   static final String USER = AppSettings.getDatabaseUser();
	   static final String PASS = AppSettings.getDatabaseUserPassword();
	   
	   private static Connection conn = null;
	   private static CallableStatement stmt = null;
	   private static ResultSet rs = null;
	   
	   private static Connection getDBConnection()
	   {
		   Connection conn = null;
		   try
		   {
		      //STEP 1: Register JDBC driver
		      Class.forName(JDBC_DRIVER);
		      //Logger.write("communication_rest", "Step -01 Register JDBC driver and DB_URL: " + DB_URL + " USER: " + USER + " PASS: " + PASS);
		      
		      //STEP 2: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      //Logger.write("communication_rest", "Step -02 Open a connection");
		      return conn;
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_PTAJourneyImporter", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_PTAJourneyImporter",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_PTAJourneyImporter", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_PTAJourneyImporter",  e);
		   }
		   return null;
	   }

	public static void updatePTAJourney(PTAJourney journeyInfo) 
	{
		 int ptaJourneyNumber = journeyInfo.getPTAJourneyNumber();
		 int operatorDriverId = journeyInfo.getOperatorDriverId();
		 String line = journeyInfo.getLine();
		 String busNumber = journeyInfo.getBusNumber();
		 Date  journeyDate = new Date(journeyInfo.getJourneyDate().getTime());
		 Timestamp   plannedStart = new Timestamp (journeyInfo.getplannedStart().getTime());;
		 Timestamp   plannedEnd = new Timestamp (journeyInfo.getplannedEnd().getTime());;
		 try
		 {
			  conn = getDBConnection();
			  //STEP 3: Execute a query
			  stmt = conn.prepareCall("{call updatePTAjourney(?,?,?,?,?,?,?)}");
			  stmt.setInt(1, ptaJourneyNumber);
			  stmt.setInt(2, operatorDriverId);
			  stmt.setString(3, line);
			  stmt.setString(4, busNumber);
			  stmt.setDate(5, journeyDate);
			  stmt.setTimestamp (6, plannedStart);
			  stmt.setTimestamp (7, plannedEnd);
			
			  rs = stmt.executeQuery();
			  while(rs.next())
		      {
				  if ("1".equalsIgnoreCase(rs.getString("ret")))
				  {
					  String journeyInfoLog = String.format("ptaJourneyNumber %s - operatorDriverId %s - line %s - busNumber %s - journeyStart %s - plannedStart %s - plannedEnd %s",ptaJourneyNumber,operatorDriverId,line,busNumber,journeyDate,plannedStart,plannedEnd);
					  Logger.write("communication_PTAJourneyImporter", journeyInfoLog);
				  }
		      }
			  //STEP 6: Clean-up environment
		 }
		 catch(SQLException se)
		 {
			 Logger.write("communication_PTAJourneyImporter", "Exception DB: " + se.getMessage());
			 Logger.writeError("communication_PTAJourneyImporter",  se);
		 }
		 catch(Exception e)
		 {
			 //return "NOT AUTH";
			 Logger.write("communication_PTAJourneyImporter", "Exception DB: " + e.getMessage());
			 Logger.writeError("communication_PTAJourneyImporter",  e);
		 }
		 finally
		 {
			 cleanup();
		 }//end try
	}
	private static void cleanup()
	{
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
	    	  Logger.writeError("communication_rest",  se);
	      }//end finally try
	}
}  

package shared;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ApplicationUtility
{
	public static boolean isNullOrEmpty(String text)
	{
		if(text == null || text.equals("") || text.equals("null"))
			return true;
		else
			return false;
	}
	public static Date getDateFromString(String dateString) throws Exception
	{
		dateString = dateString.replace("T", " ");
		dateString = dateString.replace("/", "-");
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date tempDate = (Date)format.parse(dateString);
		return tempDate;
	}
	public static Date getDateTimeFromString(String dateString) throws Exception
	{
		dateString = dateString.replace("T", " ");
		dateString = dateString.replace("/", "-");
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date tempDate = (Date)format.parse(dateString);
		return tempDate;
	}
}

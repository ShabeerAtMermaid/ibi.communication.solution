package shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.ibi.communication.importer.Main;

public class AppSettings 
{
	public static String getAROSFilePath()
	{
		//return "arosdata.csv";
		String value = getConfigValue("ftpArosDatafile");
		return (value == null || value.equals("")) ? "arosdata.csv" : value;
	}
	
	public static String getLocalAROSFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), getAROSFilePath());
		return filePath.toString();
	}
	
	public static String getDatabaseName()
	{
		//Live and DEV
	    //return "jdbc:mysql://localhost/communication";

		//stagging
		//return "jdbc:mysql://localhost/communicationstaging";

	   //stagging2
	   // return "jdbc:mysql://localhost/communicationDEV"; //return "jdbc:mysql://localhost/communicationstaging2";
		String value = getConfigValue("dbname");
		return (value == null || value.equals("")) ? "jdbc:mysql://localhost/communication" : value;
	}
	
	public static String getDatabaseUser()
	{
	   //Live and DEV
	   //return "communication";
	   
	   //stagging
	   //return "staging";
	   
	   //stagging2
	   //return "staging";
		String value = getConfigValue("dbusername");
		return (value == null || value.equals("")) ? "communication" : value;
	}
	
	public static String getDatabaseUserPassword()
	{
	    //Live and DEV
		//return "ibi0786";
	   
	    //stagging
		//return "staging123";
	
	   //stagging2
	   //return "staging123";
		
		String value = getConfigValue("dbpassword");
		return (value == null || value.equals("")) ? "ibi0786" : value;
	}
	
	public static int getFTPPort()
	{
		String value = getConfigValue("ftpport");
		return (value == null || value.equals("")) ? 21 : Integer.parseInt(value);	
	}
	
	public static int getFetchinterval_minutes()
	{
		String value = getConfigValue("fetchinterval_minutes");
		return (value == null || value.equals("")) ? 15 : Integer.parseInt(value);	
	}
	
	public static String getFTPHost()
	{
		String value = getConfigValue("ftphost");
		return (value == null || value.equals("")) ? "172.25.176.2" : value;
	}
	
	public static String getFTPUsername()
	{
		String value = getConfigValue("ftpusername");
		return (value == null || value.equals("")) ? "arrivaaros" : value;
	}
	
	public static String getFTPPassword()
	{
		String value = getConfigValue("ftppassword");
		return (value == null || value.equals("")) ? "HNWh-&D7kRv=" : value;
	}
	
	private static String getConfigFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), "config.properties");
		return filePath.toString();
	}
	
	private static String getConfigValue(String settingName)
	{
		InputStream inputStream = null;
    	try 
    	{
    		Properties prop = new Properties();
    		//String propFileName = "config.properties";
    		//inputStream = Main.class.getClassLoader().getResourceAsStream(propFileName);
    		
    		String externalFileName = getConfigFilePath();
    		inputStream = new FileInputStream(new File(externalFileName));
    		prop.load(inputStream);
    		return prop.getProperty(settingName);
    	} 
    	catch (Exception e) 
    	{
    		return "";
    	} 
    	finally 
    	{
    		try {
    			if (inputStream != null)
    				inputStream.close();	
    		} catch (IOException e) {}
    	}
	}
}

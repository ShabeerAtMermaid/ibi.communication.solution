package com.ibi.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import shared.Logger;


@Path("/")
public class RestService 
{
	/*
	@POST
	@Path("/events")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
	public Response postpushEvents(String event) 
	{
	    try
		{
			Logger.write("communication_event_rest", "@POST: events API called");
		    RestManager.creatEvent(event);
		    return Response.ok(1).build();
		}
		catch(Exception ex)
		{
			return Response.ok(0).build();
		}
		
	}
	*/
	
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getTesting()
	{
		Logger.write("communication_event_rest", "@GET: test API called");
		return "OK";
	}
	
	
	@GET
	@Path("/events")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getTesting(@QueryParam("Data") String event)
	{
		try
		{
			Logger.write("communication_event_rest", "@Get: events API called");
		    RestManager.creatEvent(event);
		    return "1";
		}
		catch(Exception ex)
		{
			return "0";
		}
	}
	
}

package shared;

import java.sql.*;

import javax.security.auth.login.Configuration;

import shared.ApplicationUtility;
import shared.Logger;

public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	   
	   //static final String DB_URL = "jdbc:mysql://localhost/communication";
	   /*
	   static final String DB_URL = AppSettings.getDatabaseName();  //"jdbc:mysql://localhost/communicationstaging";
	   //  Database credentials
	   static final String USER = AppSettings.getDatabaseUser();//"communication";
	   static final String PASS = AppSettings.getDatabaseUserPassword();//"ibi0786";
	   */
	   
	  /*
	   //stagging
	   static final String DB_URL = "jdbc:mysql://localhost/communicationstaging";
	   //  Database credentials
	   static final String USER = "staging";
	   static final String PASS = "staging123";
	  */
	   
	  
	   //Live
	   static final String DB_URL = "jdbc:mysql://localhost/communication";
	   //  Database credentials
	   static final String USER = "communication";
	   static final String PASS = "ibi0786";
	   
	   
	   private static Connection conn = null;
	   private static CallableStatement stmt = null;
	   private static ResultSet rs = null;
	   
	   private static Connection getDBConnection()
	   {
		   Connection conn = null;
		   try
		   {
		      //STEP 1: Register JDBC driver
		      Class.forName(JDBC_DRIVER);
		      Logger.write("communication_event_rest", "Step -01 Register JDBC driver and DB_URL: " + DB_URL + " USER: " + USER + " PASS: " + PASS);
		      
		      //STEP 2: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      Logger.write("communication_event_rest", "Step -02 Open a connection");
	
		      return conn;
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_event_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_event_rest",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_event_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_event_rest",  e);
		   }
		   return null;
	   }
   
	   
	   
	   public static void createPushEvent(String event) throws Exception
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_event_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call pushevents(?)}");
		      stmt.setString(1, event);
		      stmt.executeQuery();
		      Logger.write("communication_event_rest", "Qeury executed");
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_event_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_event_rest",  se);
			   throw se;
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_event_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_event_rest",  e);
			   throw e;
		   }
		   finally
		   {
			   cleanup();
		   }//end try
	   }
	   
	   
	  
	   private static void cleanup()
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_event_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_event_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
	    	  Logger.writeError("communication_event_rest",  se);
	      }//end finally try
	   }
}  

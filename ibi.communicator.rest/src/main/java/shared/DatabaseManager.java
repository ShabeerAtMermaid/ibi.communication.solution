package shared;

import java.sql.*;

import javax.security.auth.login.Configuration;

import models.CallHistoryInputModel;
import models.Credentials;
import models.UserInfo;
import models.Vehicle;
import shared.ApplicationUtility;
import shared.Logger;

public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	   static final String DB_URL = AppSettings.getDatabaseName();
	   //  Database credentials
	   static final String USER = AppSettings.getDatabaseUser();
	   static final String PASS = AppSettings.getDatabaseUserPassword();
	   
	   //private static Connection conn = null;
	   //private static CallableStatement stmt = null;
	   //private static ResultSet rs = null;
	   
	   @SuppressWarnings("unused")
	   private static Connection getDBConnection()
	   {
		   Connection conn = null;
		   try
		   {
		      //STEP 1: Register JDBC driver
		      Class.forName(JDBC_DRIVER);
		      //STEP 2: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      //Logger.write("communication_rest", "Step -02 Open a connection");
		      Logger.write("communication_rest", String.format("Connection open : DB_URL: %s ,USER: %s, PASS: %s",DB_URL,USER,PASS));
		      return conn;
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getDBConnection >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL: %s ,USER: %s, PASS: %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_rest", "getDBConnection >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL: %s ,USER: %s, PASS: %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   return null;
	   }

	   public static String getPTAReportData()
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call operatorlist()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call operatorlist()} executed"));
		      //STEP 4: Extract data from result set
		      String operators = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
								   +	 	"\"id\":\""+ rs.getString("operatorid") +"\","	
								   +	  	"\"name\":\""+ rs.getString("operatorname") +"\","	
								   +		"\"mumbleusername\":\""+ rs.getString("mumbleusername") +"\""
								   +	"}";
		  		operators += jsonString;
		  		if (!rs.isLast()) operators += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(operators))
		    	  operators = "[" + operators + "]";
		      Logger.write("communication_rest", String.format("Return %s",operators));
		      return operators;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getOperatorsList >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getOperatorsList >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "";
	   }	   
	   
	   public static String authenticate(String userName, String password,String mumbleUsername)
	   {
		   try
		   {
			   Connection conn = null;
			   ResultSet rs = null;
			   CallableStatement stmt = null;
			   try
			   {
			      conn = getDBConnection();
			      //STEP 3: Execute a query
			      stmt = conn.prepareCall("{call authenticate(?,?,?)}");
			      stmt.setString(1, userName);
			      stmt.setString(2, password);
			      stmt.setString(3, mumbleUsername);
			      rs = stmt.executeQuery();
			      Logger.write("communication_rest", String.format("Qeury {call authenticate(%s,%s,%s)} executed",userName,password,mumbleUsername));
			      //STEP 4: Extract data from result set
			      String authResponse = "";
			      String token = ApplicationUtility.issueToken();
			      while(rs.next())
			      {
			    	  String customerId = rs.getString("id");
			    	  customerId = ApplicationUtility.isNullOrEmpty(customerId) ? "":customerId;
			    	  String customerName = rs.getString("name");
			    	  customerName = ApplicationUtility.isNullOrEmpty(customerName) ? "":customerName;
			    	  
			    	  authResponse = "{"+
					    			  	"\"token\": \""+ token +"\"," +
					    			  	"\"username\": \""+ rs.getString("fullname") +"\"," +
					    			  	"\"customers\":[" + "{\"id\":\""+ customerId +"\",\"name\":\""+ customerName +"\"}" + "]" +
					                  "}";
			      }
			      if (ApplicationUtility.isNullOrEmpty(authResponse))
			    	  authResponse = "{\"token\":\"\",\"username\":\"\",\"customers\":[]}";
			      
			      Logger.write("communication_rest", String.format("authResponse %s",authResponse));			      
			      
			      return authResponse;
			      //STEP 6: Clean-up environment
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_rest", "authenticate >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
				   Logger.writeError("communication_rest",  se);
			   }
			   catch(Exception e)
			   {
				   //return "NOT AUTH";
				   Logger.write("communication_rest", "authenticate >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
				   Logger.writeError("communication_rest",  e);
			   }
			   finally
			   {
				   cleanup(conn,stmt,rs);
			   }//end try
			   return "";
		   }
		   catch(Exception se)
		   {
	    	  Logger.writeError("communication_rest",  se);
		   }//end finally try
		   return "{}";
	   }
	   
	   public static String getUserCustomers(String userName)
	   {
		   try
		   {
			   return "{\"id\":\"0000\",\"name\":\"0000\"}";
			   /*
			   try
			   {
			      conn = getDBConnection();
			      Logger.write("communication_rest", "Step -02 Open a connection");
		
			      //STEP 3: Execute a query
			      stmt = conn.prepareCall("{call getusercustomers(?)}");
			      stmt.setString(1, userName);
			      rs = stmt.executeQuery();
			      Logger.write("communication_rest", "Qeury executed");
			      //STEP 4: Extract data from result set
			      String customersJson = "";
			      while(rs.next())
			      {
			    	  customersJson += "{\"id\":\""+ rs.getString("id") +"\",\"name\":\""+ rs.getString("name") +"\"}";
			    	  if (!rs.isLast()) customersJson += ",";
			      }
			      //if (!ApplicationUtility.isNullOrEmpty(customersJson))
			      //	customersJson = "{\"customers\":[" + customersJson + "]}";
			      return customersJson;
			      //STEP 6: Clean-up environment
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
				   Logger.writeError("communication_rest",  se);
			   }
			   catch(Exception e)
			   {
				   //return "NOT AUTH";
				   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
				   Logger.writeError("communication_rest",  e);
			   }
			   finally
			   {
				   cleanup();
			   }//end try
			   return "";
			   */
		   }
		   catch(Exception se)
		   {
	    	  Logger.writeError("communication_rest",  se);
		   }//end finally try
		   return "{}";
	   }
	   
	   public static String getConfigs()
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
		      conn = getDBConnection();
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call getconfigs()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call getconfigs()} executed"));
		      //STEP 4: Extract data from result set
		      String jsonString = "";
		      while(rs.next())
		      {
		  		jsonString  +=	"\""+ rs.getString("key") +"\":\""+ rs.getString("value") + "\"" ;
		  		if (!rs.isLast()) jsonString += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(jsonString))
		    	  jsonString = "{" + jsonString + "}";
		      Logger.write("communication_rest", String.format("Return : %s",jsonString));
		      return jsonString;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getConfigs >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getConfigs >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "{}";
	   }
	   
	   public static String getBusList(int customerId,int groupId)
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      
		      stmt = conn.prepareCall("{call vehicleslistbygroupid(?,?,?)}");
		      stmt.setInt(1, customerId);
		      stmt.setInt(2, groupId);
		      stmt.setInt(3, AppSettings.getAgeToOffline_Minutes()); // Need to pass configuration of offlineage
		      
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call vehicleslistbygroupid(%s,%s,%s)} executed",customerId,groupId,AppSettings.getAgeToOffline_Minutes()));
		      //STEP 4: Extract data from result set
		      String buses = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"vehiclenumber\":\""+ rs.getString("vehiclenumber") +"\","	
						  +	  		"\"customerid\":\""+ rs.getString("customerid") +"\","	
						  +			"\"groups\" : \""+ rs.getString("vehiclescount") +"\"," 
						  +			"\"status\":\""+ rs.getString("status") +"\","
						  +			"\"line\":\""+ rs.getString("line") +"\","
						  +			"\"destination\":\""+ rs.getString("destination") +"\","
						  +			"\"ptajourneyid\":\""+ rs.getString("ptajourneyid") +"\","					  
						  +			"\"journeytype\":\""+ rs.getString("journeytype") +"\","
						  +			"\"rute\":\""+ rs.getString("rute") +"\""
						  +	  	"}";
		  		buses += jsonString;
		  		if (!rs.isLast()) buses += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(buses))
		    	  buses = "[" + buses + "]";
		      Logger.write("communication_rest", String.format("Return %s",buses));
		      return buses;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getBusList >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getBusList >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	   }
	   
	   public static String getOperatorsList()
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;

		   try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call operatorlist()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call operatorlist()} executed"));
		      //STEP 4: Extract data from result set
		      String operators = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"id\":\""+ rs.getString("operatorid") +"\","	
						  +	  		"\"name\":\""+ rs.getString("operatorname") +"\","	
						  +			"\"mumbleusername\":\""+ rs.getString("mumbleusername") +"\""
						  +	  	"}";
		  		operators += jsonString;
		  		if (!rs.isLast()) operators += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(operators))
		    	  operators = "[" + operators + "]";
		      Logger.write("communication_rest", String.format("Return %s",operators));
		      return operators;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getOperatorsList >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getOperatorsList >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "";
	   }
	   
	   public static String getOperationsOfficeList()
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;
		   try
		   {
		      conn = getDBConnection();	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call operationsofficelist()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call operationsofficelist()} executed"));
		      //STEP 4: Extract data from result set
		      String operations = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"id\":\""+ rs.getString("operatorgroupid") +"\","	
						  +	  		"\"name\":\""+ rs.getString("operatorgroupname") +"\""
						  +	  	"}";
		  		operations += jsonString;
		  		if (!rs.isLast()) operations += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(operations))
		    	  operations = "[" + operations + "]";
		      Logger.write("communication_rest", String.format("Return %s",operations));
		      return operations;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getOperationsOfficeList >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getOperationsOfficeList >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "";
	   }
	   
	   public static String getGroupData(int customerId)
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;
		   try
		   {
			  conn = getDBConnection();
		      //STEP 3: Execute a query
		      
		      stmt = conn.prepareCall("{call getgroupsdata(?)}");
		      stmt.setInt(1, customerId);

		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call getgroupsdata(%s)} executed",customerId));
		      //STEP 4: Extract data from result set
		      String groups = "";
		      while(rs.next())
		      {
		    	  String jsonString  =	"{"
						  +	  		"\"groupid\":\""+ rs.getString("groupid") +"\","	
						  +	  		"\"groupname\":\""+ rs.getString("groupname") +"\","	
						  +			"\"vehiclescount\":\""+ rs.getString("vehiclescount") +"\""
						  +	  	"}";
		    	  groups += jsonString;
		    	  if (!rs.isLast()) groups += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(groups))
		    	  groups = "[" + groups + "]";
		      Logger.write("communication_rest", String.format("Return %s",groups));
		      return groups;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getGroupData >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getGroupData >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	   }
	   
	   
	   public static String getCallHistorybuses(int customerId)
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;
		   try
		   {
			  conn = getDBConnection();
		      //STEP 3: Execute a query
		      
		      stmt = conn.prepareCall("{call getbusesbycustomerid(?)}");
		      stmt.setInt(1, customerId);

		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call getbusesbycustomerid(%s)} executed",customerId));
		      //STEP 4: Extract data from result set
		      String busese = "";
		      while(rs.next())
		      {
		    	  String jsonString  =	"{"
						  				 +	 "\"vno\":\""+ rs.getString("vehiclenumber") + "\"" +
						  	  	        "}";
		    	  busese += jsonString;
		    	  if (!rs.isLast()) busese += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(busese))
		    	  busese = "[" + busese + "]";
		      Logger.write("communication_rest", String.format("Return %s",busese));
		      return busese;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getCallHistorybuses >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getCallHistorybuses >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	   }
	   
	   public static String getVehicleInfo(Vehicle model)
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;
		   try
		   {
		   	  conn = getDBConnection();
		      //STEP 3: Execute a query
		      
		   	 stmt = conn.prepareCall("{call getvehiclemapdata(?,?)}");
		      stmt.setInt(1, model.getCustomerid());
		      stmt.setString(2, model.getVehiclenumber());
		      
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call getvehiclemapdata(%s,%s)} executed", model.getCustomerid(),model.getVehiclenumber()));
		      //STEP 4: Extract data from result set
		      String buses = "";
		      while(rs.next())
		      {
		    	  buses =  rs.getString("jsonstring");
		      }
		      Logger.write("communication_rest", String.format("Return %s",buses));
		      return buses;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getvehiclemapdata >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getvehiclemapdata >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	   }
	   
	   //call communicationdev.getcallshistory(2, '1601,9911', '2017-10-22', 0, 1);
	   public static String getCallHistory(CallHistoryInputModel model)
	   {
		   Connection conn = null;
		   ResultSet rs = null;
		   CallableStatement stmt = null;
		   try
		   {
		   	  conn = getDBConnection();
		      //STEP 3: Execute a query
		      
		      stmt = conn.prepareCall("{call getcallshistory_v4(?,?,?,?,?)}");
		      stmt.setInt(1, Integer.parseInt(model.getCustomer()));
		      stmt.setString(2, model.getBuses());
		      stmt.setString(3, model.getDates());
		      stmt.setInt(4,Integer.parseInt(model.getCountfrom()));
		      stmt.setInt(5, Integer.parseInt(model.getCountoffset()));
		      
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call getcallshistory_v4(%s,%s,%s,%s,%s)} executed",model.getCustomer(),model.getBuses(),model.getDates(),model.getCountfrom(),model.getCountoffset()));
		      //STEP 4: Extract data from result set
		      String callhistory = "[";
		      while(rs.next())
		      {
		    	  callhistory += rs.getString("jsonstring");
		    	  if (!rs.isLast()) callhistory += ",";
		      }
		      callhistory += "]";
		    	  
		      Logger.write("communication_rest", String.format("Return %s",callhistory));
		      return callhistory;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getCallHistory >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getCallHistory >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	   }
	   
	   
	   private static void cleanup(Connection conn,CallableStatement stmt,ResultSet rs)
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_rest", "getGroupData >> Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
			   Logger.write("communication_rest", "getGroupData >> Exception DB: " + se2.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
	      }// nothing we can do
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
			   Logger.write("communication_rest", "getGroupData >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
	      }//end finally try
	   }

	public static String setUserInfo(UserInfo userInfo) 
	{
		String userName = userInfo.getUsername();
	    String fullName = userInfo.getFullname();
	    String shortName = userInfo.getShortname();
	    String email = userInfo.getEmail();
	    String address = userInfo.getAddress();
	    String voiceID = userInfo.getVoiceid();
	    String loginName = userInfo.getLoginname();
	    String phoneNumber = userInfo.getPhonenumber();
	    String company = userInfo.getCompany();
	    
	    Connection conn = null;
	    ResultSet rs = null;
	    CallableStatement stmt = null;

	    try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call setuserinfo(?,?,?,?,?,?,?,?)}");
		      stmt.setString(1, userName);
		      stmt.setString(2, fullName);
		      stmt.setString(3, shortName);
		      stmt.setString(4, email);
		      stmt.setString(5, address);
		      stmt.setString(6, voiceID);
		      stmt.setString(7, phoneNumber);
		      stmt.setString(8, company);
		      
		      stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call setuserinfo(%s,%s,%s,%s,%s,%s,%s,%s)} executed",userName,fullName,shortName,email,address,voiceID,phoneNumber,company));
		      Logger.write("communication_rest", String.format("Return OK"));
		      return "{\"resp\":\"OK\"}";
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "setUserInfo >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "setUserInfo >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "{\"resp\":\"NOTOK\"}";
	}

	public static String getUserInfo(String userID) 
	{
	   Connection conn = null;
	   ResultSet rs = null;
	   CallableStatement stmt = null;

		try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call getuserinfo(?)}");
		      stmt.setString(1, userID);
		      
		      rs = stmt.executeQuery();
		      //STEP 4: Extract data from result set
		      Logger.write("communication_rest", String.format("Qeury {call getuserinfo(%s)} executed",userID));
		      String user = "{";
		      while(rs.next())
		      {
		    	  String jsonString  =	"\"username\":\""+ rs.getString("username") +"\","
									 +	"\"fullname\":\""+ rs.getString("fullname") +"\","
									 +	"\"shortname\":\""+ rs.getString("shortname") +"\","
									 +	"\"email\":\""+ rs.getString("email") +"\","
									 +	"\"address\":\""+ rs.getString("address") +"\","
									 +	"\"voiceid\":\""+ rs.getString("voiceid") +"\","
									 +	"\"loginname\":\""+ rs.getString("loginname") +"\","
									 +	"\"phonenumber\":\""+ rs.getString("phonenumber") +"\","
									 +	"\"company\":\""+ rs.getString("company") +"\"";
		    	  user += jsonString;
		    	  if (!rs.isLast()) user += ",";
		      }
		      user += "}";
		      Logger.write("communication_rest", String.format("Return %s",user));
		      return user;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getUserInfo >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getUserInfo >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,rs);
		   }//end try
		   return "[]";
	}

	public static String changePwd(Credentials credentials)
	{
		String userName = credentials.getUsername();
	    String passWord = credentials.getPassword();
	    
	    Connection conn = null;
	    CallableStatement stmt = null;

	    try
		   {
		      conn = getDBConnection();
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call changepassword(?,?)}");
		      
		      stmt.setString(1, userName);
		      stmt.setString(2, passWord);
		      
		      stmt.executeQuery();
		      Logger.write("communication_rest", String.format("Qeury {call changepassword(%s,%s)} executed",userName,passWord));
		      Logger.write("communication_rest", String.format("Return OK"));
		      return "{\"resp\":\"OK\"}";
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "changePwd >> Exception DB: " + se.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "changePwd >> Exception DB: " + e.getMessage() + String.format(" - Database info - DB_URL %s ,USER %s, PASS %s",DB_URL,USER,PASS));
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup(conn,stmt,null);
		   }//end try
		   return "{\"resp\":\"NOTOK\"}";
	}
}  

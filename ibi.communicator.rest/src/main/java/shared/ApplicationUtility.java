package shared;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class ApplicationUtility
{
	public static boolean isNullOrEmpty(String text)
	{
		if(text == null || text.equals("") || text.equals("null"))
			return true;
		else
			return false;
	}
	public static String issueToken() 
    {
    	Random random = new SecureRandom();
    	String token = new BigInteger(130, random).toString(32);
    	return token;
    }
}

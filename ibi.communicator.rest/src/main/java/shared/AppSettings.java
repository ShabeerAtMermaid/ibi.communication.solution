package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import shared.Logger;

public class AppSettings 
{
	public static String getWebsocketServerURL()
	{	
		//return "ws://localhost:8025/websockets/server";
		//return "ws://localhost:8023/websockets/server";

		String value = getConfigValue("websocketurl");
		return (value == null || value.equals("")) ? "ws://localhost:8025/websockets/server" : value;
	}
	
	
	public static String getDatabaseName()
	{
		String value = getConfigValue("dbname");
		return (value == null || value.equals("")) ? "jdbc:mysql://localhost/communication" : value;
	}
	
	public static String getDatabaseUser()
	{
		String value = getConfigValue("dbusername");
		return (value == null || value.equals("")) ? "communication" : value;
	}
	
	public static String getDatabaseUserPassword()
	{
		String value = getConfigValue("dbpassword");
		return (value == null || value.equals("")) ? "ibi0786" : value;
	}
	
	public static int getAgeToOffline_Minutes()
	{
		String value = getConfigValue("agetoOffline_minutes");
		return (value == null || value.equals("")) ? 1 : Integer.parseInt(value);
	}
	/*
	private static String getConfigFilePath()
	{
		Path currentPath = Paths.get(System.getProperty("user.dir"));
		Path filePath = Paths.get(currentPath.toString(), "config.properties");
		return filePath.toString();
	}
	*/
	private static String getConfigValue(String settingName)
	{
		InputStream inputStream = null;
    	try 
    	{
    		Properties prop = new Properties();
    		String propFileName = "config.properties";
    		inputStream = AppSettings.class.getClassLoader().getResourceAsStream(propFileName);
    		
    		//String externalFileName = getConfigFilePath();
    		//Logger.write("communication_rest", "config.properties file path " + externalFileName);
    		//inputStream = new FileInputStream(new File(externalFileName));
    		
    		prop.load(inputStream);
    		return prop.getProperty(settingName);
    	} 
    	catch (Exception e) 
    	{
    		Logger.write("communication_rest", String.format("getConfigValue Exception %s " , e.getMessage()));
    		return "";
    	} 
    	finally 
    	{
    		try {
    			if (inputStream != null)
    				inputStream.close();	
    		} catch (IOException e) {}
    	}
	}
}

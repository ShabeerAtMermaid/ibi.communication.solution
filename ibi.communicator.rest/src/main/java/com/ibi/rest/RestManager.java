package com.ibi.rest;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import models.Bus;
import models.BusGroup;
import models.CallHistoryInputModel;
import models.Credentials;
import models.UserInfo;
import models.Vehicle;
import shared.AppSettings;
import shared.ApplicationUtility;
import shared.DatabaseManager;
import shared.Logger;
import shared.PersistenceManager;
import shared.WebsocketClientEndpoint;

import java.net.URI;
import java.net.URISyntaxException;


import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

public class RestManager 
{
	public static String getConfigs() 
	{
		try
		{
			return DatabaseManager.getConfigs();
		}
		catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "Exception getConfigs failed : " + e.getMessage());
	    	return "{}";
        }  
	}
	
	public static String authenticate(Credentials credentials) {

	    String username = credentials.getUsername();
	    String password = credentials.getPassword();
	    String mumbleUsername = credentials.getMumbleusername(); // need to change as it come from UI
	    Logger.write("communication_rest", "username : " + username + " password : " + password + " mumbleUsername: " + mumbleUsername);
	    try 
	    {
	    	// Issue a token for the user
	    	return DatabaseManager.authenticate(username, password,mumbleUsername);
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "UNAUTHORIZED");
	    	return "{\"token\":\"\",\"username\":\"\"}";
        }  
	}
	
	public static String setUserInfo(UserInfo userInfo) 
	{
	    try 
	    {
	    	return DatabaseManager.setUserInfo(userInfo);
        } 
	    catch (Exception e) 
	    {
	    	return "{\"resp\":\"NOTOK\"}";
        }  
	}
	
	public static String getUserInfo(String userID) {

	    try 
	    {
	    	// Issue a token for the user
	    	return DatabaseManager.getUserInfo(userID);
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "getUserInfo not found");
	    	return "[]";
        }  
	}
	
	public static void postcommand(String command) 
	{
	    try 
	    {
	    	Logger.write("communication_rest", "connecting to ws");
	        //WebsocketClientEndpoint clientEndPoint = new WebsocketClientEndpoint(new URI("ws://localhost:8023/websockets/server"));
	    	WebsocketClientEndpoint clientEndPoint = new WebsocketClientEndpoint(new URI(AppSettings.getWebsocketServerURL()));
	        Logger.write("communication_rest", "connected and sending conference call " + command);
	           // send message to websocket
	        clientEndPoint.sendMessage(command);
	        Logger.write("communication_rest", "command has been send");
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "Exception in sending");
	    	Logger.writeError("communication_rest",  e);
        }  
	}
	
	public static Boolean sendsubscriptioncommand(String command) 
	{
	    try 
	    {
	    	Logger.write("communication_rest", "connecting to ws");
	        //WebsocketClientEndpoint clientEndPoint = new WebsocketClientEndpoint(new URI("ws://localhost:8023/websockets/server"));
	    	WebsocketClientEndpoint clientEndPoint = new WebsocketClientEndpoint(new URI(AppSettings.getWebsocketServerURL()));
	        Logger.write("communication_rest", "connected and sending conference call " + command);
	           // send message to websocket
	        clientEndPoint.sendMessage(command);
	        Logger.write("communication_rest", "command has been send");
	        return true;
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "Exception in sending");
	    	Logger.writeError("communication_rest",  e);
        }  
	    return false;
	}

	public static String getBusGroups(int customerId)
    {
	   try
		{
		   return DatabaseManager.getGroupData(customerId);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "[]";
    }
	
	public static String getCallHistorybuses(int customerId)
    {
	   try
		{
		   return DatabaseManager.getCallHistorybuses(customerId);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "[]";
    }
	
	public static String getCallHistory(CallHistoryInputModel model)
    {
	   try
		{
		   return DatabaseManager.getCallHistory(model);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "[]";
    }
	
	public static String getVehicleInfo(Vehicle model)
    {
	   try
		{
		   return DatabaseManager.getVehicleInfo(model);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "[]";
    }
	
	
	public static String getOperatorsList()
    {
	   try
		{
		   return DatabaseManager.getOperatorsList();
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
    }
	
	public static String getOperationsOfficeList()
    {
	   try
		{
		   return DatabaseManager.getOperationsOfficeList();
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
    }
	
	
	
	public static String getBusList(int customerId,int groupId)
	{
		try
		{
			return DatabaseManager.getBusList(customerId,groupId);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
	}
	
	/* private supporting document*/
	private static String issueToken() 
    {
    	Random random = new SecureRandom();
    	String token = new BigInteger(130, random).toString(32);
    	return token;
    }

	public static String changePwd(Credentials credentials) 
	{
	    try 
	    {
	    	return DatabaseManager.changePwd(credentials);
        } 
	    catch (Exception e) 
	    {
	    	return "{\"resp\":\"NOTOK\"}";
        } 
	}
	
	public static String getPTAReportData() 
	{
		try
		{
			return DatabaseManager.getPTAReportData();
		}
		catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "Exception PTAReportData failed : " + e.getMessage());
	    	return "{}";
        }  
	}
	
	/*
	@POST
	@Path("/authenticate")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String authenticateUser(Credentials credentials) {

	    String username = credentials.getUsername();
	    String password = credentials.getPassword();
	    Logger.write("communication_rest", "username : " + username + " password : " + password);
	    try 
	    {
            // Authenticate the user using the credentials provided
            //authenticate(username, password);
	    	if (!("msa".equals(username)) || !("msa123".equals(password)))
	    	{
	    		Logger.write("communication_rest", "UNAUTHORIZED");
	    		return "UNAUTHORIZED";
	    	}
	    	// Issue a token for the user
            String token = issueToken();
            Logger.write("communication_rest", "token : " + token);
            return token;
            // Return the token on the response
            //return Response.ok(token).build();
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "UNAUTHORIZED");
	    	return "UNAUTHORIZED";
            //return Response.status(Response.Status.UNAUTHORIZED).build();
        }  
	    // Authenticate the user, issue a token and return a response
	}
	 */
}

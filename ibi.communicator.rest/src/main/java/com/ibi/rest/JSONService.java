package com.ibi.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.CallHistoryInputModel;
import models.Credentials;
import models.UserInfo;
import models.Vehicle;
import shared.DatabaseManager;
import shared.Logger;

@Path("/")
public class JSONService 
{
	
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getTesting()
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: test API called");
		return "OK";
	}
	
	@POST
	@Path("/authenticate")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String authenticate(Credentials credentials) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: authenticate API called");
	    return RestManager.authenticate(credentials);
	}
	
	
	@POST
	@Path("/changepwd")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String changePwd(Credentials credentials) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: changePwd API called");
	    return RestManager.changePwd(credentials);
	}
	
	@POST
	@Path("/setuserinfo")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String setUserInfo(UserInfo userInfo) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: setuserinfo API called");
	    return RestManager.setUserInfo(userInfo);
	}
	
	@GET
	@Path("/getuserinfo/{userid}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getUserInfo(@PathParam("userid") String userid)
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getuserinfo API called");
		return RestManager.getUserInfo(userid);
	}
	
	
	@POST
	@Path("/postcommand")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String postcommand(String command) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: Conference API called");
	    RestManager.postcommand(command);
	    return "{\"resp\":\"OK\"}"; //{"resp":"NOTOK"} In case of any error it will be 
	    
	}
	
	@POST
	@Path("/cmdsubscription")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String sendsubscriptioncommand(String command) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: Conference API called");
	    Boolean resp = RestManager.sendsubscriptioncommand(command);
	    return resp ? "{\"resp\":\"OK\"}" : "{\"resp\":\"NOTOK\"}"; //{"resp":"NOTOK"} In case of any error it will be 
	    
	}
	
	@POST
	@Path("/getvehicleinfo")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String getVehicleInfo(Vehicle model) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: getVehicleInfo API called");
		String resp = RestManager.getVehicleInfo(model);
	    return resp; 
	}
	
	
	@POST
	@Path("/callhistory")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String getCallHistory(CallHistoryInputModel model) 
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@POST: getCallHistory API called");
		Logger.write("communication_rest", String.format("{customer:%s, buses:%s, dates:%s, countfrom:%s, countoffset:%s}", model.getCustomer(),model.getBuses(),model.getDates(),model.getCountfrom(),model.getCountoffset()));
		String resp = RestManager.getCallHistory(model);
	    return resp; 
	}
	
	@GET
	@Path("/callhistorybuses/{customerId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getCallHistorybuses(@PathParam("customerId") int customerId)
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: busgroups API called");
		return RestManager.getCallHistorybuses(customerId);
	}
	
	
	@GET
	@Path("/busgroups/{customerId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getBusGroups(@PathParam("customerId") int customerId)
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: busgroups API called");
		return RestManager.getBusGroups(customerId);
	}
	
	@GET
	@Path("/buses/{customerId}/{groupId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getBusList(@PathParam("customerId") int customerId, @PathParam("groupId") int groupId)
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getBuses list API called");
		return RestManager.getBusList(customerId,groupId);
	}
	
	
	@GET
	@Path("/operators")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getOperatorsList()
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getOperatorsList API called");
		return RestManager.getOperatorsList();
	}
	
	@GET
	@Path("/operationoffices")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getOperationsOfficeList()
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getOperationsOfficeList API called");
		return RestManager.getOperationsOfficeList();
	}
	
	@GET
	@Path("/configs")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getConfigs()
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getConfigs API called");
		return RestManager.getConfigs();
	}
	
	@GET
	@Path("/ptareportdata")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getPTAReportData()
	{
		Logger.write("communication_rest", "-----------------------------");
		Logger.write("communication_rest", "@GET: getPTAReportData API called");
		return RestManager.getPTAReportData();
	}
	
	/*
	@GET
	@Path("/buses")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String moviajourneys(@QueryParam("line") String line)
	{
		Logger.write("dcu2_rest", "@GET: Movia journeys by line and geo position API called");
		return MoviaJourneyService.getJourneysByLineAndGeoPosition(line);
	}
	*/
}

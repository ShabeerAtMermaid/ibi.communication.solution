package models;

import java.io.Serializable;

@SuppressWarnings("serial")

public class UserInfo  implements Serializable 
{
	private String username;
	private String fullname;
	private String shortname;
	private String email;
	private String address;
	private String voiceid;
	private String loginname;
	private String phonenumber;
	private String company;
	
	public String getUsername()
	{
		return this.username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public String getFullname()
	{
		return this.fullname;
	}
	public void setFullname(String fullname)
	{
		this.fullname = fullname;
	}
	
	public String getShortname()
	{
		return this.shortname;
	}
	public void setShortname(String shortname)
	{
		this.shortname = shortname;
	}
	
	public String getEmail()
	{
		return this.email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getAddress()
	{
		return this.address;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public String getVoiceid()
	{
		return this.voiceid;
	}
	public void setvoiceid(String voiceid)
	{
		this.voiceid = voiceid;
	}
	
	public String getLoginname()
	{
		return this.loginname;
	}
	public void setLoginname(String loginname)
	{
		this.loginname = loginname;
	}
	
	public String getPhonenumber()
	{
		return this.phonenumber;
	}
	public void setPhonenumber(String phonenumber)
	{
		this.phonenumber = phonenumber;
	}
	
	public String getCompany()
	{
		return this.company;
	}
	public void setCompany(String company)
	{
		this.company = company;
	}
	
	
}

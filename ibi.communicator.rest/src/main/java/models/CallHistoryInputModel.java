package models;

import java.io.Serializable;
@SuppressWarnings("serial")

public class CallHistoryInputModel   implements Serializable 
{
	private String customer;
	private String buses;
	private String dates;
	private String countfrom;
	private String countoffset;
	
	public String getCustomer()
	{
		return this.customer;
	}
	public void setCustomer(String customer)
	{
		this.customer = customer;
	}
	
	public String getBuses()
	{
		return this.buses;
	}
	public void setBuses(String buses)
	{
		this.buses = buses;
	}
	
	public String getDates()
	{
		return this.dates;
	}
	public void setDates(String dates)
	{
		this.dates = dates;
	}
	
	public String getCountfrom()
	{
		return this.countfrom;
	}
	public void setCountfrom(String countfrom)
	{
		this.countfrom = countfrom;
	}
	
	public String getCountoffset()
	{
		return this.countoffset;
	}
	public void setCountoffset(String countoffset)
	{
		this.countoffset = countoffset;
	}
}

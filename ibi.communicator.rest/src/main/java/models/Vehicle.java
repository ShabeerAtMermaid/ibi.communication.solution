package models;

import java.io.Serializable;

public class Vehicle  implements Serializable 
{

	private String vehiclenumber;
	private int customerid;
	private int groups;
	private String status;
	private String line;
	private String destination;
	private String ptajourneyid;
	private String journeytype;
	private String lat;
	private String lng;
	private String rute;
	
	public String getVehiclenumber()
	{
		return this.vehiclenumber;
	}
	public void setVehiclenumber(String value)
	{
		this.vehiclenumber = value;
	}
	
	public int getCustomerid()
	{
		return this.customerid;
	}
	public void setCustomerid(int value)
	{
		this.customerid = value;
	}
	
	public int getGroups()
	{
		return this.groups;
	}
	public void setGroups(int value)
	{
		this.groups = value;
	}
	
	public String getStatus()
	{
		return this.status;
	}
	public void setStatus(String value)
	{
		this.status = value;
	}
	
	public String getLine()
	{
		return this.line;
	}
	public void setLine(String value)
	{
		this.line = value;
	}
	
	public String getDestination()
	{
		return this.destination;
	}
	public void setDestination(String value)
	{
		this.destination = value;
	}
	
	public String getPtajourneyid()
	{
		return this.ptajourneyid;
	}
	public void setPtajourneyid(String value)
	{
		this.ptajourneyid = value;
	}
	
	public String getJourneytype()
	{
		return this.journeytype;
	}
	public void setJourneytype(String value)
	{
		this.journeytype = value;
	}
	
	public String getLat()
	{
		return this.lat;
	}
	public void setLat(String value)
	{
		this.lat = value;
	}
	
	public String getLng()
	{
		return this.lng;
	}
	public void setLng(String value)
	{
		this.lng = value;
	}
	
	public String getRute()
	{
		return this.rute;
	}
	public void setRute(String value)
	{
		this.rute = value;
	}
}

package com.multiq.ibi.communicator.client;

import java.sql.*;
import shared.ApplicationUtility;
import shared.Logger;

public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/communication";

	   //  Database credentials
	   static final String USER = "communication";
	   static final String PASS = "ibi0786";
	   
	   private static Connection conn = null;
	   private static CallableStatement stmt = null;
	   private static ResultSet rs = null;
	   
	   private static Connection getDBConnection()
	   {
		   Connection conn = null;
		   try
		   {
		      //STEP 1: Register JDBC driver
		      Class.forName(JDBC_DRIVER);
		      Logger.write("communication_rest", "Step -01 Register JDBC driver");
		      
		      //STEP 2: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      return conn;
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   return null;
	   }
   
	   public static String authenticate(String userName, String password,String mumbleUsername)
	   {
		   try
		   {
			   try
			   {
			      conn = getDBConnection();
			      Logger.write("communication_rest", "Step -02 Open a connection");
		
			      //STEP 3: Execute a query
			      stmt = conn.prepareCall("{call authenticate(?,?,?)}");
			      stmt.setString(1, userName);
			      stmt.setString(2, password);
			      stmt.setString(3, mumbleUsername);
			      rs = stmt.executeQuery();
			      Logger.write("communication_rest", "Qeury executed");
			      //STEP 4: Extract data from result set
			      while(rs.next())
			      {
			    	if (rs.getString("auth").equalsIgnoreCase("1"))
			    		return ApplicationUtility.issueToken();
			      }
			      return "";
			      //STEP 6: Clean-up environment
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
				   Logger.writeError("communication_rest",  se);
			   }
			   catch(Exception e)
			   {
				   //return "NOT AUTH";
				   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
				   Logger.writeError("communication_rest",  e);
			   }
			   finally
			   {
				   cleanup();
			   }//end try
			   return "";
		   }
		   catch(Exception se)
		   {
	    	  Logger.writeError("communication_rest",  se);
		   }//end finally try
		   return "{}";
	   }
	   
	   public static String getBusList(int groupId)
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call vehicleslistbygroupid(?,?)}");
		      stmt.setInt(1, groupId);
		      stmt.setInt(2, 1); // Need to pass configuration of offlineage
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String buses = "";
		      
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"vehiclenumber\":\""+ rs.getString("vehiclenumber") +"\","	
						  +	  		"\"customerid\":\""+ rs.getString("customerid") +"\","	
						  +			"\"groups\" : \""+ rs.getString("vehiclescount") +"\"," 
						  +			"\"status\":\""+ rs.getString("status") +"\","
						  +			"\"rute\":\""+ rs.getString("rute") +"\""
						  +	  	"}";
		  		buses += jsonString;
		  		if (!rs.isLast()) buses += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(buses))
		    	  buses = "[" + buses + "]";
		      return buses;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "{}";
	   }
	   
	   public static String getGroupData()
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call groupsdata()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String groups = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"groupid\":\""+ rs.getString("groupid") +"\","	
						  +	  		"\"groupname\":\""+ rs.getString("groupname") +"\","	
						  +			"\"vehiclescount\":\""+ rs.getString("vehiclescount") +"\""
						  +	  	"}";
		  		groups += jsonString;
		  		if (!rs.isLast()) groups += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(groups))
		    	  groups = "[" + groups + "]";
		      return groups;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "";
	   }
	   private static void cleanup()
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
	    	  Logger.writeError("communication_rest",  se);
	      }//end finally try
	   }
}  

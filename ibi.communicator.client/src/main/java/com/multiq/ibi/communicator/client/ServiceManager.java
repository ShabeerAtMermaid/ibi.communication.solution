package com.multiq.ibi.communicator.client;

import org.glassfish.tyrus.server.Server;

import paniccall.PanicCallHandler;
import pushtotalk.PushtoTalkHandler;
import shared.JourneyManager;
import shared.Logger;
import shared.PositionManager;
import tcpclient.TCPClient;
import websocket.WebSocketServerEndpoint;

public class ServiceManager {
	private static String version = "Production - IBI.Client.Communication.Java_7.0.3"; // "Development
																						// -
																						// IBI.Client.Java_1.1.4";
																						// //"Production
																						// -
																						// IBI.Client.Java_2.0.9";
	private static String buildDate = "2017-06-12";

	public static void StartServices() {
		
		Logger.write("communication_clientservice", "============================================");
		Logger.write("communication_clientservice", "Current Version: " + version);
		Logger.write("communication_clientservice", "Build Date: " + buildDate);
		Logger.write("communication_clientservice", "============================================");

		Logger.write("communication_clientservice", "-----------------------------------------------");
		PositionManager.initialize();
		Logger.write("communication_clientservice", "Listener started for positioning data");

		JourneyManager.initialize();
		Logger.write("communication_clientservice", "Listener started for journey data");

		PanicCallHandler.initialize();
		Logger.write("communication_clientservice", "Panic state change broadcaster process started");

		PushtoTalkHandler.initialize();
		Logger.write("communication_clientservice", "PushtoTalk state change broadcaster process started");

		TCPClient client = new TCPClient();
		client.startCommunication();
		Logger.write("communication_clientservice", "Tcp client has been started!!!");
		
		runWebSocketServer();
		Logger.write("communication_clientservice", "communicator client service has been started!!!");

	}

	private static void runWebSocketServer() {
		Server server = null;
		try {
			Logger.write("communication_clientservice", "Web Socket >> websockets server creating !!!");
			server = new Server("localhost", 8025, "/websockets", WebSocketServerEndpoint.class);
			Logger.write("communication_clientservice", "Web Socket >> websockets server created !!!");

			Logger.write("communication_clientservice", "Web Socket >> websockets server starting !!!");
			server.start();
			Logger.write("communication_clientservice", "Web Socket >> started !!!");
			Thread.currentThread().join();
		} catch (Exception e) {
			Logger.writeError("communication_clientservice", e);
		} finally {
			if (server != null)
				server.stop();
		}
	}
}

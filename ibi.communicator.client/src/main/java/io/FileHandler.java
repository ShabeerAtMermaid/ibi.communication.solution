package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import shared.AppSettings;
import shared.ApplicationUtility;
import shared.Logger;

public class FileHandler 
{
	public static String readPushtoTalkActivatedFile() throws Exception
	{
		try 
		{
			File file = new File(AppSettings.getPushtoTalkActivatedFilePath());
			if (file.exists())
			{
				String fileContent = readFileContents(file.getAbsolutePath()).trim();
				if (ApplicationUtility.isNullOrEmpty(fileContent))
				{
					return "";
				}	
				return fileContent;
			}
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String writeContentInCallFile(String contents) throws Exception
	{
		try 
		{
			writeContenFile(AppSettings.getInCallFilePath(), contents);
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String writeContentRingingFile(String contents) throws Exception
	{
		try 
		{
			writeContenFile(AppSettings.getRingingFilePath(), contents);
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String writeContentMumbleClientOnlineStatusFile(String contents) throws Exception
	{
		try 
		{
			writeContenFile(AppSettings.getMumbleClientOnlineStatusFilePath(), contents);
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String readMumbleClientOnlineStatusFile() throws Exception
	{
		try 
		{
			File file = new File(AppSettings.getMumbleClientOnlineStatusFilePath());
			if (file.exists())
			{
				String fileContent = readFileContents(file.getAbsolutePath()).trim();
				if (ApplicationUtility.isNullOrEmpty(fileContent))
				{
					return "";
				}	
				return fileContent;
			}
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String writeContentPanicCancelFile(String contents) throws Exception
	{
		try 
		{
			writeContenFile(AppSettings.getPanicCancelFilePath(), contents);
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	public static String writeContentPanicFile(String contents) throws Exception
	{
		try 
		{
			writeContenFile(AppSettings.getPanicStateFilePath(), contents);
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "";
	}
	
	private static void writeContenFile(String filePath, String contents) throws Exception
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream(filePath);
			java.io.FileDescriptor fd = fos.getFD();
			
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos,"UTF-8"), 32768);
			out.write(contents);
			out.flush();
			fd.sync();
			out.close();
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
	}
	
	private static String readFileContents(String filePath) throws Exception
	{
		File sourceFile = new File(filePath);
		FileInputStream xmlStream = new FileInputStream(sourceFile.getAbsolutePath());

		DataInputStream xmlIn = new DataInputStream(xmlStream);
		BufferedReader xmlBr = new BufferedReader(new InputStreamReader(xmlIn, Charset.forName("UTF-8")));
		
		String line;
		String xmlData = "";

		while((line = xmlBr.readLine()) != null)
		{
			xmlData += line;	
		}
		
		xmlIn.close();
		xmlStream.close();
		xmlData = xmlData.trim().replaceFirst("^([\\W]+)<","<");
		return xmlData;
	}
}

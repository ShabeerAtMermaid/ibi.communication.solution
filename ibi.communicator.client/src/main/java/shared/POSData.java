package shared;

import java.util.Date;

public class POSData {

	/*** Private variables ***/
	private Date updateTime;
	private String clock;
	private double latitude;
	private double longitude;
	private String line;
	private String destination;
	private String nextStop;
	private String zone;
	private boolean signOverwritten;
	private String schedule;
	private String PTAjourneyId;
	private String journeyType;

	private String rawData;

	private static  POSData currentPOSData = new POSData();
	
	public static POSData getInstance()
	{
		return currentPOSData;
	}
	
	/*** Getters / Setters ***/
	public Date getUpdateTime()
	{
		return this.updateTime;
	}

	public String getSchedule()
	{
		return this.schedule;
	}

	public void setSchedule(String value)
	{
		this.schedule = value;
	}
	
	public String getClock()
	{
		return this.clock;
	}

	public void setClock(String value)
	{
		this.clock = value;
	}
	
	public double getLatitude()
	{
		return this.latitude;
	}

	public void setLatitude(double value)
	{
		this.latitude = value;
	}

	public double getLongitude()
	{
		return this.longitude;
	}

	public void setLongitude(double value)
	{
		this.longitude = value;
	}
	
	public String getLine()
	{
		//return this.line; //
		return !ApplicationUtility.isNullOrEmpty(this.line)? this.line : "";
	}

	public void setLine(String value)
	{
		this.line = value;
	}

	public String getDestination()
	{
		
		return !ApplicationUtility.isNullOrEmpty(this.destination)? this.destination : "";
	}

	public void setDestination(String value)
	{
		this.destination = value;
	}

	
	public String getPTAjourneyId()
	{
		String journeyId = this.PTAjourneyId;
		if (!ApplicationUtility.isNullOrEmpty(journeyId))
		{
			String jNumber = journeyId;
			int dotIndex = jNumber.lastIndexOf(".");
			return jNumber.substring(0,dotIndex);
		}
		return "";
		//return !ApplicationUtility.isNullOrEmpty(this.PTAjourneyId)? this.PTAjourneyId : "";
	}

	public void setPTAjourneyId(String value)
	{
		this.PTAjourneyId = value;
	}
	
	public String getJourneyType()
	{
		return !ApplicationUtility.isNullOrEmpty(this.journeyType)? this.journeyType : "";
	}

	public void setJourneyType(String value)
	{
		this.journeyType = value;
	}
	
	public String getNextStop()
	{
		return this.nextStop;
	}

	public void setNextStop(String value)
	{
		this.nextStop = value;
	}

	public String getZone()
	{
		return this.zone;
	}

	public void setZone(String value)
	{
		this.zone = value;
	}

	public boolean getSignOverwritten()
	{
		return this.signOverwritten;
	}

	public void setSignOverwritten(boolean value)
	{
		this.signOverwritten = value;
	}

	public String getRawData()
	{
		return this.rawData;
	}

	public void setRawData(String value)
	{
		this.rawData = value;
	}

	public POSData()
	{
		this.updateTime = new Date();
	}
	
	public boolean isTestOK()
	{
		if(!ApplicationUtility.isNullOrEmpty(this.rawData))
			return this.rawData.toLowerCase().substring(this.rawData.indexOf("test=") + 5).trim().equals("ok");

		return false;
	}
}

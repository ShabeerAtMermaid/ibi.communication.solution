package shared;

import java.security.*;
import java.io.*;


public class MD5 
{
	public static String fromFile(String filename) 
	{
		try
		{
			byte[] b = createChecksum(filename);
			String result = "";
			
			for (int i=0; i < b.length; i++) {
				result +=
					Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
			}
			return result;
		}
		catch(Exception ex)
		{
			Logger.writeError("dcusystem", ex);

			return "";
		}			
	}

	public static String fromText(String text)
	{
		try
		{
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(text.getBytes());
			byte[] hash = digest.digest();

			StringBuffer hexString = new StringBuffer();

			for (int i=0;i<hash.length;i++) 
			{
				hexString.append(Integer.toHexString(0xFF & hash[i]));
			}

			return hexString.toString();
		}
		catch(Exception ex)
		{
			Logger.writeError("dcusystem", ex);

			return "";
		}		
	}	

	/*** Private helpers ***/
	private static byte[] createChecksum(String filename) throws Exception
	{
		InputStream fis =  new FileInputStream(filename);

		byte[] buffer = new byte[1024];
		MessageDigest complete = MessageDigest.getInstance("MD5");

		int numRead;
		do {
			numRead = fis.read(buffer);
			if (numRead > 0) {
				complete.update(buffer, 0, numRead);
			}
		} while (numRead != -1);
		fis.close();
		return complete.digest();
	}

}
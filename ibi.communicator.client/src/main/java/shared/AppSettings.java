package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import shared.Logger;

public class AppSettings 
{
	static Document m_xmlDocument = null;
	static long m_lastModified = 0;
	
	public static String resolveApplicationDirectory()
	{
		String applicationPath = System.getProperty("user.dir");
		String osName = System.getProperty("os.name");
		File applicationDirectory = new File(applicationPath);
		if(!applicationPath.endsWith("bin") && osName.toLowerCase().indexOf("windows") != -1 && applicationDirectory.exists())
			applicationPath = applicationPath + "/bin";

		return applicationPath + "/";
	}
	
	public static String getPanicStateFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/panicactive.txt";
	}
	
	public static String getPushtoTalkFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/pushtotalkactivated.txt";
	}
	
	
	
	public static String getOperatorListPartialPath()
	{
		return resolveRootPath() + "/storage/system/dcu/Data/dcu2client_operatorslist.json.partial";
	}
	
	public static String getOperatorListPath()
	{
		return resolveRootPath() + "/storage/system/dcu/Data/dcu2client_operatorslist.json";
	}
	
	public static String getPanicCancelFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/paniccancel.txt";
	}
	
	public static String getPushtoTalkActivatedFilePath()
	{
		return resolveRootPath() +"/storage/system/cache/pushtotalkactivated.txt";
	}
	
	public static String getInCallFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/incall.txt";
	}
	
	public static String getRingingFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/incommingcall.txt";
	}
	
	public static String getMumbleClientOnlineStatusFilePath()
	{
		return resolveRootPath() + "/storage/system/cache/mumbleclientonlinestatus.txt";
	}
	
	
	
	public static int getCustomerID()
	{
		String value = getValue("root/customerid");

		return (value == null || value.equals("")) ? 0 : Integer.parseInt(value);		
	}
	
	public static String getBusNumber()
	{
		String value = getValue("root/busnumber");

		return (value == null || value.equals("")) ? "0" : value;	
	}
	
	public static Boolean getTraceLogs() {
		String value = getValue("root/trace");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	
	public static String getBotServerAddress()
	{
		String value = getValue("root/botserver");
		return (value == null || value.equals("")) ? "teeny.mermaid.dk" : value;
	}
	
	public static String getOperationRESTURL()
	{
		String value = getValue("root/operationresturl");
		return (value == null || value.equals("")) ? "ibi.multiq.com" : value;
	}
	
	public static int getBotServerPort()
	{
		String value = getValue("root/botserverport");
		return (value == null || value.equals("")) ? 8026 : Integer.parseInt(value);
	}
	
	
	/*** Private methods ***/
	private static String getValue(String path)
	{
		try
		{
			Document xmlDocument = getSettingsDocument();
			Element node = null;
			
			if(xmlDocument != null)
			{
				node = ensureNodeExists(xmlDocument, path, false);
			
				//saveSettingsDocument(xmlDocument);
			}
			
			if(node != null)
				return node.getText();
			else
				return "";	
		}
		catch(Exception ex)
		{
			Logger.writeError("ibi", ex);

			return "";
		}	
	}
	
	private static Element ensureNodeExists(Document xmlDocument, String path, boolean bCreateNode) throws Exception
	{
		Element node = (Element) XPath.newInstance(path).selectSingleNode(xmlDocument);

		if(node == null && bCreateNode)
		{
			Element parent;
			String parentPath = "";
			String nodeName = path;

			int splitIndex = path.lastIndexOf("/");

			if(splitIndex != -1)
			{
				parentPath = path.substring(0, splitIndex);
				nodeName = path.substring(splitIndex + 1);
			}

			if(parentPath == null || parentPath == "")
				parent = xmlDocument.getRootElement();
			else
				parent = ensureNodeExists(xmlDocument, parentPath, bCreateNode);

			node = new Element(nodeName);

			parent.addContent(node);			
		}

		return node;
	}
	
	public static String resolveRootPath()
	{
		String osName = System.getProperty("os.name");

		if(osName.toLowerCase().indexOf("windows") != -1)
			return "C:";
		else
			return "";
	}
	
	private static String resolveSettingFilePath()
	{
		return resolveRootPath() + "/storage/config/main.xml";
	}
	
	public static void ensureDirectoryExist(String path)
	{
		ArrayList<String> missingPaths = new ArrayList<String>();

		File currentDirectory = new File(path);

		while(!currentDirectory.exists())
		{
			missingPaths.add(0, currentDirectory.getAbsolutePath());

			currentDirectory = new File(currentDirectory.getParent());
		}

		for(int i = 0; i < missingPaths.size(); i++)
		{			
			currentDirectory = new File(missingPaths.get(i));

			currentDirectory.mkdir();
		}
	}
	
	private static Document getSettingsDocument()
	{	
		try
		{
			File xmlFile = new File(resolveSettingFilePath());
			if ( m_xmlDocument == null || m_lastModified < xmlFile.lastModified() )
			{
				
	
				ensureDirectoryExist(xmlFile.getParent());
	
				if(!xmlFile.exists())
					return null;
				
				FileInputStream settingsStream = new FileInputStream(xmlFile.getAbsolutePath());
	
				DataInputStream in = new DataInputStream(settingsStream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String line;
				String settingsData = "";
	
				while((line = br.readLine()) != null)
				{
					settingsData += line;	
				}
					
				in.close();
				settingsStream.close();
				SAXBuilder builder = new SAXBuilder();
				//Document xmlDocument = builder.build(xmlFile.getAbsolutePath());
				m_xmlDocument = builder.build(new StringReader(settingsData));
				m_lastModified = xmlFile.lastModified();
			}		
		}
		catch(Exception ex)
		{
			m_xmlDocument = null;
			Logger.writeError("ibi", ex);
		}
		
		return m_xmlDocument;
	}

}

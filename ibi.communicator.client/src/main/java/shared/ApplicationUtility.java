package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Random;

public class ApplicationUtility 
{
	public static boolean isNullOrEmpty(String text)
	{
		if(text == null || text.equals("") || text.equalsIgnoreCase("null"))
			return true;
		else
			return false;
	}
	public static String issueToken() 
    {
    	Random random = new SecureRandom();
    	String token = new BigInteger(130, random).toString(32);
    	return token;
    }
	
	public static String readPanicStateFileContents() throws Exception
	{
		try 
		{
			File file = new File(AppSettings.getPanicStateFilePath());
			if (file.exists())
			{
				String fileContent = ApplicationUtility.readFileContents(file.getAbsolutePath()).trim();
				if (ApplicationUtility.isNullOrEmpty(fileContent))
				{
					return "0";
				}
				else if (fileContent.equalsIgnoreCase("1") || fileContent.equalsIgnoreCase("2")) 
				{
					return  fileContent;
				}	
				return "0";
			}
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "0";
	}
	
	public static String readPushtoTalkFileContents() throws Exception
	{
		try 
		{
			File file = new File(AppSettings.getPushtoTalkFilePath());
			if (file.exists())
			{
				String fileContent = ApplicationUtility.readFileContents(file.getAbsolutePath()).trim();
				if (ApplicationUtility.isNullOrEmpty(fileContent))
				{
					return "0";
				}
				else if (fileContent.equalsIgnoreCase("1")) 
				{
					return  fileContent;
				}	
				return "0";
			}
		} 
		catch (IOException e) 
		{
			Logger.writeError("communication_clientservice", e);
		}
		return "0";
	}
	
	public static String readFileContents(String filePath) throws Exception
	{
		File sourceFile = new File(filePath);
		FileInputStream xmlStream = new FileInputStream(sourceFile.getAbsolutePath());

		DataInputStream xmlIn = new DataInputStream(xmlStream);
		BufferedReader xmlBr = new BufferedReader(new InputStreamReader(xmlIn, Charset.forName("UTF-8")));
		
		String line;
		String xmlData = "";

		while((line = xmlBr.readLine()) != null)
		{
			xmlData += line;	
		}
		
		xmlIn.close();
		xmlStream.close();
		xmlData = xmlData.trim().replaceFirst("^([\\W]+)<","<");
		return xmlData;
	}
}

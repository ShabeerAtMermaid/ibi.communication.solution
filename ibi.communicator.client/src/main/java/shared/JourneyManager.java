package shared;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.*;

public class JourneyManager
{
	private static boolean JourneyListenerEnabled = true;
	private static UDPListener JourneyListener;
	private static JourneyDataPackageListener JourneyListenerInstance;
	//private JourneyData currentJourneyData = JourneyData.getInstance(); 

	public static boolean getJourneyListenerEnabled()
	{
		return JourneyListenerEnabled;
	}

	public static void setJourneyListenerEnabled(boolean value)
	{
		JourneyListenerEnabled = value;
	}

	/*** Public methods ***/
	public static void initialize()
	{
		Logger.write("communication_clientservice", "-----------------------------------------------");
		if(JourneyListenerEnabled)
		{
			JourneyListener = new UDPListener(7446); // 7670 //7671
			JourneyListenerInstance = new JourneyDataPackageListener();
			JourneyListener.addPackageReceivedEventListener(JourneyListenerInstance);
			Logger.write("communication_clientservice", "start listening journey data at port 7446");
		}
	}
	
	
	private static class JourneyDataPackageListener implements UDPListener.PackageReceivedEventListener 
	{
		private String currentJourneyDataHash;

		//@Override
		public void packageReceivedEventOccurred(UDPListener.PackageReceivedEvent evt) 
		{
			try
			{
				// See if data is new
				String rawData = evt.getData();
				String rawDataHash = MD5.fromText(rawData);
				boolean acceptPackage = false;

				if (!rawDataHash.equals(this.currentJourneyDataHash))
					acceptPackage = true;

				// Extract data
				if (acceptPackage)
				{
					//JourneyData data = new JourneyData();
					POSData.getInstance().setRawData(rawData);
					
					Logger.write("communication_clientservice_ping", evt.getData(),AppSettings.getTraceLogs());
					
					String[] values = evt.getData().substring(evt.getData().indexOf(";")+1).split(";");
					
					for (int i = 0; i < values.length; i++)
					{
						String valueData = values[i];

						String[] valuePair = valueData.split("=");

						String valueName = valuePair[0];
						String value = "";

						if(valuePair.length == 2)
							value = valuePair[1];

						if (valueName.equals("moviajourneynumber"))
						{
							if(!ApplicationUtility.isNullOrEmpty(value))
							{
								POSData.getInstance().setPTAjourneyId(value);
								POSData.getInstance().setJourneyType("J");
							}
						}
						else if (valueName.equals("signitemid"))
						{
							POSData.getInstance().setPTAjourneyId("");
							POSData.getInstance().setJourneyType("M");
						}
					}
					this.currentJourneyDataHash = rawDataHash;
				}
			}
			catch(Exception ex)
			{
				Logger.writeError("communication_clientservice_ping", ex);
			}
		}
	}	
}


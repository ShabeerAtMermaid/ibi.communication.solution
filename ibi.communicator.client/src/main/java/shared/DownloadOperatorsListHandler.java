package shared;

import java.io.BufferedOutputStream;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class DownloadOperatorsListHandler 
{
	private static Timer OperatorsDownloadTimer;
	private static Boolean resetTimmer = false;
	
	public static void initialize()
	{
		Logger.write("communication_clientservice", "Destination selection list downloader starting");
		OperatorsDownloadTimer = new Timer();
		OperatorsDownloadTimer.schedule(SignOverwrittenTimerTask.create(), 1000, 1000);
		Logger.write("communication_clientservice", "Sign UDP broadcaster initializing");
	}
	
	public static class SignOverwrittenTimerTask extends TimerTask
	{		
		public static SignOverwrittenTimerTask create()
		{
			return new SignOverwrittenTimerTask();
		}
		
		@Override
		public void run() 
		{
			try
			{
				
			Logger.write("communication_clientservice", "Downloading OperatorsList");
			
			File sourceFile = new File(AppSettings.getOperatorListPartialPath());

			if(sourceFile.exists())
				sourceFile.delete();
			
			String url = "http://" + AppSettings.getOperationRESTURL() + "/rest/operators";
			Logger.write("communication_clientservice", "url : " + url);
			java.io.BufferedInputStream in = new java.io.BufferedInputStream(new java.net.URL(url).openStream());
			java.io.FileOutputStream fos = new java.io.FileOutputStream(sourceFile.getAbsolutePath());
			java.io.FileDescriptor fd = fos.getFD();
			java.io.BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
			
			byte[] data = new byte[1024];
			int x=0;
			
			while((x=in.read(data,0,1024))>=0)
			{
				bout.write(data,0,x);
			}
			bout.flush();
			fd.sync();
			bout.close();
			in.close();
			
			File targetFile = new File(AppSettings.getOperatorListPath());
			
			if(targetFile.exists())
			{
				String tmpNewMD5 = MD5.fromFile(sourceFile.getAbsolutePath());
				String tmpOldMD5 = MD5.fromFile(targetFile.getAbsolutePath());
				if (!tmpNewMD5.equalsIgnoreCase(tmpOldMD5))
				{
					targetFile.delete();
					sourceFile.renameTo(targetFile);
					Logger.write("communication_clientservice", "OperatorList downloaded and updated");
				}
				else
				{
					sourceFile.delete();
					Logger.write("communication_clientservice", "OperatorList not updated on server so ignoring the download");
				}
			}
			else
			{
				sourceFile.renameTo(targetFile);
				Logger.write("communication_clientservice", "OperatorList downloaded and updated");
			}
			
			if (!resetTimmer)
			{
				if (OperatorsDownloadTimer != null)
				{
					OperatorsDownloadTimer.cancel();
					OperatorsDownloadTimer.purge();
					OperatorsDownloadTimer = null;
				}
				OperatorsDownloadTimer = new Timer();
				OperatorsDownloadTimer.schedule(SignOverwrittenTimerTask.create(), 60 * 60 * 1000, 60 * 60 * 1000);
				resetTimmer = true;
			}
			
		}
		catch(Exception ex)
		{
			System.err.println(ex);
			Logger.writeError("communication_clientservice", ex);
		}		
	}
	}

}

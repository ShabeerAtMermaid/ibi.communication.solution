package shared;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.*;

public class PositionManager
{
	private static boolean POSListenerEnabled = true;
	private static UDPListener POSListener;
	private static POSDataPackageListener POSListenerInstance;
	//private POSData currentPOSData = POSData.getInstance(); 

	public static boolean getPOSListenerEnabled()
	{
		return POSListenerEnabled;
	}

	public static void setPOSListenerEnabled(boolean value)
	{
		POSListenerEnabled = value;
	}

	/*** Public methods ***/
	public static void initialize()
	{
		Logger.write("communication_clientservice", "-----------------------------------------------");
		if(POSListenerEnabled)
		{
			POSListener = new UDPListener(7670); // 7670 //7671
			POSListenerInstance = new POSDataPackageListener();
			POSListener.addPackageReceivedEventListener(POSListenerInstance);
			Logger.write("communication_clientservice", "start listening positioning data at port 7670");
		}
	}

	private static class POSDataPackageListener implements UDPListener.PackageReceivedEventListener 
	{
		private String currentPOSDataHash;

		//@Override
		public void packageReceivedEventOccurred(UDPListener.PackageReceivedEvent evt) 
		{
			try
			{
				// See if data is new
				String rawData = evt.getData();
				String rawDataHash = MD5.fromText(rawData);
				boolean acceptPackage = false;

				if (!rawDataHash.equals(this.currentPOSDataHash))
					acceptPackage = true;

				// Extract data
				if (acceptPackage)
				{
					//POSData data = new POSData();
					POSData.getInstance().setRawData(rawData);
					
					Logger.write("communication_clientservice_ping", evt.getData(),AppSettings.getTraceLogs());
					
					String[] values = evt.getData().substring(evt.getData().indexOf(";")+1).split(";");
					
					for (int i = 0; i < values.length; i++)
					{
						String valueData = values[i];

						String[] valuePair = valueData.split("=");

						String valueName = valuePair[0];
						String value = "";

						if(valuePair.length == 2)
							value = valuePair[1];

						NumberFormat engFormat = NumberFormat.getInstance(Locale.ENGLISH); 
						
						if (valueName.equals("lat"))
						{
							if(!ApplicationUtility.isNullOrEmpty(value))
							{
								double lat = engFormat.parse(value).doubleValue();
								POSData.getInstance().setLatitude(lat);
							}
						}
						else if (valueName.equals("lon"))
						{
							if(!ApplicationUtility.isNullOrEmpty(value))
							{
								double lon = engFormat.parse(value).doubleValue();
								POSData.getInstance().setLongitude(lon);						
							}
						}					
						else if(valueName.equals("line"))
						{
							POSData.getInstance().setLine(value);
						}
						else if (valueName.equals("destination"))
						{
							POSData.getInstance().setDestination(value);
						}
						else if (valueName.equals("nextstop"))
						{
							POSData.getInstance().setNextStop(value);
						}
						else if (valueName.equals("zone"))
						{
							POSData.getInstance().setZone(value);
						}
						else if (valueName.equals("override"))
						{
							POSData.getInstance().setSignOverwritten(value.equals("1"));
						}
						else if (valueName.equals("clock"))
						{							
							POSData.getInstance().setClock(value);
						}
						else if (valueName.equals("schedule"))
						{							
							POSData.getInstance().setSchedule(value);
						}
					}
					this.currentPOSDataHash = rawDataHash;
				}
			}
			catch(Exception ex)
			{
				Logger.writeError("communication_clientservice_ping", ex);
			}
		}
	}	
}


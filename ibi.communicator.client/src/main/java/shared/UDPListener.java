package shared;


import java.net.*;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.event.EventListenerList;

public class UDPListener {

	/*** Private attributes ***/
	private boolean enabled = true; 

	private int port;
	private String packageSplitter = "<EOS>";
	private MulticastSocket socket;
	private String receiveBuffer = "";

	/*** Events ***/	
	public class PackageReceivedEvent extends EventObject
	{
		/*** Private variables ***/
		private static final long serialVersionUID = 1L;

		private String data;
	
		public String getData()
		{
			return this.data;
		}
		
		public PackageReceivedEvent(Object source, String data) {
			super(source);
			
			this.data = data;
		}
	}

	public interface PackageReceivedEventListener extends EventListener
	{
		public void packageReceivedEventOccurred(PackageReceivedEvent evt);
	}

	private EventListenerList listenerList = new EventListenerList();	

	public void addPackageReceivedEventListener(PackageReceivedEventListener listener) 
	{
		this.listenerList.add(PackageReceivedEventListener.class, listener);
	}

	public void removePackageReceivedEventListener(PackageReceivedEventListener listener) 
	{
		this.listenerList.remove(PackageReceivedEventListener.class, listener);
	}

	private void onPackageReceived(PackageReceivedEvent evt)
	{
		Object[] listeners = listenerList.getListenerList();

		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == PackageReceivedEventListener.class) {
				((PackageReceivedEventListener) listeners[i+1]).packageReceivedEventOccurred(evt);
			}
		}
	}

	/*** Constructors ***/
	public UDPListener(int port)
	{
		this.port = port;
		this.configureSocket();
	}

	public UDPListener(int port, String packageSplitter)
	{
		this.port = port;
		this.packageSplitter = packageSplitter;
		this.configureSocket();
	}

	/*** Private methods ***/
	private void configureSocket()
	{
		try 
		{			
			// Create a socket to listen on the port.
			this.socket = new MulticastSocket(port);
			// Start thread for listening
			new Thread() {
				public void run() {
					try { beginListen(); }
					catch (Exception e) { System.out.println(e); Logger.writeError("udplistener", e); }
				}
			}.start();
		} 
		catch (Exception e) 
		{
			System.err.println(e);
			Logger.writeError("udplistener", e);
		}
	}
	private void beginListen()
	{
		try	
		{
			// Create a buffer to read datagrams into. If a
			// packet is larger than this buffer, the
			// excess will simply be discarded!
			byte[] buffer = new byte[2048];
			// Create a packet to receive data into the buffer
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			// Now loop forever, waiting to receive packets and printing them.
			while (this.enabled) 
			{
				// Wait to receive a datagram
				this.socket.receive(packet);
				// Convert the contents to a string, and display them
				String msg = new String(buffer, 0, packet.getLength(), "UTF-8");
				this.receiveBuffer += msg;
				String result = this.getString();
				while (result != null)
				{
					this.onPackageReceived(new PackageReceivedEvent(this, result));
					result = this.getString();
				}
				// Reset the length of the packet before reusing it.
				packet.setLength(buffer.length);
			}
		}
		catch (Exception e) 
		{
			System.err.println(e);
		}
	}

	private String getString()
	{
		if (this.receiveBuffer != null)
		{
			int firstIndex = receiveBuffer.indexOf(this.packageSplitter);
			if (firstIndex != -1)
			{
				String result = this.receiveBuffer.substring(0, firstIndex);
				this.receiveBuffer = this.receiveBuffer.substring(firstIndex + this.packageSplitter.length());
				return result;
			}
		}
		return null;
	}
}

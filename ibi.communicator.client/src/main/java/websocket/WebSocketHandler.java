package websocket;

import javax.websocket.Session;

import models.CommandsHandler;
import models.WebsocketSession;
import shared.ApplicationUtility;
import shared.Logger;


public class WebSocketHandler 
{
	public WebSocketHandler() {}
	
	public static void sendIncomingCallRespToUI(String message)
	{
    	String logFileName = "communication_clientservice_websocket";
    	if (message.indexOf("heartbeat") != -1)
    		logFileName += "_heartbeat";
		try
		{
			//Session s = WebSocketServerEndpoint.panicStateSessions.get(tIndex);
			WebsocketSession ws = WebSocketServerEndpoint.Sessions;
			Session s = ws.getSession();
			String lastMessage = ws.getMessage();
			try
			{
			    if (s != null && s.isOpen()) 
		        {
			    	Logger.write(logFileName, "Sending to DCU UI >> Session is opend");
					String content = message;
					//if (!ApplicationUtility.isNullOrEmpty(content) && !lastMessage.equalsIgnoreCase(content))
					{
						s.getBasicRemote().sendText(content.trim().replace("\r", "").replace("\n", "").replace(System.getProperty("line.separator"), ""));
						Logger.write(logFileName, "Sending to DCU UI >> command is send " + content);
						ws.setMessage(content);
					}
				}
				else
				{
					Logger.write(logFileName, "Sending to DCU UI >> Session is closed");
					if (s != null)
						s.close();
					
			        Logger.write(logFileName, "Sending to DCU UI >> Session cleanup done");
				}
			}
			catch(Exception ex)
			{
		        Logger.write(logFileName, "Sending to DCU UI >> Exception : " + ex.getMessage());
				Logger.writeError(logFileName, ex);
				if (s != null)
					s.close();
		        Logger.write(logFileName, "Sending to DCU UI >> Session cleanup done");
			}

		}
		catch(Exception ex)
		{
			Logger.writeError(logFileName, ex);
		}
	}
	
	public static boolean sendPanicCallRespToUI(String message)
	{
		try
		{
			WebsocketSession ws = WebSocketServerEndpoint.panicStateSessions;
			Session s = ws.getSession();
			try
			{
			    if (s != null && s.isOpen()) 
		        {
			    	Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> Session is opend");
					String content = CommandsHandler.getPanicCommandForUI(message);
					//s.getBasicRemote().sendText(content);
					s.getBasicRemote().sendText(content.trim().replace("\r", "").replace("\n", "").replace(System.getProperty("line.separator"), ""));
					Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> command is send " + content);
					ws.setMessage(message);
					return true;
				}
				else
				{
					Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> Session is closed");
					if (s != null)
						s.close();
					Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> Session cleanup done");
				}
			}
			catch(Exception ex)
			{
		        Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> Exception : " + ex.getMessage());
				Logger.writeError("communication_clientservice_websocket", ex);
				if (s != null)
					s.close();
		        Logger.write("communication_clientservice_websocket", "Sending to DCU UI >> Session cleanup done");
			}
		}
		catch(Exception ex)
		{
			Logger.writeError("communication_clientservice_websocket", ex);
		}
		return false;
	}
}

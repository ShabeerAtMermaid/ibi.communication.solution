package websocket;

import java.io.IOException;
import java.util.ArrayList;
 
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import io.FileHandler;
import models.CommandsHandler;
import models.WebsocketSession;
import shared.ApplicationUtility;
import shared.Logger;
import tcpclient.TCPClient;


@ServerEndpoint(value = "/server")
public class WebSocketServerEndpoint {

	//public static ArrayList<WebsocketSession> Sessions = new ArrayList<WebsocketSession>();
	//public static ArrayList<WebsocketSession> panicStateSessions = new ArrayList<WebsocketSession>();
	
	public static WebsocketSession Sessions = new WebsocketSession();
	public static WebsocketSession panicStateSessions = new WebsocketSession();
	
	@OnOpen
    public void onOpen(Session session) throws Exception 
    {
		
        Logger.write("communication_clientservice", "WebSocket >> Connected ... " + session.getId());
        /*
        session.setMaxIdleTimeout(0);
		WebsocketSession sock = new WebsocketSession();
		sock.setSession(session);
		sock.setMessage("");
		Sessions.add(sock);
		*/
    }
    
    @OnMessage
    public void onMessage(String message, Session session) throws IOException 
    {
    	Logger.write("communication_clientservice_websocket", "UI to Websocket >> command received at Websocket >> " + message);
    	
    	session.setMaxIdleTimeout(0);
    	if (message.equalsIgnoreCase("panicstate"))
    	{
    		panicStateSessions.setSession(session);
    		panicStateSessions.setMessage("");
    	}
    	else if (message.equalsIgnoreCase("incomingcalls"))
    	{
    		Logger.write("communication_clientservice_websocket", "UI to Websocket >> Adding into incoming sessions list" + message);
    		//incomingStateSessions.add(session);
    		session.setMaxIdleTimeout(0);
    		Sessions.setSession(session);
    		Sessions.setMessage("");
    		Logger.write("communication_clientservice_websocket", "UI to Websocket >> Added into incoming sessions list" + message);
    	} 	
    	else
    	{
    		if (!ApplicationUtility.isNullOrEmpty(message))
    		{
    			JSONObject jsonObj  = new JSONObject(message);
    			String commandText = jsonObj.getString("command"); //call
    			String statusText = jsonObj.getString("status");
    			if (commandText.equalsIgnoreCase(CommandsHandler.CALL) && (statusText.equalsIgnoreCase(CommandsHandler.END) || statusText.equalsIgnoreCase(CommandsHandler.REJECT) || statusText.equalsIgnoreCase(CommandsHandler.DECLINE)))
    			{
    				try {
    					io.FileHandler.writeContentInCallFile(CommandsHandler.NOTINCALL);
    					Logger.write("communication_clientservice_websocket", "InCall.txt >> File updated with value : " + CommandsHandler.NOTINCALL);
    				} catch (Exception e) {}
    			}
    			if (commandText.equalsIgnoreCase(CommandsHandler.CALL) && (message.indexOf("\"type\":\"operator\"") != -1))
    			{
	    			try {
						io.FileHandler.writeContentRingingFile(CommandsHandler.RINGINGEND);
						Logger.write("communication_clientservice_websocket", "Incomming.txt >> File updated with value : " + CommandsHandler.RINGINGEND);
	    			} catch (Exception e) {}
    			}
    		}
    		Logger.write("communication_clientservice_websocket", "WebSocket to Client Service : " + CommandsHandler.NOTINCALL);
    		CommandsHandler.sendIncomingCallResponse(message);
	    }
    }
 
    @OnClose
    public void onClose(Session session, CloseReason closeReason) throws IOException 
    {
        Logger.write("communication_clientservice_websocket", "WebSocket >>"+ String.format("Session %s closed because of %s", session.getId(), closeReason));   
        session.close();
    }
}
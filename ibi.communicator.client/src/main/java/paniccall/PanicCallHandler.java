package paniccall;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.websocket.Session;

import org.json.JSONObject;

import models.CommandsHandler;
import models.WebsocketSession;
import shared.ApplicationUtility;
import shared.Logger;
import websocket.WebSocketServerEndpoint;


public class PanicCallHandler 
{
	private static Timer PanicStateTimer;
	
	
	
	public PanicCallHandler() {}
	
	public static void initialize()
	{
		Logger.write("communication_clientservice", "--------------PanicState process initialized---------------------------------");
		PanicStateTimer = new Timer();
		PanicStateTimer.schedule(new TimerTask(){
			@Override
			public void run()
			{
				try
				{
					WebsocketSession ws = WebSocketServerEndpoint.panicStateSessions;
					Session s = ws.getSession();
					String lastMessage = ws.getMessage();
					try
					{
					    if (s != null && s.isOpen()) 
				        {
							String panicContent = ApplicationUtility.readPanicStateFileContents();
							//if (!lastPanicState.equalsIgnoreCase(panicContent))
							if (!ApplicationUtility.isNullOrEmpty(panicContent) && !lastMessage.equalsIgnoreCase(panicContent))
							{
								String panicCommand = CommandsHandler.getPanicCommandForUI(panicContent);
								s.getBasicRemote().sendText(panicCommand);
								ws.setMessage(panicContent);
								Logger.write("communication_clientservice_websocket", "Websocket to UI >> data send : " + panicCommand);
								if (panicContent.equalsIgnoreCase(CommandsHandler.PANICSTATE))
								{
									CommandsHandler.sendPanicCommand();
								}
							}
						}
						else
						{
							if (s != null)
							{
								s.close();
							}
						}
					}
					catch(Exception ex)
					{
						Logger.writeError("communication_clientservice_websocket", ex);
						if (s != null)
							s.close();
					}
				}
				catch(Exception ex)
				{
					Logger.writeError("communication_clientservice", ex);
				}	
			}
		}, 1 * 500, 1 * 500);
	}
	
	
	public static void panicCallAccpetedOrEnded(String command)
	{
		Logger.write("communication_clientservice", "Client service to WebSockt >> Command : " + command);
		try
		{
			WebsocketSession ws = WebSocketServerEndpoint.panicStateSessions;
			Session s = ws.getSession();
			try
			{
			    if (s != null && s.isOpen()) 
		        {
					String panicCommand = CommandsHandler.getPanicCommandForUI(command);
					s.getBasicRemote().sendText(panicCommand);
					Logger.write("communication_clientservice_websocket", "Websocket to UI >> data send : " + panicCommand);
				}
				else
				{
					if (s != null)
						s.close();
				}
			}
			catch(Exception ex)
			{
				Logger.writeError("communication_clientservice_websocket", ex);
				if (s != null)
					s.close();
			}

		}
		catch(Exception ex)
		{
			Logger.writeError("communication_clientservice", ex);
		}	
	}
}

package tcpclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.logging.FileHandler;

import org.json.JSONObject;

import models.CommandsHandler;
import paniccall.PanicCallHandler;
import shared.AppSettings;
import shared.ApplicationUtility;
import shared.Logger;
import shared.PositionManager;
import websocket.WebSocketHandler;

public class TCPClient {

   public final static int DISCONNECTED = 0;
   public final static int CONNECTED = 1;
  
   public static String hostIP =  AppSettings.getBotServerAddress(); //"teeny.mermaid.dk"; //"localhost"; //"192.168.66.110";
   public static int port = AppSettings.getBotServerPort();
   
   public static int connectionStatus = DISCONNECTED;
   
   
    private final Thread communicationThread;
    
    private Thread outStreamThread;
    private Thread inStreamThread;
    
    
    //private Thread inputStreamThread;
    private long heartbeatDelayMillis = 1 * 1000;
    
    // TCP Components
    public static Socket socket = null;
    
    public static  BufferedReader input = null;
    //public static  DataOutputStream output = null;
    public static PrintWriter output = null;
    
    public TCPClient() {
    	communicationThread = new Thread() 
        {
            public void run() 
            {
            	while (true) {
					switch (connectionStatus) {
                    case DISCONNECTED:
                       try 
                       {
                    	   // try to connect to the server
                    	   Logger.write("communication_clientservice", "trying to connect");
                    	   socket = new Socket();
                    	   socket.connect(new InetSocketAddress(hostIP, port), 10 * 1000);;
                    	   Logger.write("communication_clientservice", "connection establish");
                          
                    	   input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    	   //output = new DataOutputStream( socket.getOutputStream());
                    	   
                    	   output = new PrintWriter(
   										new BufferedWriter(
   												new OutputStreamWriter(
   														socket.getOutputStream(), "UTF-8")), true);

                    	   connectionStatus = CONNECTED;
                    	   socket.setSoTimeout(5 * 1000);
                       }
                       // If error, clean up and output an error message
                       catch (SocketException e) {
                    	  Logger.write("communication_clientservice", "SocketException >> " + e.getMessage());
                    	  Logger.writeError("communication_clientservice", e);
                          cleanUp();
                          connectionStatus = DISCONNECTED;
                       }
                       catch (IOException e) {
                     	  Logger.write("communication_clientservice", "IOException >> " + e.getMessage());
                     	  Logger.writeError("communication_clientservice", e);
                           cleanUp();
                           connectionStatus = DISCONNECTED;
                       }
                       catch (Exception e) {
                    	   Logger.write("communication_clientservice", "Exception >> " + e.getMessage());
                      	   Logger.writeError("communication_clientservice", e);
                           cleanUp();
                           connectionStatus = DISCONNECTED;
                       }
                       break;
                    default: break; // do nothing
                    }
					try { // Poll every ~1000 ms
	                       Thread.sleep(heartbeatDelayMillis);
	                }
	                catch (InterruptedException e) {}
                 }
            };
        };
        communicationThread.start();
    }
    public void startCommunication()
    {
    	startinStreamThread();
    	startouStreamThread();
    }
    private void startouStreamThread() {
    	outStreamThread = new Thread()
 	   {
 	   	  public void run()
 	   	  {
 	   		  while(true)
 	   		  {
 		   		  if(connectionStatus == CONNECTED)
 				  {
 		   			  try
 		   			  {
 		   				  String ping = CommandsHandler.getPingCommand();
 		   				  sendCommand(ping);
 		   			  }
 		   			  catch (Exception e) 
 		   			  {
 		   				  Logger.write("communication_clientservice", "Ping command format Exception >> " + e.getMessage());
 		   				  Logger.writeError("communication_clientservice", e);
 		   			  }
 				  }
 		   		 try {
 			          Thread.sleep(5 * 1000);
 			      }catch (InterruptedException e) {}
 	   		  }
 	   	  }
 	   };
 	   outStreamThread.start();
	}
	private void startinStreamThread() {
		inStreamThread = new Thread()
 	    {
     	  public void run()
     	  {
 		  	   while(true)
 		  	   {
 		  		 try 
 	     		  { 
	         		  if(connectionStatus == CONNECTED && input.ready())
	         		  {
	         			  String readData = input.readLine(); 
	         			  //if (ApplicationUtility.isNullOrEmpty(readData)) 
	         			  //  return;
	         			  System.out.println("Received: "+ readData);
	         			  
	         			  String logFileName = "communication_clientservice";
	         			  if (readData.indexOf("heartbeat") != -1)
         		    		logFileName += "_heartbeat";
	         			 if (!ApplicationUtility.isNullOrEmpty(readData))
	         				 Logger.write(logFileName,  "Received >> " + readData);
	         			  processCommand(readData);
	         		  }
 	     		  }
         		 catch (IOException e) 
        		  {
                	  Logger.write("communication_clientservice", "exception >> " + e.getMessage());
                	  Logger.writeError("communication_clientservice", e);
                      cleanUp();
                      connectionStatus = DISCONNECTED;
                  }
         		 try { // Poll every ~1000 ms
                     Thread.sleep(100);
                  }
                  catch (InterruptedException e) {}
			  } 
     	  }
 	   };
 	   inStreamThread.start();
	}
	private static boolean isSocketAliveUitlitybyCrunchify(String hostName, int port,String logFileName) {
		boolean isAlive = false;
 
		// Creates a socket address from a hostname and a port number
		SocketAddress socketAddress = new InetSocketAddress(hostName, port);
		Socket socket = new Socket();
 
		// Timeout required - it's in milliseconds
		int timeout = 2000;
		try {
			socket.connect(socketAddress, timeout);
			socket.close();
			isAlive = true;
 
		} catch (SocketTimeoutException exception) {
			Logger.write(logFileName,"SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
		} catch (IOException exception) {
			Logger.write(logFileName,
					"IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
		}
		return isAlive;
	}
	public static boolean sendCommand(String command)
    {
		String logFileName = "communication_clientservice";
		if(command.indexOf(CommandsHandler.PING) != -1)
    		logFileName += "_ping";
    	try 
        {   
     	   synchronized (output) 
     	   {
     		   //output.println(command);
     		   //output.flush();
     		   
	   		   //String encodedString = URLEncoder.encode(command, "UTF-8");
	   		   //output.writeBytes(encodedString);
     		  
     		  if(isSocketAliveUitlitybyCrunchify(hostIP, port,logFileName))
     		  {
	         	   //output.writeBytes(command);
     			  output.println(command);
	     		   Logger.write(logFileName, "Send >> " + command);
	         	   return true;
         	  }
     		  else
     			  throw new SocketException("Connection to server not available!");
     	   }
        }
    	catch (SocketException e) 
    	{
      	   Logger.write(logFileName, "exception >> " + e.getMessage());
      	   Logger.writeError(logFileName, e);
            cleanUp();
            connectionStatus = DISCONNECTED;
         }
         catch (IOException e) 
    	{
       	   Logger.write(logFileName, "exception >> " + e.getMessage());
       	   Logger.writeError(logFileName, e);
             cleanUp();
             connectionStatus = DISCONNECTED;
        }
        catch (Exception e) 
        {
     	   Logger.write(logFileName, "exception >> " + e.getMessage());
     	   Logger.writeError(logFileName, e);
           cleanUp();
           connectionStatus = DISCONNECTED;
        }
    	return false;
    }
    
    private static void processCommand(String command)
    {
    	String logFileName = "communication_clientservice_websocket";
    	if (command.indexOf("heartbeat") != -1)
    		logFileName += "_heartbeat";
    	try
    	{
	    	if (!ApplicationUtility.isNullOrEmpty(command))
			{
	        	Logger.write(logFileName, "Sending to DCU UI >> ProcessCommand " + command);

	        	JSONObject jsonObj  = new JSONObject(command);
				String commandText = jsonObj.getString("command"); //call
				String statusText = jsonObj.getString("status");
				Logger.write(logFileName, "Sending to DCU UI >> command : " + commandText + " status " + statusText);
				
				if (commandText.equalsIgnoreCase(CommandsHandler.PANIC))
				{
					if (statusText.equalsIgnoreCase(CommandsHandler.ACCEPT))
					{
						//WebSocketHandler.sendPanicCallRespToUI(CommandsHandler.PANICLISTENSTATE);
						try 
						{
							Logger.write(logFileName, "panicactive.txt update >> Updating file : " + CommandsHandler.PANICLISTENSTATE);
							io.FileHandler.writeContentPanicFile(CommandsHandler.PANICLISTENSTATE);
							Logger.write(logFileName, "panicactive.txt update  >> Updated Updating file : " + CommandsHandler.PANICLISTENSTATE);
						} catch (Exception e) {}
	
					}
					else if (statusText.equalsIgnoreCase(CommandsHandler.END))
					{
						if(WebSocketHandler.sendPanicCallRespToUI(CommandsHandler.END))
						{
							try 
							{
								Logger.write(logFileName, "paniccancel.txt update >> Updating Updating file : " + CommandsHandler.PANICCANCEL);
								io.FileHandler.writeContentPanicCancelFile(CommandsHandler.PANICCANCEL);
								Logger.write(logFileName, "paniccancel.txt update  >> Updated Updating file : " + CommandsHandler.PANICCANCEL);
							} catch (Exception e) {}
							
							try 
							{
								Logger.write(logFileName, "panicactive.txt update >> Updating Updating file : " + CommandsHandler.PANICENDSTATE);
								io.FileHandler.writeContentPanicFile(CommandsHandler.PANICENDSTATE);
								Logger.write(logFileName, "panicactive.txt update  >> Updated Updating file : " + CommandsHandler.PANICENDSTATE);
							} catch (Exception e) {}
						}
					}
				}
				else if (commandText.equalsIgnoreCase(CommandsHandler.CALL) && statusText.equalsIgnoreCase(CommandsHandler.ACCEPT))
				{
					try {
						Logger.write(logFileName, "Incall.txt update >> Updating Incall.txt file : " + CommandsHandler.INCALL);
						io.FileHandler.writeContentInCallFile(CommandsHandler.INCALL);
						Logger.write(logFileName, "Incall.txt update  >> Updated Incall.txt file : " + CommandsHandler.INCALL);
						if (command.indexOf("\"type\":\"operator\"") != -1) return;
					} catch (Exception e) {}
					Logger.write(logFileName, "Sending to DCU UI >> Calling broadcastToBrowsers");
					WebSocketHandler.sendIncomingCallRespToUI(command);
					Logger.write(logFileName, "Send to DCU UI >> Calling broadcastToBrowsers");
				}
				else if (commandText.equalsIgnoreCase(CommandsHandler.PING) && statusText.equalsIgnoreCase(CommandsHandler.ACK))
				{
					String mumbleClientStatus = "";
					if (jsonObj.has("mumbleclientstatus"))
						mumbleClientStatus = jsonObj.getString("mumbleclientstatus");
					String existingValue = io.FileHandler.readMumbleClientOnlineStatusFile();
					if (!ApplicationUtility.isNullOrEmpty(mumbleClientStatus) && !existingValue.equalsIgnoreCase(mumbleClientStatus))
						io.FileHandler.writeContentMumbleClientOnlineStatusFile(mumbleClientStatus);
				}
				else
				{
					if (commandText.equalsIgnoreCase(CommandsHandler.CALL) && (statusText.equalsIgnoreCase(CommandsHandler.END) || statusText.equalsIgnoreCase(CommandsHandler.REJECT) || statusText.equalsIgnoreCase(CommandsHandler.DECLINE)))
					{
						try {
							Logger.write(logFileName, "Incall.txt update >> Updating Incall.txt file : " + CommandsHandler.NOTINCALL);
							io.FileHandler.writeContentInCallFile(CommandsHandler.NOTINCALL);
							Logger.write(logFileName, "Incall.txt update  >> Updated Incall.txt file : " + CommandsHandler.NOTINCALL);
						} catch (Exception e) {}
					}
					else if (commandText.equalsIgnoreCase(CommandsHandler.CALL) && statusText.equalsIgnoreCase(CommandsHandler.RINGINGSTATUS) && (command.indexOf("\"type\":\"operator\"") != -1))
					{
						io.FileHandler.writeContentRingingFile(CommandsHandler.RINGING);
					}

					Logger.write(logFileName, "Sending to DCU UI >> Sending to broadcastToBrowsers");
					WebSocketHandler.sendIncomingCallRespToUI(command);
				}
			}
    	}
    	catch(Exception ex)
    	{
    		Logger.write(logFileName, "Exception : " + ex);
    	}
    }
    
    // Cleanup for disconnect
    private static void cleanUp() {

       try {
          if (socket != null) {
             socket.close();
             socket = null;
          }
       }
       catch (IOException e) { socket = null; }

       try {
          if (input != null) {
        	  input.close();
        	  input = null;
          }
       }
       catch (IOException e) { input = null; }
       
       if (output != null) {
    	   output.close();
    	   output = null;
       }
       /*
       try {
           if (output != null) {
        	   output.close();
        	   output = null;
           }
        }
        catch (IOException e) { output = null; }
        */
    }
}